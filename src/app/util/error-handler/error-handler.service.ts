import { Injectable } from '@angular/core';
import { ToasterService } from '../toaster/toaster.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private _toaster: ToasterService) { }
  error(error:any) {
    this._toaster.toast({
      icon: "error",
      title: error.message,
      timer: 3000,
      showConfirmButton: false,
    })
  }
}
