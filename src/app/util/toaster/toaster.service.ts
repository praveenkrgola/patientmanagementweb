import { Injectable } from '@angular/core';
import Swal, { SweetAlertOptions } from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor() { }
  
  toast(options: SweetAlertOptions) {
    const config: SweetAlertOptions = {
      timer: 3000,
      showConfirmButton: false
    };
    Swal.fire({
      ...config,
      ...options
    })
  }
}
