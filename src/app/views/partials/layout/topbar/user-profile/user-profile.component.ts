// Angular
import { Component, Input, OnInit } from "@angular/core";
// RxJS
import { Observable } from "rxjs";
// NGRX
import { select, Store } from "@ngrx/store";
// State
import { AppState } from "../../../../../core/reducers";
import { currentUser, Logout, User } from "../../../../../core/auth";
import { Router } from "@angular/router";
import { UserLoginService } from "../../../../../../services/user-login/user-login.service";

@Component({
	selector: "kt-user-profile",
	templateUrl: "./user-profile.component.html",
})
export class UserProfileComponent implements OnInit {
	// Public properties
	user$: Observable<User>;

	@Input() avatar = true;
	@Input() greeting = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(
		private store: Store<AppState>,
		private router: Router,
		private userLoginService: UserLoginService
	) {}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	user = JSON.parse(sessionStorage.getItem("user"));
	name = this.user.name;
	designation = this.user.designationName;
	ngOnInit(): void {
		//this.user$ = this.store.pipe(select(currentUser));
	}

	/**
	 * Log out
	 */
	logout() {
		localStorage.removeItem("layoutConfig");
		sessionStorage.clear();
		this.userLoginService.userPs$.next(null);
		this.router.navigate(["/auth/login"]);
		setTimeout(() => {
			location.reload();
		}, 200);
		//this.store.dispatch(new Logout());
	}
}
