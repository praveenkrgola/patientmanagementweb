import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'kt-time-period',
  templateUrl: './time-period.component.html',
  styleUrls: ['./time-period.component.scss']
})
export class TimePeriodComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() date;
  maxDate = new Date();
  constructor() { }

  ngOnInit() {
  }

  changedValue(val) {    
    this.valueChange.emit(val);
  }
  setBlank(){
    this.date = null;
  }

}
