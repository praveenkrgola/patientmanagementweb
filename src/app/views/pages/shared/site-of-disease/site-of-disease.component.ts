import { ChangeDetectorRef, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'kt-site-of-disease',
  templateUrl: './site-of-disease.component.html',
  styleUrls: ['./site-of-disease.component.scss']
})
export class SiteOfDiseaseComponent implements OnInit {
  @Input() multiple;
  @Output() valueChange = new EventEmitter();
  diseases = [
    {
      value: "all",
      name: "ALL"
    },
    {
      value: "ptb",
      name: "PTB"
    }, {
      value: "eptb",
      name: "EPTB"
    }
  ]
  constructor(
    private _cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }
  changedValue(val) {
    this.valueChange.emit(val);
    this._cdr.detectChanges()
  }

}
