import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteOfDiseaseComponent } from './site-of-disease.component';

describe('SiteOfDiseaseComponent', () => {
  let component: SiteOfDiseaseComponent;
  let fixture: ComponentFixture<SiteOfDiseaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteOfDiseaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteOfDiseaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
