import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserLoginService } from '../../../../../../src/services/user-login/user-login.service';

@Component({
  selector: 'kt-field-officer',
  templateUrl: './field-officer.component.html',
  styleUrls: ['./field-officer.component.scss']
})
export class FieldOfficerComponent implements OnInit {
	fieldOfficerList = [];
  selectedValue;
  @Input() multiple;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _userLoginService: UserLoginService,
    private _cdr: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }
  getAllUsers() {
		const filter = {
      where: {
        designation : "fieldofficer"
      },
      order: "name ASC"
		};
		this._userLoginService.getList(filter).subscribe((res: any) => {			
			if (res.length > 0) {

        this.fieldOfficerList = res;
			
			}
		});
		this._cdr.detectChanges();
	}
  changedValue(val) {
    this.valueChange.emit(val);
  }
}
