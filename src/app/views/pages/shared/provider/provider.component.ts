import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { UserLoginService } from '../../../../../../src/services/user-login/user-login.service';

@Component({
  selector: 'kt-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  providerList = [];
  selection = new SelectionModel(true);

  @Input() multiple;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _userLoginService: UserLoginService,
    private _cdr: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }
  getAllUsers() {
		const filter = {
      where: {
        designation: {inq : ["chemist","empanelleddoctor","empanelledchemist","generaldoctor","ayushprovider"]},
        status: true
      },
      order: "name ASC"
		};
		this._userLoginService.getList(filter).subscribe((res: any) => {			
			if (res.length > 0) {
        this.providerList = res;
        this._cdr.detectChanges();
			}
		});
		this._cdr.detectChanges();
	}
  getUsersBasedOnDesignations(designation) {
		const filter = {
      where: {
        designation: {inq: designation}
      },
      order: "name ASC"
		};
		this._userLoginService.getList(filter).subscribe((res: any) => {			
			if (res.length > 0) {

        this.providerList = res;
			
			}
		});
		this._cdr.detectChanges();
	}
  changedValue(val:MatSelectionListChange) {
    this.valueChange.emit(val);
  }
}
