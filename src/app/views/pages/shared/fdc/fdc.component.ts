import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'kt-fdc',
  templateUrl: './fdc.component.html',
  styleUrls: ['./fdc.component.scss']
})
export class FdcComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  fdcData = [
    {
    value: "all",
    name: "ALL"
    },
    {
    value: "yes",
    name: "Yes"
  }, {
    value: "no",
    name: "No"
  }
  ]
  constructor(
    private _cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }
  changedValue(val) {
    this.valueChange.emit(val);
    this._cdr.detectChanges()
  }

}