import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CbnaatOfferedComponent } from './cbnaat-offered.component';

describe('CbnaatOfferedComponent', () => {
  let component: CbnaatOfferedComponent;
  let fixture: ComponentFixture<CbnaatOfferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbnaatOfferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CbnaatOfferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
