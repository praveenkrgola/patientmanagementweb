import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'kt-cbnaat-offered',
  templateUrl: './cbnaat-offered.component.html',
  styleUrls: ['./cbnaat-offered.component.scss']
})
export class CbnaatOfferedComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  cbnaatData = [
    {
      value: "all",
      name: "ALL"
    },
    {
      value: "yes",
      name: "Yes"
    }, {
      value: "no",
      name: "No"
    }
  ]
  constructor(
    private _cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }
  changedValue(val) {
    this.valueChange.emit(val);
    this._cdr.detectChanges()
  }

}
