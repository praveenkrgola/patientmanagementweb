import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SCTAgentComponent } from './sctagent.component';

describe('SCTAgentComponent', () => {
  let component: SCTAgentComponent;
  let fixture: ComponentFixture<SCTAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SCTAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SCTAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
