import { Component, OnInit, Input, Output, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { UserLoginService } from '../../../../../services/user-login/user-login.service';

@Component({
  selector: 'kt-sctagent',
  templateUrl: './sctagent.component.html',
  styleUrls: ['./sctagent.component.scss']
})
export class SCTAgentComponent implements OnInit {

  sctAgentList = [];
  selectedValue;
  @Input() multiple;
  @Output() valueChange = new EventEmitter();

  constructor(
    private _userLoginService: UserLoginService,
    private _cdr: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }
  getAllUsers() {
		const filter = {
      where: {
        designation: "sctagent",
        status : true
      },
      order: "name ASC"
		};
		this._userLoginService.getList(filter).subscribe((res: any) => {			
			if (res.length > 0) {

        this.sctAgentList = res;
			
			}
		});
		this._cdr.detectChanges();
	}
  changedValue(val) {
    this.valueChange.emit(val);
  }
}
