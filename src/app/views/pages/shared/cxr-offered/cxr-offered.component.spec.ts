import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxrOfferedComponent } from './cxr-offered.component';

describe('CxrOfferedComponent', () => {
  let component: CxrOfferedComponent;
  let fixture: ComponentFixture<CxrOfferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxrOfferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxrOfferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
