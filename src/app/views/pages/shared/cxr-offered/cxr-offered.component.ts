import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'kt-cxr-offered',
  templateUrl: './cxr-offered.component.html',
  styleUrls: ['./cxr-offered.component.scss']
})
export class CxrOfferedComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  cxrData = [
    {
    value: "all",
    name: "ALL"
    },
    {
    value: "yes",
    name: "Yes"
  }, {
    value: "no",
    name: "No"
  }
  ]
  constructor(
    private _cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }
  changedValue(val) {
    this.valueChange.emit(val);
    this._cdr.detectChanges()
  }

}
