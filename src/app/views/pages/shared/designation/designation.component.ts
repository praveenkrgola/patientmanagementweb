import { ChangeDetectorRef, Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ListMasterService } from '../../../../../../src/services/list-master/list-master.service';

@Component({
  selector: 'kt-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss']
})
export class DesignationComponent implements OnInit {
  designationList = [];
  @Input() multiple;
  @Output() valueChange = new EventEmitter();
  constructor(private _listMasterService: ListMasterService,
    private _cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.getRegistrationType();
  }
  getRegistrationType() {
    this._listMasterService.getStakeholder({ where: { type: "stakeholder", "name.canBeCreated": true }, order: "name.key ASC" }).subscribe(res => {
      this.designationList = res;
      this._cdr.detectChanges()
    });
  }
  changedValue(val) {
    this.valueChange.emit(val);
  }
}
