import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '../../partials/partials.module';
import { CoreModule } from '../../../../../src/app/core/core.module';
import { ThemeModule } from '../../theme/theme.module';
import { PatientInfoComponent } from './patient-info/patient-info.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SharedComponent } from './shared.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthTokenInterceptor } from '../../../../../src/services/util/interceptors/auth-token.interceptor';
import { RouterModule } from '@angular/router';
import { MatDatepickerModule, MatNativeDateModule, MatRadioModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { FieldOfficerComponent } from './field-officer/field-officer.component';
import { TimePeriodComponent } from './time-period/time-period.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SiteOfDiseaseComponent } from './site-of-disease/site-of-disease.component';
import { CxrOfferedComponent } from './cxr-offered/cxr-offered.component';
import { CbnaatOfferedComponent } from './cbnaat-offered/cbnaat-offered.component';
import { FdcComponent } from './fdc/fdc.component';
import { DesignationComponent } from './designation/designation.component';
import { ProviderComponent } from './provider/provider.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SCTAgentComponent } from './sctagent/sctagent.component';
import { MonthYearComponent } from './month-year/month-year.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [SharedComponent ,PatientInfoComponent, FieldOfficerComponent, TimePeriodComponent, SiteOfDiseaseComponent, CxrOfferedComponent, CbnaatOfferedComponent, FdcComponent, DesignationComponent, ProviderComponent, SCTAgentComponent, MonthYearComponent],
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    ThemeModule,
    MatDialogModule,
    FormsModule,
    MatTooltipModule,
    MatIconModule,
    RouterModule,
    MatSelectModule,
    MatRadioModule,
    MatNativeDateModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatDatepickerModule,        // <----- import(must)
    MatNativeDateModule,        // <----- import for date formating(optional)
    MatMomentDateModule,
    ReactiveFormsModule,
    // CDK MODULES
    ScrollingModule
  ],
  exports: [PatientInfoComponent, FieldOfficerComponent, TimePeriodComponent, SiteOfDiseaseComponent, CxrOfferedComponent, CbnaatOfferedComponent, FdcComponent, DesignationComponent, ProviderComponent, SCTAgentComponent, MonthYearComponent],
  entryComponents: [PatientInfoComponent],
  providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthTokenInterceptor,
			multi: true,
		},
	],
})
export class SharedModule { }
