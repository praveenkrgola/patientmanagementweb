import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InvoiceComponent } from '../../program-manager/invoice/invoice.component';

@Component({
  selector: 'kt-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.scss']
})
export class PatientInfoComponent implements OnInit {

	constructor(
		private dialogRef: MatDialogRef<InvoiceComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		console.log("data from Shared Patient", data);	
	}
  ngOnInit() {
  }

}
