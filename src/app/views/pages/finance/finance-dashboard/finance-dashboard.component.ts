import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import _moment from "moment";
import { Subscription } from 'rxjs';
import { InvoiceHistoryService } from '../../../../../../src/services/invoice-history/invoice-history.service';
import { ProviderComponent } from '../../shared/provider/provider.component';

@Component({
	selector: 'kt-finance-dashboard',
	templateUrl: './finance-dashboard.component.html',
	styleUrls: ['./finance-dashboard.component.scss']
})
export class FinanceDashboardComponent implements OnInit {
	@ViewChild("callProviderComponent", { static: true }) callProviderComponent: ProviderComponent;
	filterForm: FormGroup;
	checkboxForm: FormGroup;
	fromDateToDate: Date[];
	dataOfTiles: any;
	dynamicform: any;
	invoiceNotRaisedByUserTypeData: any;
	amountDueByUserTypeData: any;
	raisedInvoicePieChartData: {}[];
	acceptedRejectedPieChartData: any[];
	amountRejectedGroupByReasonData: any;
	paymentsProcessedRejectedOpenPieChartData: { name: string; value: any; }[];
	totalAmountDue: boolean = false;
	voucherAcceptedAmount: boolean = false;
	totalInvoiceAmount: boolean = false;
	paymentsMade: boolean = false;
	private subscriptions: Subscription[] = [];

	constructor(
		private _cdr: ChangeDetectorRef,
		private _invoiceHistoryService: InvoiceHistoryService
	) {

		let today = new Date();
		let fromDate = new Date();
		fromDate.setDate(fromDate.getDate() - 85);
		const fromDate2 = new Date(`${fromDate.getFullYear()}-${fromDate.getMonth() + 1}-01`);
		this.fromDateToDate = [fromDate2, today];
		this.createForm();

	}
	createForm() {
		this.filterForm = new FormGroup({
			designationList: new FormControl(),
			providerList: new FormControl(),
			// date: new FormControl(this.fromDateToDate),
			month: new FormControl(),
			year: new FormControl()
		});
		this.checkboxForm = new FormGroup({
			totalAmountDue: new FormControl(false),
			totalInvoiceAmount: new FormControl(false),
			voucherAcceptedAmount: new FormControl(false),
			paymentsMade: new FormControl(false),
		});
	}

	async ngOnInit() {
		// const checkbox = this.getCheckboxForm();
		const form = this.getForm();
		await this.getTilesDataForFinanceDashboard(form);
		this.checkboxForm.valueChanges.subscribe(async (check) => {
			const form = this.getForm();
			await this.getTilesDataForFinanceDashboard(form);
			if (check.totalAmountDue) { this.totalAmountDue = true; await this.amountDueByUserType(form); } else this.totalAmountDue = false;
			if (check.totalInvoiceAmount) { this.totalInvoiceAmount = true; await this.invoiceNotRaisedByUserType(form); } else this.totalInvoiceAmount = false;
			if (check.voucherAcceptedAmount) {
				this.voucherAcceptedAmount = true;
				await this.amountRejectedGroupByReason(form);
			} else this.voucherAcceptedAmount = false;
			if (check.paymentsMade) this.paymentsMade = true; else this.paymentsMade = false;
		})

		this.filterForm.valueChanges.subscribe(async (x) => {
			const form = this.getForm();
			await this.getTilesDataForFinanceDashboard(form);
			if (this.totalInvoiceAmount) await this.invoiceNotRaisedByUserType(form);
			if (this.totalAmountDue) await this.amountDueByUserType(form);
			if (this.voucherAcceptedAmount) await this.amountRejectedGroupByReason(form);
		});
		this._cdr.detectChanges();
	}


	getTilesDataForFinanceDashboard(form: any) {
		const subs = this._invoiceHistoryService.getTilesDataForFinanceDashboard(form).subscribe(res => {
			this.dataOfTiles = res;
			this.raisedInvoicePieChartData = [
				{
					name: "Raised invoice amount",
					value: res.totalInvoiceAmount
				}, {
					name: "Not raised invoice amount",
					value: res.totalAmountDue
				}
			];
			this.acceptedRejectedPieChartData = [
				{
					name: "Total amount accepted",
					value: res.voucherAcceptedAmount
				}, {
					name: "Total amount rejected",
					value: res.voucherRejectedAmountByPT
				}
			];

			this.paymentsProcessedRejectedOpenPieChartData = [
				{
					name: "Payments processed",
					value: res.paymentsMade
				}, {
					name: "Payments rejected",
					value: res.voucherRejectedAmountByFT
				},
				{
					name: "Payments open",
					value: res.voucherAcceptedAmount - (res.paymentsMade + res.voucherRejectedAmountByFT)
				}
			];

			this._cdr.detectChanges();
		});
		this.subscriptions.push(subs);
	}
	invoiceNotRaisedByUserType(form: any) {
		const subs = this._invoiceHistoryService.invoiceNotRaisedByUserType(form).subscribe(res => {
			this.invoiceNotRaisedByUserTypeData = res;
		});
		this.subscriptions.push(subs);

	}
	amountDueByUserType(form: any) {
		const subs = this._invoiceHistoryService.amountDueByUserType(form).subscribe(res => {
			this.amountDueByUserTypeData = res;
		});
		this.subscriptions.push(subs);
	}
	amountRejectedGroupByReason(form: any) {
		const subs = this._invoiceHistoryService.amountRejectedGroupByReason(form).subscribe(res => {
			this.amountRejectedGroupByReasonData = res;
		})
		this.subscriptions.push(subs);

	}
	getForm() {
		let form = this.filterForm.value;
		form = Object.entries(form).reduce(
			(a, [k, v]: any) =>
				v == null || v.length == 0 ? a : ((a[k] = v), a),
			{}
		);
		this.dynamicform = form;
		return form;
	}
	// getCheckboxForm() {
	// 	let form = this.checkboxForm.value;
	// 	console.log(form);

	// 	form = Object.entries(form).reduce(
	// 		(a, [k, v]: any) =>
	// 			v == null || v.length == 0 ? a : ((a[k] = v), a),
	// 		{}
	// 	);
	// 	return form;
	// }
	getDesignation(designation) {
		this.filterForm.patchValue({ designationList: designation });
		this.callProviderComponent.getUsersBasedOnDesignations(designation);
	}
	getProvider(event) {
		this.filterForm.patchValue({ providerList: event });
	}
	getDateRange(event) {
		const date = [
			_moment(event[0]).format('YYYY-MM-DD'),
			_moment(event[1]).format('YYYY-MM-DD'),
		]

		this.filterForm.patchValue({ date: date });
	}
	monthYearValue(event) {
		const month = new Date(event).getMonth() + 1;
		const year = new Date(event).getFullYear();
		this.filterForm.patchValue({
			month: month,
			year: year
		})
	}

	ngOnDestroy() {
		this.subscriptions.forEach((sub, i) => {
			console.log(`unsubscribing: ${i}`);
			sub.unsubscribe();
		});
	}
}
