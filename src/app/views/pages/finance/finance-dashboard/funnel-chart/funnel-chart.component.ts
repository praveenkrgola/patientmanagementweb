import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'kt-funnel-chart',
  templateUrl: './funnel-chart.component.html',
  styleUrls: ['./funnel-chart.component.scss']
})
export class FunnelChartComponent implements OnChanges {

  constructor() { }
  @Input() data: any;
  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) this.createChart();
  }
  createChart() {
    let chart = am4core.create("chartdiv", am4charts.SlicedChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
    chart.logo.disabled = true;
    chart.data = [
      {
      "name": "# Amount due",
      "value": this.data.totalAmountDue
    },
    {
      "name": "# Raised invoice amount ",
      "value": this.data.totalInvoiceAmount
    },
    {
      "name": "Amount accepted",
      "value": this.data.voucherAcceptedAmount
      },
      {
      "name": "Payments made",
      "value": this.data.paymentsMade
      }
    ];

    let series = chart.series.push(new am4charts.FunnelSeries());
    series.colors.step = 2;
    series.dataFields.value = "value";
    series.dataFields.category = "name";
    series.alignLabels = true;

    series.labelsContainer.paddingLeft = 15;
    series.labelsContainer.width = 200;
    series.labels.template.text = "[bold]{category}[/]: [bold]{value}[/]";
    series.slices.template.tooltipText = "[bold]{category}[/]: [bold]{value}[/]";
    // series.tooltipText = "{category}: {value}";
    
    //series.orientation = "horizontal";
    //series.bottomRatio = 1;

    // chart.legend = new am4charts.Legend();
    // chart.legend.position = "left";
    // chart.legend.valign = "bottom";
    // chart.legend.margin(5, 5, 20, 5);

  }

}
