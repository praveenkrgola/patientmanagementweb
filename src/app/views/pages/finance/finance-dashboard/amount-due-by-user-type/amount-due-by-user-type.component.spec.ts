import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountDueByUserTypeComponent } from './amount-due-by-user-type.component';

describe('AmountDueByUserTypeComponent', () => {
  let component: AmountDueByUserTypeComponent;
  let fixture: ComponentFixture<AmountDueByUserTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmountDueByUserTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountDueByUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
