import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { InvoiceHistoryService } from '../../../../../../../src/services/invoice-history/invoice-history.service';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'kt-amount-due-by-user-type',
  templateUrl: './amount-due-by-user-type.component.html',
  styleUrls: ['./amount-due-by-user-type.component.scss']
})
export class AmountDueByUserTypeComponent implements OnChanges {

  constructor(
    private _invoiceHistoryService: InvoiceHistoryService
  ) { }
  @Input() data: any;
  @Input() graphId: any;
  @Input() title: any;
  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) this.createGraph(this.data);
  }
  createGraph(res: any) {
    let chart = am4core.create(this.graphId, am4charts.XYChart);
    chart.logo.disabled = true;
    // Add data
    chart.data.length = 0;
    chart.data = res;
    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "label";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    
    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "amount";
    series.dataFields.categoryX = "label";
    series.name = "amount";
    series.columns.template.tooltipText = "{categoryX}: [bold]₹{valueY}[/]";
    series.columns.template.fillOpacity = .8;
    series.columns.template.width = 60; //am4core.percent(20);
    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;
    // Truncate
    let label = categoryAxis.renderer.labels.template;
    // label.wrap = true;
    // label.maxWidth = 120;
    label.truncate = true;
    label.maxWidth = 120;
    label.tooltipText = "{name}";
  }

}
