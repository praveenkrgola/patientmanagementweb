import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'kt-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnChanges {

  constructor() { }
  @Input() data: any;
  @Input() title: any;
  @Input() graphId: any;
  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) this.createChart();
  }
  createChart() {
    let chart = am4core.create(this.graphId, am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    chart.logo.disabled = true;
    chart.legend = new am4charts.Legend();
    chart.data.length = 0;
    chart.data = this.data;

    let series = chart.series.push(new am4charts.PieSeries3D());
    series.dataFields.value = "value";
    series.dataFields.category = "name";
    series.colors.step = 3;
    series.labels.template.text = "₹{value}" //"{value.percent.formatNumber('#.0')}%"
    series.legendSettings.valueText = "₹{value}";
  }

}
