import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountDueByPaymentTypeComponent } from './amount-due-by-payment-type.component';

describe('AmountDueByPaymentTypeComponent', () => {
  let component: AmountDueByPaymentTypeComponent;
  let fixture: ComponentFixture<AmountDueByPaymentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmountDueByPaymentTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountDueByPaymentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
