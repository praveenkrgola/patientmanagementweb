import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { InvoiceHistoryService } from '../../../../../../../src/services/invoice-history/invoice-history.service';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'kt-amount-due-by-payment-type',
  templateUrl: './amount-due-by-payment-type.component.html',
  styleUrls: ['./amount-due-by-payment-type.component.scss']
})
export class AmountDueByPaymentTypeComponent implements OnChanges {

  constructor(
    private _invoiceHistoryService: InvoiceHistoryService
  ) { }
  @Input() form: any;
  ngOnChanges(changes: SimpleChanges): void {
    if (this.form) this.amountDueByPaymentType();
  }
  amountDueByPaymentType() {
    this._invoiceHistoryService.amountDueByPaymentType(this.form).subscribe(res => {
      console.log(res);
      this.createGraph(res)
    });
  }
  createGraph(res: any) {
    let chart = am4core.create("chartdiv2", am4charts.XYChart);
    chart.logo.disabled = true;
    // Add data
    chart.data = res;
    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "service";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    
    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "amount";
    series.dataFields.categoryX = "service";
    series.name = "amount";
    series.columns.template.tooltipText = "{categoryX}: [bold]₹{valueY}[/]";
    series.columns.template.fillOpacity = .8;
    series.columns.template.width = 60; //am4core.percent(20);
    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;
    // Truncate
    let label = categoryAxis.renderer.labels.template;
    // label.wrap = true;
    // label.maxWidth = 120;
    label.truncate = true;
    label.maxWidth = 120;
    label.tooltipText = "{service}";
  }

}
