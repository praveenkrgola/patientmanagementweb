import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { InvoiceHistoryService } from "../../../../../../src/services/invoice-history/invoice-history.service";
import Swal from "sweetalert2";
import { ReferralMasterService } from "../../../../../../src/services/referral-master/referral-master.service";
import { ContainerService } from "../../../../../../src/services/container/container.service";
import { SelectionModel } from "@angular/cdk/collections";
import { forkJoin } from "rxjs";
import * as XLSX from "xlsx";
import { DatePipe } from "@angular/common";
import { ListMasterService } from "../../../../../../src/services/list-master/list-master.service";
import { truncateWithEllipsis } from "@amcharts/amcharts4/.internal/core/utils/Utils";
import { InvoiceRejectedVoucherService } from "../../../../../../src/services/invoice-rejected-voucher/invoice-rejected-voucher.service";
import { environment } from "../../../../../../src/environments/environment";

@Component({
    selector: 'kt-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
    currentUser = JSON.parse(sessionStorage.getItem("user"));
    invoiceForm: FormGroup;
    // Mat-TABLE
    @ViewChild("paginator", { static: false }) paginator: MatPaginator;
    @ViewChild("paginatoruserwise", { static: false }) paginatorUserWise: MatPaginator;
    @ViewChild("paginatorRejectedVoucher", { static: false }) paginatorRejectedVoucher: MatPaginator;
    @ViewChild("sort", { static: true }) sort: MatSort;
    @ViewChild("sort2", { static: true }) sort2: MatSort;
    @ViewChild("sort3", { static: true }) sort3: MatSort;
    months = [
        { key: "January", value: 1 },
        { key: "February", value: 2 },
        { key: "March", value: 3 },
        { key: "April", value: 4 },
        { key: "May", value: 5 },
        { key: "June", value: 6 },
        { key: "July", value: 7 },
        { key: "August", value: 8 },
        { key: "September", value: 9 },
        { key: "October", value: 10 },
        { key: "November", value: 11 },
        { key: "December", value: 12 },
    ];
    years = [];
    displayedColumns: string[] = [
        "sn",
        "providerName",
        "providerType",
        "mappedFieldOfficer",
        "month",
        "year",
        "billNumber",
        "billDate",
        "PDFInvoice",
        "annualAmountEarned",
        "voucherAcceptedByPT",
        "voucherAcceptedAmountByPT",
        "previousMonthvoucherAcceptedByPT",
        "previousMonthvoucherAcceptedAmountByPT",
        "voucherRejectedByPT",
        "voucherRejectedAmountByPT",
        "totalPayableAmount",
        "accountNumber",
        "IFSCCode",
        "PANNumber",
        "bankAccountPhoto",
        "PANCard",
        "invoiceValidationByFinanceTeam"
    ];
    displayedColumnsUserWise: string[] = [
        "sn",
        "patientName",
        "mobile",
        "adityaId",
        "nikshayId",
        "voucherName",
        "voucherCode",
        "voucherCreatedAt",
        "voucherRedeemedAt",
        "incentiveAmount",
        "proofOfService",
    ];
    displayedColumnsRejectedVoucher: string[] = [
        "sn",
        "patientName",
        "mobile",
        "adityaId",
        "nikshayId",
        "voucherName",
        "voucherCode",
        "voucherCreatedAt",
        "voucherRedeemedAt",
        "incentiveAmount",
        "voucherRejectedAt",
        "rejectionRemarks",
        "proofOfService",
    ];
    selection = new SelectionModel(true, []);

    dataSource: MatTableDataSource<any>;
    dataSourceUserWise: MatTableDataSource<any>;
    dataSourceRejectedVoucher: MatTableDataSource<any>;
    showUserDetailsTable: boolean = false;
    showUserWiseDetailedTable: boolean = false;
    showRejectedVoucherTable: boolean = false;
    selectedUser: any;
    showProcessing: boolean = false;
    constructor(
        private _fb: FormBuilder,
        private _invoiceHistoryService: InvoiceHistoryService,
        private _referralMasterService: ReferralMasterService,
        private _listMasterService: ListMasterService,
        private _containerService: ContainerService,
        private _invoiceRejectedVoucherService: InvoiceRejectedVoucherService,
        private _cdr: ChangeDetectorRef
    ) {
        this.createForm();
    }
    createForm() {
        this.invoiceForm = this._fb.group({
            month: new FormControl("", Validators.required),
            year: new FormControl("", Validators.required),
        });
    }

    ngOnInit() {
        let counter = 2;
        let currentYear = new Date().getFullYear();
        for (let i = 0; i < counter; i++) {
            this.years.push({
                key: currentYear,
            });
            currentYear--;
        }
    }
    getAllGeneratedInvoices() {
        this.showUserWiseDetailedTable = false;
        this.showProcessing = true;
        const filter = {
            where: {
                month: this.invoiceForm.value.month,
                year: this.invoiceForm.value.year,
                "invoiceStatus": "approvedByPT"
                // "invoiceStatus": { nin: ["approvedByFT", "rejectedByFT"] }
            },
            include: [
                {
                    relation: "user",
                    scope: {
                        include: ["fieldOfficer", "panCard", "bankAccountPhoto"],
                    },
                },
            ],
        };
        this._invoiceHistoryService.getInvoiceHistory(filter).subscribe((res) => {
            this.showProcessing = false;
            if (res.length == 0) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "center",
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                });
                Toast.fire({
                    icon: "error",
                    title: `No Invoice Found !!`,
                });
                this.showUserDetailsTable = false;
                this._cdr.detectChanges();
            } else {
                this.showUserDetailsTable = true;
                this.scroll("#invoice-table");
                this.dataSource = new MatTableDataSource(res);
                setTimeout(() => {
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                }, 200);
                this._cdr.detectChanges();
            }
            this._cdr.detectChanges();
        });
    }
    getDetailedInfoUserWise(data) {
        this.showProcessing = true;
        this.selectedUser = data;
        let referralIds = [];
        referralIds = referralIds.concat(
            this.selectedUser.notificationReferralId,
            this.selectedUser.reimbursementXRAYReferralId,
            this.selectedUser.reimbursementDOCTORReferralId,
            this.selectedUser.reimbursementFDCReferralId,
            this.selectedUser.rejectedVoucherAcceptedByPT
        ).filter(Boolean);
        let rejectedReferralIds = [];
        rejectedReferralIds = rejectedReferralIds.concat(this.selectedUser.voucherRejectedByPT, this.selectedUser.rejectedVoucherRejectedByPT).filter(Boolean);

        if (data.voucherRejectedByPT && data.voucherRejectedByPT.length > 0) {
            referralIds = referralIds.filter((el) => !data.voucherRejectedByPT.includes(el));
        }
        const filter = {
            where: {
                _id: { inq: referralIds },
                invoiceStatus: { nin: ["voucherRejectedByPT"] }
            },
            include: [
                {
                    relation: "patient",
                    scope: {},
                },
                {
                    relation: "file",
                },
                {
                    relation: "FDCRedeemedFile",
                },
                {
                    relation: "voucher",
                    scope: {
                        include: ["incentive"],
                    },
                },
                {
                    relation: "clinicVisitRedeemedFile"
                }
            ],
        };
        const rejectedFilter = {
            where: {
                _id: { inq: rejectedReferralIds },
                invoiceStatus: { inq: ["voucherRejectedByPT"] }
            },
            include: [
                {
                    relation: "patient",
                    scope: {},
                },
                {
                    relation: "file",
                },
                {
                    relation: "FDCRedeemedFile",
                },
                {
                    relation: "voucher",
                    scope: {
                        include: ["incentive"],
                    },
                },
                {
                    relation: "clinicVisitRedeemedFile"
                }
            ],
        };
        const currentReferral = this._referralMasterService.getReferralData(filter);
        const rejectedReferral = this._referralMasterService.getReferralData(rejectedFilter);
        forkJoin([currentReferral, rejectedReferral]).subscribe(([data1, data2]: any) => {
            this.showProcessing = false;
            if (data1.length > 0) {
                console.log(data1);
                
                this.dataSourceUserWise = new MatTableDataSource(data1);
                setTimeout(() => {
                    this.dataSourceUserWise.paginator = this.paginatorUserWise;
                    this.dataSourceUserWise.sort = this.sort2;
                }, 200);
                this.showUserWiseDetailedTable = true;
                this.scroll("#userDetailsTable");
                this._cdr.detectChanges();
            }
            if (data2.length > 0) {
                this.dataSourceRejectedVoucher = new MatTableDataSource(data2);
                setTimeout(() => {
                    this.dataSourceRejectedVoucher.paginator = this.paginatorRejectedVoucher;
                    this.dataSourceRejectedVoucher.sort = this.sort3;
                }, 200);
                this.showRejectedVoucherTable = true;
                this.scroll("#userDetailsTable");
                this._cdr.detectChanges();
            }
        });
        this._cdr.detectChanges();
    }

    downloadFile(container, modifiedFileName) {
        this._containerService
            .downloadFile(container, modifiedFileName)
            .subscribe((res) => {
                var fileURL = window.URL.createObjectURL(res);
                // let tab = window.open();
                // tab.location.href = fileURL;
                window.open(
                    fileURL,
                    "Proof",
                    "toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
                );
            });
    }
    getPatientDetail(row) { }

    acceptInvoice(row) {
        (Swal as any).fire({
            title: "Are you sure want to accept the invoice.",
            icon: "info",
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
            confirmButtonColor: "#16a184",
            cancelButtonColor: "#f4516c",
            confirmButtonText: "Yes",
        }).then((alertRes) => {
            if (alertRes.value) {
                let referralIds = [];
                referralIds = referralIds.concat(row.voucherAcceptedByPT, row.rejectedVoucherAcceptedByPT).filter(Boolean);
                const where = {
                    _id: { inq: referralIds },
                };
                const updateData = { invoiceStatus: "voucherAcceptedByFT", voucherAcceptedByFTAt: new Date().getTime() };
                this._referralMasterService.updateReferralData(where, updateData).subscribe((referralRes) => {
                    if (referralRes) {
                        const whereObj = { where: { _id: row.id } };
                        this._invoiceHistoryService.getInvoiceHistory(whereObj).subscribe((getInvoiceHistoryRes) => {

                            const where = { _id: getInvoiceHistoryRes[0].id };
                            let updateData = {
                                voucherAcceptedByFT: referralIds,
                                invoiceStatus: "approvedByFT",
                                voucherAcceptedAmountByFT: ((row.voucherAcceptedAmountByPT ? row.voucherAcceptedAmountByPT : 0) + (row.rejectedVoucherAcceptedAmountByPT ? row.rejectedVoucherAcceptedAmountByPT : 0)),
                                voucherAcceptedByFTAt: new Date().getTime(),
                                actionTakenByFTId: this.currentUser.id,
                                totalPayableAmount: ((row.voucherAcceptedAmountByPT ? row.voucherAcceptedAmountByPT : 0) + (row.rejectedVoucherAcceptedAmountByPT ? row.rejectedVoucherAcceptedAmountByPT : 0)),
                            };
                            this._invoiceHistoryService.updateInvoiceHistory(where, updateData).subscribe((updateInvoiceHistoryRes) => {

                                const rejectedVoucherWhere = { referralId: { inq: row.voucherRejectedByPT }, userId: row.userId };

                                const updateRejectedVouchersObj = { actionPerformedByFT: true };
                                this._invoiceRejectedVoucherService.updateData(rejectedVoucherWhere, updateRejectedVouchersObj).subscribe(updateRejectedRes => {
                                    this.showUserWiseDetailedTable = false;
                                    this.showRejectedVoucherTable = false;
                                    this.getAllGeneratedInvoices();
                                    this._cdr.detectChanges();
                                })
                            }
                            );

                        });
                    }
                });
                this._cdr.detectChanges();
            }
        });
    }
    getRejectionRemarks(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._listMasterService
                .getStakeholder({
                    where: { type: "rejection" },
                    order: "name.key ASC",
                })
                .subscribe((res) => {
                    resolve(res[0].name)
                }, err => {
                    reject(err)
                });
        });
    }

    async rejectInvoice(row) {
        let selectOptions = await this.getRejectionRemarks();

        (Swal as any)
            .fire({
                title: "Please enter a reason for the denial of invoice.",
                icon: "info",
                input: "select",
                inputOptions: { "select": "Select One", ...selectOptions },
                inputValidator: (value) => {
                    if (!value || value === "select") {
                        return 'You need to choose a reason!'
                    }
                },
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
                confirmButtonColor: "#16a184",
                cancelButtonColor: "#f4516c",
                confirmButtonText: "Submit",
            })
            .then(async (alertRes) => {

                if (alertRes.value) {
                    let reason = alertRes;

                    if (alertRes.value === "other") {
                        const otherValue = await (Swal as any).fire({
                            title: "Please type a reason for the denial of voucher.",
                            icon: "info",
                            input: "textarea",
                            inputPlaceholder: "Type here...",
                            inputValidator: value => {
                                if (!value) {
                                    return "You need to type a reason!";
                                }
                            },
                            showConfirmButton: true,
                            showCancelButton: true
                        });
                        reason = otherValue;
                    }

                    let referralIds = [];
                    referralIds = referralIds.concat(row.voucherAcceptedByPT, row.rejectedVoucherAcceptedByPT).filter(Boolean);
                    const updateInvoiceRejectedVouchers = await this.getReferralDataAndUpdateInvoiceRejectedVouchers(referralIds, row, reason);
                    const where = { _id: { inq: referralIds } };
                    const updateData = {
                        invoiceStatus: "voucherRejectedByFT",
                        voucherRejectedByFTRemarks: reason.value,
                        voucherRejectedByFTAt: new Date().getTime(),
                    };
                    this._referralMasterService.updateReferralData(where, updateData).subscribe((referralRes) => {
                        if (referralRes) {
                            const whereObj = { where: { _id: row.id } };
                            this._invoiceHistoryService.getInvoiceHistory(whereObj).subscribe((getInvoiceHistoryRes) => {
                                const where = { _id: getInvoiceHistoryRes[0].id };
                                let updateData = {
                                    voucherRejectedByFT: referralIds,
                                    invoiceStatus: "rejectedByFT",
                                    actionTakenByFTId: this.currentUser.id,
                                    invoiceRejectedByFTRemarks: reason.value,
                                    voucherRejectedAmountByFT: row.voucherAcceptedAmountByPT,
                                    voucherRejectedByFTAt: new Date().getTime(),
                                };
                                this._invoiceHistoryService.updateInvoiceHistory(where, updateData).subscribe((updateInvoiceHistoryRes) => {
                                    const rejectedVoucherWhere = {
                                        referralId: { inq: row.voucherRejectedByPT },
                                        userId: row.userId,
                                    };

                                    const updateRejectedVouchersObj = {
                                        actionPerformedByFT: true
                                    };
                                    this._invoiceRejectedVoucherService.updateData(rejectedVoucherWhere, updateRejectedVouchersObj).subscribe(updateRejectedRes => {
                                        this.showRejectedVoucherTable = false;
                                        this.showUserWiseDetailedTable = false;
                                        this.getAllGeneratedInvoices();
                                        this._cdr.detectChanges();
                                    })
                                }
                                );

                            });
                        }
                    });
                    this._cdr.detectChanges();
                }
            });
    }
    getReferralDataAndUpdateInvoiceRejectedVouchers(referralIds, row, reason): Promise<any> {
        return new Promise((resolve, reject) => {
            this._referralMasterService.getReferralData({
                where: { _id: { inq: referralIds } },
                include: [
                    {
                        relation: "voucher",
                        scope: {
                            include: ["incentive"],
                        },
                    },
                ],
            }).subscribe((res) => {
                const finalRes = res;
                const rejectedDate = `${this.invoiceForm.value.month}-1-${this.invoiceForm.value.year}`;
                let rejectedVouchers = finalRes.map((item) => {
                    let data = {
                        invoiceId: row.id,
                        billNumber: row.invoiceBillNumber,
                        userId: row.userId,
                        month: row.month,
                        year: row.year,
                        referralId: item.id,
                        patientId: item.patientId,
                        voucherId: item.voucherId,
                        voucherCode: item.voucherCode,
                        voucherCreatedAt: item.voucherCreatedAt,
                        voucherRedeemedAt: item.voucherRedeemedAt,
                        mobile: item.mobile,
                        adityaId: item.adityaId,
                        unitCost: item.voucher.incentive[0].amount,
                        voucherRejectedByFTRemarks: reason.value,
                        voucherRejectedByFTAt: new Date().getTime(),
                        invoiceRejectedDate: new Date(rejectedDate).setMonth(new Date(rejectedDate).getMonth() + 1),
                        rejectedBy: this.currentUser.id,
                        invoiceStatus: "notClear",
                        "actionPerformedByFT": true
                    };
                    if (item.voucherId == "5ff565416dede21747a682b9") {
                        data["fileId"] = item.fileId;
                    } else if (item.voucherId == "5ff5659f6dede21747a682bd") {
                        data["fileId"] = item.FDCRedeemedFileId;
                    } else if (item.voucherId == "601112b06dede219e6eac8e7") {
                        data["fileId"] = item.CBNAATReportFileId;
                    } else if (item.voucherId == "602127ad6dede214bc03966c") {
                        data["fileId"] = item.fileId;
                    }
                    return data;
                }
                );

                this._invoiceRejectedVoucherService.postRejectedVoucher(rejectedVouchers).subscribe((rejectedVoucherRes) => {
                    if (rejectedVoucherRes) {
                        resolve(rejectedVoucherRes);
                    }
                })
            }, err => {
                reject(err)
            });
        });
    }
    scroll(elem: string) {
        setTimeout(() => {
            document
                .querySelector(elem)
                .scrollIntoView({ behavior: "smooth", block: "start" });
        }, 100);
    }
    openInvoice(fileURL) {
        window.open(
            fileURL,
            "Proof",
            "toolbar=yes, resizable=yes, scrollbars=yes, width=600, height=400, top=200 , left=500"
        );
    }

    exportDataSourceTable() {
        let dataToExport = this.dataSource.data.map((x) => ({
            "Provider's Name": x.user.name || '---',
            "Designation Name": x.user.designationName || '---',
            "Month": x.month || '---',
            "Year": x.year || '---',
            "Field Officer Name": x.user.fieldOfficer.name || '---',
            "Bill Number": x.invoiceBillNumber || '---',
            "Bill Date": x.billDate ? new DatePipe('en-US').transform(new Date(x.billDate), 'dd-MM-yyyy hh:mm:ss a') : '---',
            "Invoice Amount": x.invoiceAmount || '---',
            "Total Vouchers": x.totalVouchers || '---',
            "Invoice Link": {
                f: `=HYPERLINK("${x.pdfLink}", "Invoice PDF")`
            },
            "Account Number": x.user.providerBankAccountNumber || '---',
            "PAN Card Number": x.user.providerPANCardNumber || '---',
            "Bank IFSC Code": x.user.providerBankIFSCCode || '---',
            "Bank Account Photo": {
                f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.user.bankAccountPhoto.container}/download/${x.user.bankAccountPhoto.modifiedFileName}", "PHOTO LINK")`,
            },
            "PAN Card": {
                f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.user.panCard.container}/download/${x.user.panCard.modifiedFileName}", "PAN Card LINK")`,
            }
        }));
        const workSheet = XLSX.utils.json_to_sheet(dataToExport);
        const workBook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
        XLSX.writeFile(workBook, "SUMMARY OF INVOICES GENERATED.xlsx");
    }

    exportTableUserWise() {
        const dataToExport = this.dataSourceUserWise.data.map((x) => {
            let proofOfService;
            let proofServiceObject;
            let proofServiceClaimedObject;
            if (x.voucherId == '5ff565416dede21747a682b9' && x.CXRUpload == true) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "View")`,
                }
            }
            if (x.voucherId == '5ff5659f6dede21747a682bd' && x.file) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "FDC Issue")`,
                }
            }
            if (x.voucherId == '5ff5659f6dede21747a682bd' && x.FDCRedeemedFile) {
                proofOfService = `${environment.apiEndPoint}containers/${x.FDCRedeemedFile.container}/download/${x.FDCRedeemedFile.modifiedFileName}`;
                proofServiceClaimedObject = {
                    f: `=HYPERLINK("${proofOfService}", "FDC Claim")`,
                }
            }
            if (x.voucherId == '602127ad6dede214bc03966c' && x.file) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "View")`,
                }
            }


            return {
                "Patient's Name": x.patient ? `${x.patient.firstName} ${x.patient.surName}` : '---',
                "mobile": x.patient.mobile || '---',
                "Aditya ID": x.patient.adityaId || '---',
                "Nikshay ID": x.patient.nikshayId || '---',
                "Voucher Name": x.voucher.name || '---',
                "Voucher Code": x.voucherCode || '---',
                "Date Of Service Issued": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
                "Date Of Service Claimed": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
                "Incentive Amount": x.voucher.incentive[0].amount || '---',
                "Proof Of Service": proofServiceObject,
                "Proof Of Service Claimed(For FDC Only)": proofServiceClaimedObject
            }
        });

        const workSheet = XLSX.utils.json_to_sheet(dataToExport);
        const workBook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
        XLSX.writeFile(workBook, "PATIENT LIST OF INVOICES GENERATED.xlsx");
    }

    exportTableRejectedVoucherUserwise() {
        const dataToExport = this.dataSourceRejectedVoucher.data.map((x) => {
            let proofOfService;
            let proofServiceObject;
            let proofServiceClaimedObject;
            if (x.voucherId == '5ff565416dede21747a682b9' && x.CXRUpload == true) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "View")`,
                }
            }
            if (x.voucherId == '5ff5659f6dede21747a682bd' && x.file) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "FDC Issue")`,
                }
            }
            if (x.voucherId == '5ff5659f6dede21747a682bd' && x.FDCRedeemedFile) {
                proofOfService = `${environment.apiEndPoint}containers/${x.FDCRedeemedFile.container}/download/${x.FDCRedeemedFile.modifiedFileName}`;
                proofServiceClaimedObject = {
                    f: `=HYPERLINK("${proofOfService}", "FDC Claim")`,
                }
            }
            if (x.voucherId == '602127ad6dede214bc03966c' && x.file) {
                proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
                proofServiceObject = {
                    f: `=HYPERLINK("${proofOfService}", "View")`,
                }
            }


            return {
                "Patient's Name": x.patient ? `${x.patient.firstName} ${x.patient.surName}` : '---',
                "mobile": x.patient.mobile || '---',
                "Aditya ID": x.patient.adityaId || '---',
                "Nikshay ID": x.patient.nikshayId || '---',
                "Voucher Name": x.voucher.name || '---',
                "Voucher Code": x.voucherCode || '---',
                "Date Of Service Issued": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
                "Date Of Service Claimed": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
                "Incentive Amount": x.voucher.incentive[0].amount || '---',
                "Voucher Rejected At": x.voucherRejectedByPTAt ? new DatePipe('en-US').transform(new Date(x.voucherRejectedByPTAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
                "Rejection Remarks": x.voucherRejectedByPTRemarks || '---',
                "Proof Of Service": proofServiceObject,
                "Proof Of Service Claimed(For FDC Only)": proofServiceClaimedObject
            }
        });

        const workSheet = XLSX.utils.json_to_sheet(dataToExport);
        const workBook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
        XLSX.writeFile(workBook, "LIST OF REJECTED VOUCHERS.xlsx");
    }

}
