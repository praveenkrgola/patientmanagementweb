import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FinanceDashboardComponent } from "./finance-dashboard/finance-dashboard.component";
import { InvoiceComponent } from "./invoice/invoice.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ThemeModule } from "../../theme/theme.module";
import { CoreModule } from "../../../../../src/app/core/core.module";
import { PartialsModule } from "../../partials/partials.module";
import { RouterModule } from "@angular/router";
import { ErrorPageComponent } from "../../theme/content/error-page/error-page.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthTokenInterceptor } from "../../../../../src/services/util/interceptors/auth-token.interceptor";
import { FinanceComponent } from "./finance.component";
import { MatButtonModule, MatCheckboxModule, MatProgressSpinnerModule, MatSelectModule, MatTooltipModule } from "@angular/material";
import { SharedModule } from "../shared/shared.module";
import { FunnelChartComponent } from './finance-dashboard/funnel-chart/funnel-chart.component';
import { AmountDueByPaymentTypeComponent } from './finance-dashboard/amount-due-by-payment-type/amount-due-by-payment-type.component';
import { AmountDueByUserTypeComponent } from './finance-dashboard/amount-due-by-user-type/amount-due-by-user-type.component';
import { PieChartComponent } from './finance-dashboard/pie-chart/pie-chart.component';

@NgModule({
	declarations: [
		FinanceDashboardComponent,
		InvoiceComponent,
		FinanceComponent,
		FunnelChartComponent,
		AmountDueByPaymentTypeComponent,
		AmountDueByUserTypeComponent,
		PieChartComponent,
	],
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		ThemeModule,
		ReactiveFormsModule,
		FormsModule,
		MatButtonModule,
		MatTooltipModule,
		MatSelectModule,
		MatProgressSpinnerModule,
		SharedModule,
		MatCheckboxModule,
		RouterModule.forChild([ 
			{
				path: "",
				component: FinanceComponent,
				children: [
					{
						path: "",
						redirectTo: "finance-dashboard",
						pathMatch: "full",
					},
					{
						path: "finance-dashboard",
						component: FinanceDashboardComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "invoice",
						component: InvoiceComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "error/403",
						component: ErrorPageComponent,
						data: {
							type: "error-v6",
							code: 403,
							title: "403... Access forbidden",
							desc:
								"Looks like you don't have permission to access for requested page.<br> Please, contact administrator",
						},
					},
					{ path: "error/:type", component: ErrorPageComponent },
					{
						path: "",
						redirectTo: "finance",
						pathMatch: "full",
					},
					{
						path: "**",
						redirectTo: "finance",
						pathMatch: "full",
					},
				],
			},
		]),
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthTokenInterceptor,
			multi: true,
		},
	],
})
export class FinanceModule {}
