import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	Inject,
	OnInit,
	ViewChild,
} from "@angular/core";
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import {
	MatDialog,
	MatDialogRef,
	MatPaginator,
	MatSort,
	MatTableDataSource,
	MAT_DIALOG_DATA,
} from "@angular/material";
import { InvoiceHistoryService } from "../../../../../../src/services/invoice-history/invoice-history.service";
import Swal from "sweetalert2";
import { ReferralMasterService } from "../../../../../../src/services/referral-master/referral-master.service";
import { ContainerService } from "../../../../../../src/services/container/container.service";
import { SelectionModel } from "@angular/cdk/collections";
import { PatientInfoComponent } from "../../shared/patient-info/patient-info.component";
import { InvoiceRejectedVoucherService } from "../../../../../../src/services/invoice-rejected-voucher/invoice-rejected-voucher.service";
import { forkJoin } from "rxjs";
import { DatePipe } from "@angular/common";
import * as XLSX from "xlsx";
import { ListMasterService } from "../../../../../../src/services/list-master/list-master.service";
import { api } from '../../../../../../src/services/util/api';
import { environment } from "../../../../../../src/environments/environment";
@Component({
	selector: 'kt-invoice-report',
	templateUrl: './invoice-report.component.html',
	styleUrls: ['./invoice-report.component.scss']
})
export class InvoiceReportComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.getItem("user"));
	invoiceForm: FormGroup;
	// Mat-TABLE
	@ViewChild("paginator", { static: false }) paginator: MatPaginator;
	@ViewChild("paginatoruserwise", { static: false }) paginatorUserWise: MatPaginator;
	@ViewChild("rejectedVouchersPaginator", { static: false }) rejectedVouchersPaginator: MatPaginator;

	@ViewChild("sort", { static: true }) sort: MatSort;
	@ViewChild("sort2", { static: true }) sort2: MatSort;
	@ViewChild("sort3", { static: true }) sort3: MatSort;

	@ViewChild("invoicesTable", { static: false }) invoicesTable: ElementRef;
	months = [
		{ key: "January", value: 1 },
		{ key: "February", value: 2 },
		{ key: "March", value: 3 },
		{ key: "April", value: 4 },
		{ key: "May", value: 5 },
		{ key: "June", value: 6 },
		{ key: "July", value: 7 },
		{ key: "August", value: 8 },
		{ key: "September", value: 9 },
		{ key: "October", value: 10 },
		{ key: "November", value: 11 },
		{ key: "December", value: 12 },
	];
	years = [];
	displayedColumns: string[] = [
		"sn",
		"providerName",
		"providerType",
		"mappedFieldOfficer",
		"month",
		"year",
		"totalVouchers",
		"annualAmountEarned",
		"totalPayableAmount",
		// "voucherAcceptedByPT",
		// "voucherAcceptedAmountByPT",
		// "voucherRejectedByPT",
		// "voucherRejectedAmountByPT",
		"lastMonthRejectedVoucher",
		"lastMonthRejectedVoucherAmount",
		"rejectedVoucherAcceptedByPT",
		"rejectedVoucherAcceptedAmountByPT",
		"billNumber",
		"billDate",
		"processedByPT",
		"processedByPTAt",
		"processedByPTBy",
		"processedByFT", // Yes or No
		"actionTakenByFT", // accept or reject
		"processedByFTAt",
		"processedByFTBy",
		"processedByFTRemarks",
		"totalAmountPayable",
		"PDFInvoice",
		"accountNumber",
		"IFSCCode",
		"PANNumber",
		"bankAccountPhoto",
		"PANCard",
	];
	displayedColumnsUserWise: string[] = [
		"sn",
		"patientName",
		"mobile",
		"adityaId",
		"nikshayId",
		"voucherName",
		"voucherCode",
		"voucherCreatedAt",
		"voucherRedeemedAt",
		"incentiveAmount",
		"proofOfService",
	];
	rejectedVouchersDisplayedColumns: string[] = [
		"sn",
		"patientName",
		"mobile",
		"adityaId",
		"nikshayId",
		"voucherName",
		"voucherCode",
		"month",
		"year",
		"incentiveAmount",
		"voucherCreatedAt",
		"voucherRedeemedAt",
		"voucherRejectedAt",
		"rejectionRemarks",
		"oldProofOfService",
		"newProofOfService",
	];
	selection = new SelectionModel(true, []);
	selectionRejected = new SelectionModel(true, []);
	dataSource: MatTableDataSource<any>;
	dataSourceUserWise: MatTableDataSource<any>;
	showUserDetailsTable: boolean = false;
	showUserWiseDetailedTable: boolean = false;
	selectedUser: any;
	showProcessing: boolean = false;
	rejectedVouchersdataSource: MatTableDataSource<any>;
	showRejectedVouchersDetailedTable: boolean = false;
	constructor(
		private _fb: FormBuilder,
		private _invoiceHistoryService: InvoiceHistoryService,
		private _referralMasterService: ReferralMasterService,
		private _containerService: ContainerService,
		private _listMasterService: ListMasterService,
		private _cdr: ChangeDetectorRef,
		public dialog: MatDialog,
		private _invoiceRejectedVoucherService: InvoiceRejectedVoucherService,
	) {
		this.createForm();
	}
	createForm() {
		this.invoiceForm = this._fb.group({
			month: new FormControl("", Validators.required),
			year: new FormControl("", Validators.required),
		});
	}
	ngOnInit() {
		let counter = 2;
		let currentYear = new Date().getFullYear();
		for (let i = 0; i < counter; i++) {
			this.years.push({
				key: currentYear,
			});
			currentYear--;
		}
	}

	getAllGeneratedInvoices() {
		this.showProcessing = true;
		this.showUserWiseDetailedTable = false;
		this.showRejectedVouchersDetailedTable = false;
		const filter = {
			where: {
				month: this.invoiceForm.value.month,
				year: this.invoiceForm.value.year,
				// invoiceStatus: {
				// 	nin: [
				// 		"approvedByPT",
				// 		"rejectedByPT",
				// 		"approvedByFT",
				// 		"rejectedByFT",
				// 	],
				// },
			},
			include: [
				{
					relation: "user",
					scope: {
						include: ["fieldOfficer", "panCard", "bankAccountPhoto"],
					},
				},
				{
					relation: "actionTakenByPT"
				},
				{
					relation: "actionTakenByFT"
				}
			],
		};
		const date = new Date(`${this.invoiceForm.value.month}-1-${this.invoiceForm.value.year}`).setHours(0, 0, 0, 0);

		const rejectedFilter = {
			invoiceRejectedDate: date,
		};

		const allInvoicesHistory = this._invoiceHistoryService.getInvoiceHistory(
			filter
		);
		const getAllRejectedVouchers = this._invoiceRejectedVoucherService.getRejectedVoucherList(
			rejectedFilter
		);
		forkJoin([allInvoicesHistory, getAllRejectedVouchers]).subscribe(
			([data1, data2]: any) => {
				this.showProcessing = false;
				if (data1.length == 0) {
					const Toast = Swal.mixin({
						toast: true,
						position: "center",
						showConfirmButton: false,
						timer: 4000,
						timerProgressBar: true,
					});
					Toast.fire({
						icon: "error",
						title: `No Invoice Found !!`,
					});
					this.showUserDetailsTable = false;
					this._cdr.detectChanges();
				} else {
					const updatedData = data1.map((data1Res) => {
						
						let obj = {
							id: data1Res.id,
							notificationReferralId: data1Res.notificationReferralId ? data1Res.notificationReferralId : [],
							reimbursementFDCReferralId: data1Res.reimbursementFDCReferralId ? data1Res.reimbursementFDCReferralId : [],
							reimbursementXRAYReferralId: data1Res.reimbursementXRAYReferralId ? data1Res.reimbursementXRAYReferralId : [],
							reimbursementDOCTORReferralId: data1Res.reimbursementDOCTORReferralId ? data1Res.reimbursementDOCTORReferralId : [],

							name: data1Res.user.name,
							designationName: data1Res.user.designationName,
							month: data1Res.month,
							year: data1Res.year,
							invoiceId: data1Res.id,
							userId: data1Res.userId,
							fieldOfficerName: data1Res.user.fieldOfficer ? data1Res.user.fieldOfficer.name : "---",
							billDate: data1Res.billDate ? new DatePipe("en-US").transform(new Date(data1Res.billDate), "dd-MM-yyyy hh:mm:ss a") : "----",
							billNumber: data1Res.invoiceBillNumber,
							pdfLink: data1Res.pdfLink,
							invoiceAmount: data1Res.invoiceAmount,
							currMonthVoucherRejectedAmountByPT: data1Res.currMonthVoucherRejectedAmountByPT || 0,
							voucherAcceptedAmountByPT: data1Res.voucherAcceptedAmountByPT || 0,
							rejectedVoucherIdsAcceptedByPT: data1Res.rejectedVoucherAcceptedByPT ? data1Res.rejectedVoucherAcceptedByPT : [],
							rejectedVoucherAcceptedByPT: data1Res.rejectedVoucherAcceptedByPT ? data1Res.rejectedVoucherAcceptedByPT.length : 0,
							rejectedVoucherAcceptedAmountByPT: data1Res.rejectedVoucherAcceptedAmountByPT || 0,
							rejectedVoucherRejectedAmountByPT: data1Res.rejectedVoucherRejectedAmountByPT || 0,
							totalVouchers: data1Res.notificationTotalVoucher + data1Res.reimbursementTotalVoucher + (data1Res.invoiceRejectedVoucherId ? data1Res.invoiceRejectedVoucherId.length : 0),
							// bankName: "----",
							accountNumber: data1Res.user.providerBankAccountNumber,
							IFSCCode: data1Res.user.providerBankIFSCCode,
							PANNumber: data1Res.user.providerPANCardNumber,
							bankAccountPhoto: data1Res.user.bankAccountPhoto,
							PANCard: data1Res.user.panCard,
							actionTakenByPTId: data1Res.actionTakenByPTId,
							actionTakenByFTId: data1Res.actionTakenByFTId,
							actionTakenByPTName: data1Res.actionTakenByPT ? data1Res.actionTakenByPT.name : "----",
							actionTakenByFTName: data1Res.actionTakenByFT ? data1Res.actionTakenByFT.name : "----",
							voucherAcceptedByPTAt: data1Res.voucherAcceptedByPTAt ? data1Res.voucherAcceptedByPTAt : undefined,
							voucherAcceptedByFTAt: data1Res.voucherAcceptedByFTAt ? data1Res.voucherAcceptedByFTAt: "",
							voucherRejectedByFTAt: data1Res.voucherRejectedByFTAt,
							invoiceRejectedByFTRemarks: data1Res.invoiceRejectedByFTRemarks ? data1Res.invoiceRejectedByFTRemarks : "----",
							totalPayableAmount: data1Res.totalPayableAmount,
							invoiceStatus: data1Res.invoiceStatus,
						};

						const rejectedVoucherFoundIndex = data2.findIndex(
							(i) =>
								i.userId.toString() ===
								data1Res.user.id.toString()
						);
						if (rejectedVoucherFoundIndex !== -1) {
							const rejData = data2[rejectedVoucherFoundIndex];
							obj["lastMonthRejectedVoucher"] =
								rejData.totalVoucher;
							obj["lastMonthRejectedVoucherAmount"] =
								rejData.totalAmount;
							obj["invoiceRejectedVoucherId"] =
								rejData.invoiceRejectedVoucherId;
						}

						return obj;
					});
					this.showUserDetailsTable = true;
					updatedData.sort((a, b) => a.name.localeCompare(b.name));
					this.dataSource = new MatTableDataSource(updatedData);
					setTimeout(() => {
						this.dataSource.paginator = this.paginator;
						this.dataSource.sort = this.sort;
					}, 200);
					this.scroll("#invoices-table");
					this._cdr.detectChanges();
				}
			}
		);
	}
	getDetailedInfoUserWise(data) {
		this.showProcessing = true;
		this.selection.clear();
		this.selectedUser = data;
		let referralIds = [];
		referralIds = referralIds
			.concat(
				this.selectedUser.notificationReferralId,
				this.selectedUser.reimbursementFDCReferralId,
				this.selectedUser.reimbursementXRAYReferralId,
				this.selectedUser.reimbursementDOCTORReferralId,
				this.selectedUser.rejectedVoucherIdsAcceptedByPT
			)
			.filter(Boolean);
		if (data.voucherRejectedByPT && data.voucherRejectedByPT.length > 0) {
			referralIds = referralIds.filter(
				(el) => !data.voucherRejectedByPT.includes(el)
			);
		}
		const filter = {
			where: {
				_id: { inq: referralIds },
				// invoiceStatus: {
				// 	nin: ["voucherAcceptedByPT", "voucherRejectedByPT"],
				// },
			},
			include: [
				{
					relation: "patient",
					scope: {},
				},
				{
					relation: "file",
				},
				{
					relation: "FDCRedeemedFile",
				},
				{
					relation: "voucher",
					scope: {
						include: ["incentive"],
					},
				},
				{
					relation: "clinicVisitRedeemedFile"
				}
			],
		};
		const rejectedFilter = {
			where: {
				_id: { inq: data.invoiceRejectedVoucherId },
			},
			include: [
				{
					relation: "user",
				},
				{
					relation: "patient",
				},
				{
					relation: "referral",
					scope: {
						include: ["file", "FDCRedeemedFile", "clinicVisitRedeemedFile"],
					},
				},
				{
					relation: "file",
				},
				{
					relation: "voucher",
					scope: {
						include: ["incentive"],
					},
				}
			],
		};
		const allVouchers = this._referralMasterService.getReferralData(filter);
		const allRejectedVouchers = this._invoiceRejectedVoucherService.getRejectedVoucherDetails(rejectedFilter);
		forkJoin([allVouchers, allRejectedVouchers]).subscribe(
			([data1, data2]: any) => {
				if (data1.length > 0) {
					this.showProcessing = false;
					this.dataSourceUserWise = new MatTableDataSource(data1);
					setTimeout(() => {
						this.dataSourceUserWise.paginator = this.paginatorUserWise;
						this.dataSourceUserWise.sort = this.sort2;
					}, 200);
					this.showUserWiseDetailedTable = true;
					this.scroll("#userWiseDetailedTable");
					this._cdr.detectChanges();
				}
				if (data2.length > 0) {
					this.showProcessing = false;
					this.rejectedVouchersdataSource = new MatTableDataSource(data2);
					setTimeout(() => {
						this.rejectedVouchersdataSource.paginator = this.rejectedVouchersPaginator;
						this.rejectedVouchersdataSource.sort = this.sort3;
					}, 200);
					this.showRejectedVouchersDetailedTable = true;
					data1.legth > 0
						? this.scroll("#userWiseDetailedTable")
						: this.scroll("#userWiseRejectedTable");
					this._cdr.detectChanges();
				}
			}
		);
		this._cdr.detectChanges();
	}

	downloadFile(container, modifiedFileName) {
		this._containerService
			.downloadFile(container, modifiedFileName)
			.subscribe((res) => {
				var fileURL = window.URL.createObjectURL(res);
				// let tab = window.open();
				// tab.location.href = fileURL;
				window.open(
					fileURL,
					"Proof",
					"toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
				);
			});
	}

	getPatientDetail(row) {
		const dialogRef = this.dialog.open(PatientInfoComponent, {
			data: row,
		});

		dialogRef.afterClosed().subscribe((result) => {
			console.log(`Dialog result: ${result}`);
		});
	}

	scroll(elem: string) {
		setTimeout(() => {
			document
				.querySelector(elem)
				.scrollIntoView({ behavior: "smooth", block: "start" });
		}, 100);
	}
	openInvoice(fileURL) {
		window.open(
			fileURL,
			"Proof",
			"toolbar=yes, resizable=yes, scrollbars=yes, width=600, height=400, top=200 , left=500"
		);
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	applyFilterDataSourceUserWise(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSourceUserWise.filter = filterValue.trim().toLowerCase();

		if (this.dataSourceUserWise.paginator) {
			this.dataSourceUserWise.paginator.firstPage();
		}
	}

	exportDataSourceTable() {
		let dataToExport = this.dataSource.data.map((x) => {
			const obj = {
				Name: x.name || "---",
				"Designation Name": x.designationName || "---",
				Month: x.month || "---",
				Year: x.year || "---",
				"Field Officer Name": x.fieldOfficerName || "---",
				"Bill Number": x.billNumber || "---",
				"Bill Date": x.billDate || "---",
				"Invoice Amount": x.invoiceAmount || "---",
				"Total Vouchers": x.totalVouchers || "---",
				"Rejected Voucher Accepted By PT": x.rejectedVoucherAcceptedByPT,
				"Rejected Voucher Accepted Amount": x.rejectedVoucherAcceptedAmountByPT,
				"Last Month Rejected Voucher": x.lastMonthRejectedVoucher ? x.lastMonthRejectedVoucher : "----",
				"Last Month Rejected Voucher Amount": x.lastMonthRejectedVoucherAmount ? x.lastMonthRejectedVoucherAmount : 0,
				"Processed By PT": x.actionTakenByPTId ? "Yes" : "No",
				"Processed By PT Date": x.voucherAcceptedByPTAt ? new DatePipe('en-US').transform(new Date(x.voucherAcceptedByPTAt), 'dd-MM-yyyy hh:mm:ss a') : '---',

				"Processed By PT Name": x.actionTakenByPTName ? x.actionTakenByPTName : "----",
				"Processed By FT": x.actionTakenByFTId ? "Yes" : "No",
				"Action Taken By FT": x.invoiceStatus === "approvedByFT" ? "Accepted" : (x.invoiceStatus === "rejectedByFT" ? "Rejected" : "----"),
				"Total Amount Payable": x.totalPayableAmount ? x.totalPayableAmount : 0,
				"Processed By FT Date": x.voucherAcceptedByFTAt ? new DatePipe('en-US').transform(new Date(x.voucherAcceptedByFTAt), 'dd-MM-yyyy hh:mm:ss a') : '---',

				"Processed By FT Name": x.actionTakenByFTName ? x.actionTakenByFTName : "----",
				"Processed By FT Remarks": x.invoiceRejectedByFTRemarks ? x.invoiceRejectedByFTRemarks : "----",
				"Bank Name": x.bankName ? x.bankName : "----",
				"Account Number": x.accountNumber ? x.accountNumber : "----",
				"IFSC Code": x.IFSCCode ? x.IFSCCode : "----",
				"PAN Number": x.PANNumber ? x.PANNumber : "----",
				"Invoice Link": {
					f: `=HYPERLINK("${x.pdfLink}", "Invoice PDF")`,
				},
			}
			if (x.bankAccountPhoto) {
				obj["Bank Account Photo"] = {
					f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.bankAccountPhoto.container}/download/${x.bankAccountPhoto.modifiedFileName}", "PHOTO LINK")`,
				}
			}
			if (x.PANCard) {
				obj["PAN Card"] = {
					f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.PANCard.container}/download/${x.PANCard.modifiedFileName}", "PAN Card LINK")`,
				}
			}
			return obj;
		});

		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Invoices Details.xlsx");
	}

	exportTableUserWise() {
		const dataToExport = this.dataSourceUserWise.data.map((x) => {
			let proofOfService;
			let proofServiceObject;
			let proofServiceClaimedObject;
			if (x.voucherId == '5ff565416dede21747a682b9' && x.CXRUpload == true) {
				proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				proofServiceObject = {
					f: `=HYPERLINK("${proofOfService}", "View")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.file) {
				proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				proofServiceObject = {
					f: `=HYPERLINK("${proofOfService}", "FDC Issue")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.FDCRedeemedFile) {
				proofOfService = `${environment.apiEndPoint}containers/${x.FDCRedeemedFile.container}/download/${x.FDCRedeemedFile.modifiedFileName}`;
				proofServiceClaimedObject = {
					f: `=HYPERLINK("${proofOfService}", "FDC Claim")`,
				}
			}
			if (x.voucherId == '602127ad6dede214bc03966c' && x.file) {
				proofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				proofServiceObject = {
					f: `=HYPERLINK("${proofOfService}", "View")`,
				}
			}


			return {
				"Patient's Name": x.patient ? `${x.patient.firstName} ${x.patient.surName}` : '---',
				"mobile": x.patient.mobile || '---',
				"Aditya ID": x.patient.adityaId || '---',
				"Nikshay ID": x.patient.nikshayId || '---',
				"Voucher Name": x.voucher.name || '---',
				"Voucher Code": x.voucherCode || '---',
				"Date Of Service Issued": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
				"Date Of Service Claimed": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
				"Incentive Amount": x.voucher.incentive[0].amount || '---',
				"Proof Of Service": proofServiceObject,
				"Proof Of Service Claimed(For FDC Only)": proofServiceClaimedObject,
				"Voucher Rejected By Program Team": x.invoiceStatus == 'voucherRejectedByPT' ? "Yes" : "No"
			}
		});

		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "PATIENT LIST OF INVOICES GENERATED.xlsx");
	}

	exportTableRejectedVoucherUserwise() {
		const dataToExport = this.rejectedVouchersdataSource.data.map((x) => {
			let oldProofOfService;
			let oldProofServiceObject;
			let oldProofServiceClaimedObject;

			if (x.voucherId == '5ff565416dede21747a682b9' && x.CXRUpload == true) {
				oldProofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				oldProofServiceObject = {
					f: `=HYPERLINK("${oldProofOfService}", "View")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.file) {
				oldProofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				oldProofServiceObject = {
					f: `=HYPERLINK("${oldProofOfService}", "FDC Issue")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.FDCRedeemedFile) {
				oldProofOfService = `${environment.apiEndPoint}containers/${x.FDCRedeemedFile.container}/download/${x.FDCRedeemedFile.modifiedFileName}`;
				oldProofServiceClaimedObject = {
					f: `=HYPERLINK("${oldProofOfService}", "FDC Claim")`,
				}
			}
			if (x.voucherId == '602127ad6dede214bc03966c' && x.file) {
				oldProofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				oldProofServiceObject = {
					f: `=HYPERLINK("${oldProofOfService}", "View")`,
				}
			}

			//-------------New Proof------------
			let newProofOfService;
			let newProofServiceObject;
			let newProofServiceClaimedObject;

			if (x.voucherId == '5ff565416dede21747a682b9' && x.referral.CXRUpload == true) {
				newProofOfService = `${environment.apiEndPoint}containers/${x.referral.file.container}/download/${x.referral.file.modifiedFileName}`;
				newProofServiceObject = {
					f: `=HYPERLINK("${newProofOfService}", "View")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.referral.file) {
				newProofOfService = `${environment.apiEndPoint}containers/${x.referral.file.container}/download/${x.referral.file.modifiedFileName}`;
				newProofServiceObject = {
					f: `=HYPERLINK("${newProofOfService}", "FDC Issue")`,
				}
			}
			if (x.voucherId == '5ff5659f6dede21747a682bd' && x.referral.FDCRedeemedFile) {
				newProofOfService = `${environment.apiEndPoint}containers/${x.referral.FDCRedeemedFile.container}/download/${x.referral.FDCRedeemedFile.modifiedFileName}`;
				newProofServiceClaimedObject = {
					f: `=HYPERLINK("${newProofOfService}", "FDC Claim")`,
				}
			}
			if (x.voucherId == '601152466dede219e6eac8f2' && x.referral.file) {
				newProofOfService = `${environment.apiEndPoint}containers/${x.referral.file.container}/download/${x.referral.file.modifiedFileName}`;
				newProofServiceObject = {
					f: `=HYPERLINK("${newProofOfService}", "View")`,
				}
			}
			if (x.voucherId == '602127ad6dede214bc03966c' && x.file) {
				newProofOfService = `${environment.apiEndPoint}containers/${x.file.container}/download/${x.file.modifiedFileName}`;
				newProofServiceObject = {
					f: `=HYPERLINK("${newProofOfService}", "View")`,
				}
			}



			return {
				"Patient's Name": x.patient ? `${x.patient.firstName} ${x.patient.surName}` : '---',
				"mobile": x.patient.mobile || '---',
				"Aditya ID": x.patient.adityaId || '---',
				"Nikshay ID": x.patient.nikshayId || '---',
				"Voucher Name": x.voucher.name || '---',
				"Voucher Code": x.voucherCode || '---',
				"Month": x.month || '---',
				"yeaer": x.year || '---',
				"Date Of Service Issued": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
				"Date Of Service Claimed": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
				"Incentive Amount": x.voucher.incentive[0].amount || '---',
				"Voucher Rejected At": x.voucherRejectedByPTAt ? new DatePipe('en-US').transform(new Date(x.voucherRejectedByPTAt), 'dd-MM-yyyy hh:mm:ss a') : '---',
				"Rejection Remarks": x.voucherRejectedByPTRemarks || '---',

				"Old Proof Of Service": oldProofServiceObject,
				"Old Proof Of Service Claimed(For FDC Only)": oldProofServiceClaimedObject,

				"New Proof Of Service": newProofServiceObject,
				"New Proof Of Service Claimed(For FDC Only)": newProofServiceClaimedObject
			}
		});

		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "LIST OF REJECTED VOUCHERS.xlsx");
	}
}