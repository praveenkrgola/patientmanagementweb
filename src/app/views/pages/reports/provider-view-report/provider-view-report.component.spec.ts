import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderViewReportComponent } from './provider-view-report.component';

describe('ProviderViewReportComponent', () => {
  let component: ProviderViewReportComponent;
  let fixture: ComponentFixture<ProviderViewReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderViewReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderViewReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
