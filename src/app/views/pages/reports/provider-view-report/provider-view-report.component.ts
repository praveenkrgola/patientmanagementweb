import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ReferralMasterService } from '../../../../../../src/services/referral-master/referral-master.service';
import * as XLSX from "xlsx";
import { FormControl, FormGroup } from '@angular/forms';
import { object } from '@amcharts/amcharts4/core';

@Component({
  selector: 'kt-provider-view-report',
  templateUrl: './provider-view-report.component.html',
  styleUrls: ['./provider-view-report.component.scss']
})
export class ProviderViewReportComponent implements OnInit {

  // Chemist data
  chemistForm: FormGroup;
  empChemistForm: FormGroup;
  labForm: FormGroup;
  formalProviderForm: FormGroup;
  ayushProviderForm: FormGroup;


  chemistDataSource = new MatTableDataSource();
  @ViewChild("chemistPaginator", { static: false }) chemistPaginator: MatPaginator;
  @ViewChild("chemistMatSort", { static: false }) chemistSort: MatSort;

  chemistDataDisplayedColumns = [
    "providerName",
    "NikshayHFID",
    "providerDesignation",
    "fieldOfficer",
    "issued",
    "claimed",
    "cxrRedeemRatio",
    "cxrSuggestivePercent",
    "isPatientEnrolled"
  ];
  showChemistTable: boolean = false;

  // Empanelled Chemist data
  empChemistDataSource = new MatTableDataSource();
  @ViewChild("empChemistPaginator", { static: false }) empChemistPaginator: MatPaginator;
  @ViewChild("empChemistMatSort", { static: false }) empChemistSort: MatSort;

  empChemistDataDisplayedColumns = [
    "providerName",
    "NikshayHFID",
    "providerDesignation",
    "fieldOfficer",
    "claimed",
    "chemistNotified",
    "uniquePatientsOnFDC"
  ]
  showEmpChemistTable: boolean = false;

  // X-Ray Lab data
  labDataSource = new MatTableDataSource();
  @ViewChild("labPaginator", { static: false }) labPaginator: MatPaginator;
  @ViewChild("labMatSort", { static: false }) labSort: MatSort;

  labDataDisplayedColumns = [
    "name",
    "NikshayHFID",
    "fo",
    "cxrClaimed",
    "consultIssued",
    "consultClaimed",
    "isPatientEnrolled"
  ]
  showLabTable: boolean = false;

  // Formal Provider data
  formalProviderDataSource = new MatTableDataSource();
  @ViewChild("formalProviderPaginator", { static: false }) formalProviderPaginator: MatPaginator;
  @ViewChild("formalProviderMatSort", { static: false }) formalProviderSort: MatSort;

  formalProviderDataDisplayedColumns = [
    "name",
    "NikshayHFID",
    "designation",
    "fo",
    "consultClaimed",
    "isPatientEnrolled",
    "cxrIssued",
    "cxrClaimed",
    "cxrRedeemRatio",
    "cbnaatIssued",
    "cbnaatClaimed",
    "cbnaatRedeemRatio",
    "fdcIssued",
    "fdcClaimed",
    "fdcRedeemRatio"

  ]
  showFormalProviderTable: boolean = false;

    // Ayush Provider data
    ayushProviderDataSource = new MatTableDataSource();
    @ViewChild("ayushProviderPaginator", { static: false }) ayushProviderPaginator: MatPaginator;
    @ViewChild("ayushProviderMatSort", { static: false }) ayushProviderSort: MatSort;
  
    ayushProviderDataDisplayedColumns = [
      "name",
      "NikshayHFID",
      "designationName",
      "fo",
      "consultIssued",
      "consultClaimed",
      "consultRedeemRatio",
      "cxrIssued",
      "cxrClaimed",
      "cxrRedeemRatio",
      "cxrSuggestivePercent"
    ]
  showAyushProviderTable: boolean = false;
  
  constructor(
    private _referralMasterService: ReferralMasterService,
    private _cdr: ChangeDetectorRef
  ) {
    this.createForms();
   }
  createForms() {
    this.chemistForm = new FormGroup({
      fieldOfficerId: new FormControl(""),
      date: new FormControl([]),
    });
    this.empChemistForm = new FormGroup({
      fieldOfficerId: new FormControl(""),
      date: new FormControl([]),
    });
    this.labForm = new FormGroup({
      fieldOfficerId: new FormControl(""),
      date: new FormControl([]),
    });
    this.formalProviderForm = new FormGroup({
      fieldOfficerId: new FormControl(""),
      date: new FormControl([]),
    });
    this.ayushProviderForm = new FormGroup({
      fieldOfficerId: new FormControl(""),
      date: new FormControl([]),
    });
  }

  ngOnInit() {
    this.getChemistData();
    this.getEmpanelledChemistData();
    this.getXrayLabData();
    this.getFormalProviderData();
    this.getAyushProviderData();
  }
	getForm(form) {
		form = Object.entries(form).reduce(
			(a, [k, v]: any) =>
			v == null || v.length == 0 ? a : ((a[k] = v), a),
			{}
		);
		return form;
  }
  // Chemist Data
  getChemistData() {
    const query = this.getForm(this.chemistForm.value);
    this._referralMasterService.getChemistData(query).subscribe(res => {
      this.chemistDataSource = new MatTableDataSource(res);
      this.showChemistTable = true;
      setTimeout(() => {
        this.chemistDataSource.paginator = this.chemistPaginator;
        this.chemistDataSource.sort = this.chemistSort;
      }, 200);
      this._cdr.detectChanges();
    });
  }
  chemistDataFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.chemistDataSource.filter = filterValue.trim().toLowerCase();
    if (this.chemistDataSource.paginator) {
      this.chemistDataSource.paginator.firstPage();
    }
  }
  exportChemistData() {
    let dataToExport = this.chemistDataSource.data.map((x: any) => ({
      'Name': x.providerName,
      'NikshayHFID': x.NikshayHFID,
      'Designation': x.providerDesignation,
      'Field Officer': x.fieldOfficer,
      'CXR Issued': x.issued,
      'CXR Claimed': x.claimed,
      'CXR Redeem Ratio': x.cxrRedeemRatio.toFixed(2),
      'CXR Suggestive Percent': x.cxrSuggestivePercent.toFixed(2),
      'TB Enrolled Patients': x.isPatientEnrolled,
      // "User Registration Date" : new DatePipe('en-US').transform(new Date(x.createdAt), 'dd-MM-yyyy hh:mm:ss a')
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Chemist Data.xlsx");
  }

  // Empanelled Chemist Data
  getEmpanelledChemistData() {
    const query = this.getForm(this.empChemistForm.value);
    this._referralMasterService.getEmpanelledChemistData(query).subscribe(res => {
      this.empChemistDataSource = new MatTableDataSource(res);
      this.showEmpChemistTable = true;
      setTimeout(() => {
        this.empChemistDataSource.paginator = this.empChemistPaginator;
        this.empChemistDataSource.sort = this.empChemistSort;
      }, 200);
      this._cdr.detectChanges();
    });
  }
  empChemistDataFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.empChemistDataSource.filter = filterValue.trim().toLowerCase();
    if (this.empChemistDataSource.paginator) {
      this.empChemistDataSource.paginator.firstPage();
    }
  }
  exportEmpChemistData() {
    let dataToExport = this.empChemistDataSource.data.map((x: any) => ({
      'Name': x.providerName,
      'NikshayHFID': x.NikshayHFID,
      'Designation': x.providerDesignation,
      'Field Officer': x.fieldOfficer,
      'FDC VOucher Claimed': x.claimed,
      'Chemist Notified': x.chemistNotified,
      'Unique Patients On FDC': x.uniquePatientsOnFDC,
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Empanelled Chemist Data.xlsx");
  }
  // X-Ray Lab Data
  getXrayLabData() {
    const query = this.getForm(this.labForm.value);
    this._referralMasterService.getXrayLabData(query).subscribe(res => {
      this.labDataSource = new MatTableDataSource(res);
      this.showLabTable = true;
      setTimeout(() => {
        this.labDataSource.paginator = this.labPaginator;
        this.labDataSource.sort = this.labSort;
      }, 200);
      this._cdr.detectChanges();
    })
  }
  labDataFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.labDataSource.filter = filterValue.trim().toLowerCase();
    if (this.labDataSource.paginator) {
      this.labDataSource.paginator.firstPage();
    }
  }
  exportLabData() {
    let dataToExport = this.labDataSource.data.map((x: any) => ({
      'Name': x.name,
      'NikshayHFID': x.NikshayHFID,
      "Field Officer": x.fo,
      'CXR Claimed': x.cxrClaimed,
      'D Consult Issued': x.consultIssued,
      'D Consult Claimed': x.consultClaimed,
      'TB Enrolled Patients': x.isPatientEnrolled,
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Lab Data.xlsx");
  }

  // Formal Provider Data
  getFormalProviderData() {
    const query = this.getForm(this.formalProviderForm.value);
    this._referralMasterService.getFormalProviderData(query).subscribe(res => {
      this.formalProviderDataSource = new MatTableDataSource(res);
      this.showFormalProviderTable = true;
      setTimeout(() => {
        this.formalProviderDataSource.paginator = this.formalProviderPaginator;
        this.formalProviderDataSource.sort = this.formalProviderSort;
      }, 200);
      this._cdr.detectChanges();
    })
  }
  formalProviderDataFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.formalProviderDataSource.filter = filterValue.trim().toLowerCase();
    if (this.formalProviderDataSource.paginator) {
      this.formalProviderDataSource.paginator.firstPage();
    }
  }
  exportFormalProviderData() {
    let dataToExport = this.formalProviderDataSource.data.map((x: any) => ({
      'Name': x.name,
      'NikshayHFID': x.NikshayHFID,
      'Designation': x.designation,
      'Field Officer': x.fo,
      'D Consult Claimed': x.consultClaimed,
      'CXR Issued': x.cxrIssued,
      'CXR Claimed': x.cxrClaimed,
      'CXR Redeem Ratio': x.cxrRedeemRatio.toFixed(2),
      'TB Enrolled Patients': x.isPatientEnrolled,
      "CBNAAT Issued": x.cbnaatIssued,
      "CBNAAT Claimed": x.cbnaatClaimed,
      "CBNAAT Redemption Ratio": x.cbnaatRedeemRatio,
      "FDC Issued": x.fdcIssued,
      "FDC Claimed": x.fdcClaimed,
      "FDC RedeemRatio":x.fdcRedeemRatio
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Formal Provider Data.xlsx");
  }
  // Ayush Provider Report
  getAyushProviderData() {
    const query = this.getForm(this.ayushProviderForm.value);
    this._referralMasterService.getAyushProviderData(query).subscribe(res => {
      this.ayushProviderDataSource = new MatTableDataSource(res);
      this.showAyushProviderTable = true;
      setTimeout(() => {
        this.ayushProviderDataSource.paginator = this.ayushProviderPaginator;
        this.ayushProviderDataSource.sort = this.ayushProviderSort;
      }, 200);
      this._cdr.detectChanges();
    });
    this._cdr.detectChanges();
  }
  ayushProviderDataFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.ayushProviderDataSource.filter = filterValue.trim().toLowerCase();
    if (this.ayushProviderDataSource.paginator) {
      this.ayushProviderDataSource.paginator.firstPage();
    }
  }
  exportAyushProviderData() {
    let dataToExport = this.ayushProviderDataSource.data.map((x: any) => ({
      'Name': x.name,
      'NikshayHFID': x.NikshayHFID,
      'Designation': x.designationName,
      'Field Officer': x.fo,
      'D Consult Issued': x.consultIssued,
      'D Consult Claimed': x.consultClaimed,
      'D Consult Redeemption Ratio': x.consultRedeemRatio,
      'CXR Issued': x.cxrIssued,
      'CXR Claimed': x.cxrClaimed,
      'CXR Redeemption Ratio': (typeof x.cxrRedeemRatio == 'number') ? x.cxrRedeemRatio.toFixed(2) : x.cxrRedeemRatio,
      'CXR Suggestive Percent': (typeof x.cxrRedeemRatio == 'number') ? x.cxrSuggestivePercent.toFixed(2) : x.cxrSuggestivePercent,
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "AYUSH Provider Data.xlsx");
  }
  // Field Officer Filter
  fieldOfficerSelected(event:any,dataFor:string) {
    console.log(event, dataFor)
    if (dataFor === "chemist") {
      this.chemistForm.patchValue({
        fieldOfficerId: event
      });
    this.getChemistData();
    } else if (dataFor === "empchemist") {
      this.empChemistForm.patchValue({
        fieldOfficerId: event
      });
      this.getEmpanelledChemistData();
    } else if (dataFor === "lab") {
      this.labForm.patchValue({
        fieldOfficerId: event
      });
      this.getXrayLabData();
    } else if (dataFor === "formalprovider") {
      this.formalProviderForm.patchValue({
        fieldOfficerId: event
      });
      this.getFormalProviderData();
    } else if (dataFor === "ayush") {
      this.ayushProviderForm.patchValue({
        fieldOfficerId: event
      });
      this.getAyushProviderData();
    }
  }

  // Date Range Filter
  dateSelected(event:any,dataFor:string) {
    console.log(event, dataFor)
    if (dataFor === "chemist") {
      this.chemistForm.patchValue({
        date: event
      });
    this.getChemistData();
    } else if (dataFor === "empchemist") {
      this.empChemistForm.patchValue({
        date: event
      });
      this.getEmpanelledChemistData();
    } else if (dataFor === "lab") {
      this.labForm.patchValue({
        date: event
      });
      this.getXrayLabData();
    } else if (dataFor === "formalprovider") {
      this.formalProviderForm.patchValue({
        date: event
      });
      this.getFormalProviderData();
    } else if (dataFor === "ayush") {
      this.ayushProviderForm.patchValue({
        date: event
      });
      this.getAyushProviderData();
    }
  }
}
