import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { PatientMasterService } from '../../../../../services/PatientMaster/patient-master.service';
import * as XLSX from "xlsx";
import { DatePipe } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator, MatOption } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListMasterService } from '../../../../../services/list-master/list-master.service';
import Swal from 'sweetalert2';
import { ContainerService } from '../../../../../services/container/container.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-direct-notification',
  templateUrl: './direct-notification.component.html',
  styleUrls: ['./direct-notification.component.scss']
})
export class DirectNotificationComponent implements OnInit {

  @ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;

  constructor(private _patientMasterService: PatientMasterService,
    private _listMasterService: ListMasterService,
    private _containerService : ContainerService,
    private _cdr: ChangeDetectorRef
  ) { }
  registrationTypeList = [];
  filterForm: FormGroup;
  showProcessing: boolean = false;
  showTable: boolean = false;
  displayedColumns: String[] = [
    "sn",
    "RegistrationFacilityName",
    "RegistrationProviderName",
    "RegistrationProviderId",
    "RegistrationProvidertype",
    "PatientName",
    "PatientPhoneNumber",
    "PatientID",
    "PatientRegistrationDate",
    "PatientAge",
    "PatientGender",
    "TBSymptoms",
    "prescription"
  ];
  minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
  maxDate = new Date(new Date().setHours(0, 0, 0, 0));
  dataSource: MatTableDataSource<any>;

  ngOnInit() {
    this.filterForm = new FormGroup({
      registrationType: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required),

    });
    this.getRegistrationType();
  }
  getRegistrationType() {
    this._listMasterService.getStakeholder({ where: { type: "stakeholder", "name.canBeCreated": true }, order: "name.key ASC" }).subscribe(res => {
      this.registrationTypeList = res;
      this._cdr.detectChanges()
    });
  }

  viewData() {
    this.showProcessing = true;
    this.showTable = false;

    const filterValue = { registrationType: this.filterForm.value.registrationType, fromDate: new Date(this.filterForm.value.date[0]).getTime(), toDate: new Date(this.filterForm.value.date[1]).getTime() }
    console.log(filterValue);

    this._patientMasterService.getDirectNotificationRegister(filterValue).subscribe(res => {
      if (res.length > 0) {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.showProcessing = false;
        this.showTable = true;
        this._cdr.detectChanges();
      } else {
        this.showProcessing = false;
        this.showTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!! No Data Found.");
      }
    }, err => {
      this.showProcessing = false;
      this.showTable = false;
      this._cdr.detectChanges();
      this.showAlert("error", "Oops!! No Data Found.");
    });
  }

  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }

  exportTable() {

    let dataToExport = this.dataSource.data.map((x) => ({
      "Registration Facility Name": x.RegistrationFacilityName,
      "Registration Provider Name": x.RegistrationProviderName,
      "Registration Provider ID": x.RegistrationProviderId || '-----',
      "Registration Provider type": x.RegistrationProvidertype || '-----',
      "Patient Name": x.PatientName,
      "Patient Phone Number": x.PatientPhoneNumber,
      "Patient ID": x.PatientID,
      "Patient Registration Date": new DatePipe('en-US').transform(new Date(x.PatientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a') || "-----",
      "Patient Gender": x.PatientGender.toUpperCase() || '-----',
      "Patient Age": x.PatientAge || '-----',
      "TB Symptoms": x.TBSymptoms.toString(),
      Prescription: {
        f: `=HYPERLINK("${environment.apiEndPoint}${x.fileURL}", "View")`,
      }      
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "Sheet1");
    XLSX.writeFile(workBook, "Direct Notification Register.xlsx");
  }


  selectSingle() {
    if (this.allRegistrationType.selected) {
      this.allRegistrationType.deselect();
      return false;
    }
    if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
      this.allRegistrationType.select();

  }
  selectAll() {
    if (this.allRegistrationType.selected) {
      this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name.value)]);
    } else {
      this.filterForm.controls.registrationType.patchValue([]);
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  downloadFile(container, modifiedFileName) {
		this._containerService.downloadFile(container, modifiedFileName).subscribe((res) => {
				var fileURL = window.URL.createObjectURL(res);
				// let tab = window.open();
				// tab.location.href = fileURL;
				window.open(
					fileURL,
					"Prescription",
					"toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
				);
			});
	}
}
