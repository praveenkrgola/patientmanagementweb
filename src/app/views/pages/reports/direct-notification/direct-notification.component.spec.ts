import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectNotificationComponent } from './direct-notification.component';

describe('DirectNotificationComponent', () => {
  let component: DirectNotificationComponent;
  let fixture: ComponentFixture<DirectNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
