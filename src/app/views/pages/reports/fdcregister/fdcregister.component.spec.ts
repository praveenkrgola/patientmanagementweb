import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FDCRegisterComponent } from './fdcregister.component';

describe('FDCRegisterComponent', () => {
  let component: FDCRegisterComponent;
  let fixture: ComponentFixture<FDCRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FDCRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FDCRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
