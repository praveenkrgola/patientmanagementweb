import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ListMasterService } from '../../../../../services/list-master/list-master.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatOption } from '@angular/material';
import { PatientMasterService } from '../../../../../services/PatientMaster/patient-master.service';
import Swal from 'sweetalert2';
import * as XLSX from "xlsx";
import { DatePipe } from '@angular/common';
import _moment from 'moment';
import { ContainerService } from '../../../../../services/container/container.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-fdcregister',
  templateUrl: './fdcregister.component.html',
  styleUrls: ['./fdcregister.component.scss']
})
export class FDCRegisterComponent implements OnInit {

  @ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;
  constructor(private _listMasterService: ListMasterService,
    private _patientMasterService: PatientMasterService,
    private _containerService: ContainerService,
    private _cdr: ChangeDetectorRef) { }
  registrationTypeList = [];
  filterForm: FormGroup;
  showProcessing: boolean = false;
  showTable: boolean = false;
  displayedColumns: String[] = [
    "sn",
    "RegistrationFacilityName",
    "RegistrationProviderName",
    "RegistrationProviderId",
    "RegistrationProvidertype",
    "PatientName",
    "PatientPhoneNumber",
    "PatientID",
    "PatientGender",
    "PatientAge",
    "PatientRegistrationDate",
    "TBSymptoms",
    "TBIndicative",
    "MobileVerified",
    "FDCVoucherIssuingProviderName",
    "FDCVoucherIssuingProviderHFID",
    "FDCVoucherIssuingFacilityName",
    "DoctorVoucherIssued",
    "DoctorVoucherClaimed",
    "FDCVoucherCode",
    "FDCVoucherIssueDate",
    "TreatmentInitiationDate",
    "Adult_Pediatric",
    "FDCIssuedFileURL",
    "FDCVoucherClaimDate",
    "FDCVoucherClaimProviderHFID",
    "FDCVoucherClaimProviderName",
    "FDCVoucherClaimFacilityName",
    "FDCRedeemedFileURL",
    "pd_0_productName",
    "pd_0_confirmWeightBand",
    "pd_0_numberOfBlister",
    "pd_0_weightBand",
    "pd_0_numberOfDays",
    "pd_1_productName",
    "pd_1_confirmWeightBand",
    "pd_1_numberOfBlister",
    "pd_1_weightBand",
    "pd_1_numberOfDays",
    "pd_2_productName",
    "pd_2_confirmWeightBand",
    "pd_2_numberOfBlister",
    "pd_2_weightBand",
    "pd_2_numberOfDays",
    "pd_3_productName",
    "pd_3_confirmWeightBand",
    "pd_3_numberOfBlister",
    "pd_3_weightBand",
    "pd_3_numberOfDays",
  ];
  minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
  maxDate = new Date(new Date().setHours(0, 0, 0, 0));
  dataSource: MatTableDataSource<any>;
  ngOnInit() {
    this.filterForm = new FormGroup({
      registrationType: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required),

    });
    this.getRegistrationType();
  }

  getRegistrationType() {
    this._listMasterService.getStakeholder({ where: { type: "stakeholder", "name.canBeCreated": true }, order: "name.key ASC" }).subscribe(res => {
      this.registrationTypeList = res;
      this._cdr.detectChanges()
    });
  }

  viewData() {

    this.showProcessing = true;
    this.showTable = false;

    const filterValue = { registrationType: this.filterForm.value.registrationType, fromDate: new Date(this.filterForm.value.date[0]).getTime(), toDate: new Date(this.filterForm.value.date[1]).getTime() }
    this._patientMasterService.getFDCRegister(filterValue).subscribe(res => {
      if (res.length > 0) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.showProcessing = false;
        this.showTable = true;
        this._cdr.detectChanges();
      } else {
        this.showProcessing = false;
        this.showTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!! No Data Found.");
      }
    }, err => {
      this.showProcessing = false;
      this.showTable = false;
      this._cdr.detectChanges();
      this.showAlert("error", "Oops!! No Data Found.");
    });
  }

  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }


  exportTable() {
    const dataToExport = this.dataSource.data.map((x) => ({
      "Registration Facility Name": x.RegistrationFacilityName,
      "Registration Provider Name": x.RegistrationProviderName || "-----",
      "Registration Provider Id": x.RegistrationProviderId  || "-----",
      "Registration Provider type": x.RegistrationProvidertype,
      "Patient Name": x.PatientName,
      "Patient Phone Number": x.PatientPhoneNumber,
      "Patient ID": x.PatientID,
      "Patient Gender": x.PatientGender,
      "Patient Age": x.PatientAge,
      "Patient Registration Date": new DatePipe('en-US').transform(new Date(x.PatientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a'),
      "TB Symptoms": x.TBSymptoms.toString(),
      "TB Indicative": x.TBIndicative,
      "Mobile Verified": x.MobileVerified,
      "FDC Voucher Issuing Provider Name": x.FDCVoucherIssuingProviderName,
      "FDC Voucher Issuing Provider HFID": x.FDCVoucherIssuingProviderHFID,
      "FDC Voucher Issuing Facility Name": x.FDCVoucherIssuingFacilityName,
      "DoctorVoucherIssued": x.DoctorVoucherIssued,
      "DoctorVoucherClaimed": x.DoctorVoucherClaimed,
      "FDC Voucher Code": x.FDCVoucherCode,
      "FDC Voucher Issue Date": new DatePipe('en-US').transform(new Date(x.FDCVoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a'),
      "Treatment Initiation Date": x.TreatmentInitiationDate ? new DatePipe('en-US').transform(new Date(x.TreatmentInitiationDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "Adult/Pediatric": x.Adult_Pediatric,
      "FDC Issued File ": {
        f: `=HYPERLINK("${environment.apiEndPoint}${x.FDCIssuedFileURL}", "View")`,
      },
      "FDC Voucher Claim Date": x.FDCVoucherClaimDate ? new DatePipe('en-US').transform(new Date(x.FDCVoucherClaimDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "FDCVoucher Claim Provider HFID": x.FDCVoucherClaimProviderHFID || '-----',
      "FDC Voucher Claim Provider Name": x.FDCVoucherClaimProviderName || '-----',
      "FDC Voucher Claim Facility Name": x.FDCVoucherClaimFacilityName || '-----',
      "FDC Redeemed File": x.FDCRedeemedFileURL ? {
        f: `=HYPERLINK("${environment.apiEndPoint}${x.FDCRedeemedFileURL}", "View")`,
      } : '-----',
      "Product 1 Name": x.pd_0_productName || '-----',
      "Product 1 Confirm Weight Band": x.pd_0_confirmWeightBand || '-----',
      "Product 1 Number Of Blister": x.pd_0_numberOfBlister || '-----',
      "Product 1 WeightBand": x.pd_0_weightBand || '-----',
      "Product 1 Number Of Days": x.pd_0_numberOfDays || '-----',
      "Product 2 Name": x.pd_1_productName || '-----',
      "Product 2 Confirm Weight Band": x.pd_1_confirmWeightBand || '-----',
      "Product 2 Number Of Blister": x.pd_1_numberOfBlister || '-----',
      "Product 2 Weight Band": x.pd_1_weightBand || '-----',
      "Product 2 Number Of Days": x.pd_1_numberOfDays || '-----',
      "Product 3 Name": x.pd_2_productName || '-----',
      "Product 3 Confirm Weight Band": x.pd_2_confirmWeightBand || '-----',
      "Product 3 Number Of Blister": x.pd_2_numberOfBlister || '-----',
      "Product 3 Weight Band": x.pd_2_weightBand || '-----',
      "Product 3 Number Of Days": x.pd_2_numberOfDays || '-----',
      "Product 4 Name": x.pd_3_productName || '-----',
      "Product 4 Confirm Weight Band": x.pd_3_confirmWeightBand || '-----',
      "Product 4 Number Of Blister": x.pd_3_numberOfBlister || '-----',
      "Product 4 Weight Band": x.pd_3_weightBand || '-----',
      "Product 4 number Of Days": x.pd_3_numberOfDays || '-----',
    }));


    let workSheet = XLSX.utils.json_to_sheet(dataToExport);
   
    const widthArray: any = Object.entries(dataToExport[0]).map(([key, value]) => ({ wch: key.length }));

    workSheet["!cols"] = widthArray;
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "FDC Register .xlsx", { Props: { Title: "FDC Register", Subject: "FDC Register" } });
  }


  selectSingle() {
    if (this.allRegistrationType.selected) {
      this.allRegistrationType.deselect();
      return false;
    }
    if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
      this.allRegistrationType.select();

  }
  selectAll() {
    if (this.allRegistrationType.selected) {
      this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name.value)]);
    } else {
      this.filterForm.controls.registrationType.patchValue([]);
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  downloadFile(container, modifiedFileName) {
    this._containerService.downloadFile(container, modifiedFileName).subscribe((res) => {
      var fileURL = window.URL.createObjectURL(res);
      // let tab = window.open();
      // tab.location.href = fileURL;
      window.open(
        fileURL,
        "Prescription",
        "toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
      );
    });
  }
}

