import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FOActivityComponent } from './foactivity.component';

describe('FOActivityComponent', () => {
  let component: FOActivityComponent;
  let fixture: ComponentFixture<FOActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FOActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FOActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
