import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UserLoginService } from '../../../../../services/user-login/user-login.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FieldOfficerActivityService } from '../../../../../services/FieldOfficerActivity/field-officer-activity.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as XLSX from "xlsx";
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'kt-foactivity',
  templateUrl: './foactivity.component.html',
  styleUrls: ['./foactivity.component.scss']
})



export class FOActivityComponent implements OnInit {
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;

  constructor(
    private _userLoginService: UserLoginService,
    private _fieldOfficerActivityService: FieldOfficerActivityService,
    private _cdr: ChangeDetectorRef) { }
  filterForm: FormGroup;
  FOList: Object;
  showTable: boolean = false;
  displayedColumns: String[] = ["sn", "fieldOfficerName", "submissionDate", "typeOfProvider", "ableToMeetProvider", "providerName", "providerNumber", "facilityName", "facilityHFID", "objectiveOfVisit", "summaryOfInteractionWithProvider", "compounderInteractionSummary", "comments"];
  dataSource: MatTableDataSource<any>;
  minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
  maxDate = new Date(new Date().setHours(0, 0, 0, 0));
  ngOnInit() {
    this.filterForm = new FormGroup({
      fieldofficer: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required)
    });
    this.getFOList();
  }
  getFOList() {
    this._userLoginService.getList({ where: { designation: "fieldofficer" }, order: "name ASC", fields: ["id", "name"] }).subscribe(res => {
      this.FOList = res;
      this._cdr.detectChanges();
    });
  }

  viewData() {
    this.showTable = false;
    const filterValue = { fieldofficer: this.filterForm.value.fieldofficer, fromDate: new Date(this.filterForm.value.date[0]).getTime(), toDate: new Date(this.filterForm.value.date[1]).getTime() }
    this._fieldOfficerActivityService.getFOActivity(filterValue).subscribe(res => {
      if (res.length > 0) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.showTable = true;
        this._cdr.detectChanges();
      } else {
        this.showTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!! No Data Found.");
      }
    }, err => {
      this.showTable = false;
      this._cdr.detectChanges();
      this.showAlert("error", "Oops!! No Data Found.");
    });
  }

  exportTable() {

    let dataToExport = this.dataSource.data.map((x) => ({
      "Field Officer Name": x.fieldOfficerName,
      "Date of Field Activity": typeof(x.submissionDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.submissionDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
      "Type Of Provider": x.typeOfProvider,
      "Able To Meet Provider": x.ableToMeetProvider,
      "Provider/compounder Name": x.providerName,
      "Provider Number": x.providerNumber,
      "Facility Name": x.facilityName,
      "Facility HFID": x.facilityHFID,
      "Objective Of Visit": x.objectiveOfVisit,
      "Summary Of Interaction With Provider": x.summaryOfInteractionWithProvider ? x.summaryOfInteractionWithProvider.toString() : '----',
      "Compounder Interaction Summary": x.compounderInteractionSummary ? x.compounderInteractionSummary.toString() : '-----',
      "Comments": x.comments
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Field Officer Activity.xlsx");
  }

  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }

  applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

}
