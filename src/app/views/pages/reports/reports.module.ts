import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { FOActivityComponent } from './foactivity/foactivity.component';
import { ErrorPageComponent } from '../../theme/content/error-page/error-page.component';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthTokenInterceptor } from "../../../../../src/services/util/interceptors/auth-token.interceptor";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ThemeModule } from "../../theme/theme.module";
import { CoreModule } from "../../../../../src/app/core/core.module";
import { PartialsModule } from "../../partials/partials.module";
import { RouterModule } from "@angular/router";
import { MatButtonModule, MatProgressSpinnerModule, MatSelectModule, MatTooltipModule, MatIconModule, MatNativeDateModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule } from "@angular/material";
import { XrayComponent } from './xray/xray.component';
import { DoctorConsultationRegisterComponent } from './doctor-consultation-register/doctor-consultation-register.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ConfirmTBRegisterComponent } from './confirm-tbregister/confirm-tbregister.component';
import { CbnaatSctComponent } from './cbnaat-sct/cbnaat-sct.component';
import { InvoiceReportComponent } from './invoice-report/invoice-report.component';
import { DirectNotificationComponent } from './direct-notification/direct-notification.component';
import { SharedModule } from '../shared/shared.module';
import { FDCRegisterComponent } from './fdcregister/fdcregister.component';
import { ProviderViewReportComponent } from './provider-view-report/provider-view-report.component';
import { CompletePatientDetailComponent } from './complete-patient-detail/complete-patient-detail.component';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [ReportsComponent, FOActivityComponent, DoctorConsultationRegisterComponent, XrayComponent, ConfirmTBRegisterComponent, CbnaatSctComponent, InvoiceReportComponent, DirectNotificationComponent, FDCRegisterComponent, ProviderViewReportComponent, CompletePatientDetailComponent],
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatTooltipModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    RouterModule.forChild([
      {
        path: "",
        component: ReportsComponent,
        children: [
          {
            path: "foactivity",
            component: FOActivityComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "dconsultregister",
            component: DoctorConsultationRegisterComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "xray",
            component: XrayComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "confirmtbregister",
            component: ConfirmTBRegisterComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "cbnaat",
            component: CbnaatSctComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "invoice",
            component: InvoiceReportComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "directnotification",
            component: DirectNotificationComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "fdcregister",
            component: FDCRegisterComponent,
            data: { returnUrl: window.location.pathname },
          },
          {
            path: "provider-report",
            component: ProviderViewReportComponent,
            data: { returnUrl: window.location.pathname }
          },
          {
            path: "patient-detail",
            component: CompletePatientDetailComponent,
            data: { returnUrl: window.location.pathname }
          },
          {
            path: "error/403",
            component: ErrorPageComponent,
            data: {
              type: "error-v6",
              code: 403,
              title: "403... Access forbidden",
              desc:
                "Looks like you don't have permission to access for requested page.<br> Please, contact administrator",
            },
          },
          { path: "error/:type", component: ErrorPageComponent },
          {
            path: "",
            redirectTo: "reports",
            pathMatch: "full",
          },
          {
            path: "**",
            redirectTo: "reports",
            pathMatch: "full",
          },
        ],
      },
    ]),
    NgCircleProgressModule.forRoot({
      // set defaults here
      animation: false,
      responsive: true,
      renderOnClick: false,
      lazy: false,
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true,
    },
  ]
})
export class ReportsModule { }
