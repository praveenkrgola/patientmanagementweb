import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReferralMasterService } from '../../../../../../src/services/referral-master/referral-master.service';
import { ListMasterService } from '../../../../../../src/services/list-master/list-master.service';
import { MatOption, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import Swal from 'sweetalert2';
import * as XLSX from "xlsx";
import { DatePipe } from '@angular/common';

@Component({
	selector: "kt-xray",
	templateUrl: "./xray.component.html",
	styleUrls: ["./xray.component.scss"],
})
export class XrayComponent implements OnInit {
	registrationTypeList = [];
  filterForm: FormGroup;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;
  @ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
  minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
  maxDate = new Date(new Date().setHours(0, 0, 0, 0));
	displayedColumns: String[] = [
		"sn",
		"registrationFacilityName",
		"registrationProviderName",
		"registrationProviderId",
		"registrationProviderType",
		"patientName",
		"patientNumber",
		"patientId",
		"patientRegistrationDate",
		"TBSymptoms",
		"otpVerified",
		"cxrDisbursementDate",
		"cxrVoucherCode",
		"cxrRedemptionDate",
		"suggestiveOfTB",
    "cxrReportUploaded",
    "labFacilityName",
		"labProviderName",
	];
  dataSource: MatTableDataSource<any>;
  showTable: boolean = false;
  showProcessing: boolean;

	constructor(
		private _listMasterService: ListMasterService,
		private _referralMaster: ReferralMasterService,
		private _cdr: ChangeDetectorRef
	) {}
	ngOnInit() {
		this.createForm();
	}
	createForm() {
		this.filterForm = new FormGroup({
			registrationType: new FormControl(null, Validators.required),
			date: new FormControl(null, Validators.required),
		});
		this.getRegistrationType();
	}

	getRegistrationType() {
		this._listMasterService
			.getStakeholder({
				where: { type: "stakeholder", "name.canBeCreated": true },
				order: "name.key ASC",
			})
			.subscribe((res) => {
				console.log(res);
				this.registrationTypeList = res;
				this._cdr.detectChanges();
			});
  }
  selectSingle() {
    if (this.allRegistrationType.selected) {
      this.allRegistrationType.deselect();
      return false;
    }
    if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
      this.allRegistrationType.select();

  }
  selectAll() {
    if (this.allRegistrationType.selected) {
      this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name.value)]);
    } else {
      this.filterForm.controls.registrationType.patchValue([]);
    }
  }
  viewData() {
    this.showProcessing = true;
    this.showTable = false;
		const formValue = this.filterForm.value;
		const reqObject = {
			designation: formValue.registrationType,
			fromDate: formValue.date[0].setHours(0, 0, 0, 0),
			toDate: formValue.date[1].setHours(23, 59, 59, 0),
		};
		this._referralMaster.getCXRReport(reqObject).subscribe((res) => {
      if (res.length > 0) {
    this.showProcessing = false;
    this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.showTable = true;
        this._cdr.detectChanges();
      } else {
        this.showTable = false;
        this.showProcessing = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!! No Data Found.");
      }
		});
  }
  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

  exportTable() {
		let dataToExport = this.dataSource.data.map((x) => ({
			"Registration Facility Name": x.registrationFacilityName || '---' ,
			"Registration Provider Name": x.registrationProviderName || "---",
			"Registration Provider Id": x.registrationProviderId || "---",
			"Registration Provider Type": x.registrationProviderType || "---",
			"Patient Name": x.patientName || "---",
			"Patient Number": x.patientNumber || "---",
			"Patient Id": x.patientId || "---",
			"Patient Registration Date": x.cxrRedemptionDate !== '---' ? new DatePipe('en-US').transform(new Date(x.patientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"TB Symptoms": x.TBSymptoms.toString() || "---",
			"OTP Verified": x.otpVerified || "---",
			"CXR Disbursement Date": typeof(x.cxrDisbursementDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.cxrDisbursementDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"CXR Voucher Code": x.cxrVoucherCode || "---",
			"CXR Redemption Date": typeof(x.cxrRedemptionDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.cxrRedemptionDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"CXR Result (Suggestive Of TB)": x.suggestiveOfTB || "---",
			"CXR Report Uploaded": x.cxrReportUploaded || "---",
			"Lab Facility Name": x.labFacilityName || "---",
			"Lab Provider Name": x.labProviderName || "---",

		}));
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "X-Ray Details.xlsx");
	}
}
