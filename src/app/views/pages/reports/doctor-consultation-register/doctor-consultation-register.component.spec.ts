import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorConsultationRegisterComponent } from './doctor-consultation-register.component';

describe('DoctorConsultationRegisterComponent', () => {
  let component: DoctorConsultationRegisterComponent;
  let fixture: ComponentFixture<DoctorConsultationRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorConsultationRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorConsultationRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
