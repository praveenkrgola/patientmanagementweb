import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ListMasterService } from '../../../../../services/list-master/list-master.service';
import { ReferralMasterService } from '../../../../../services/referral-master/referral-master.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { MatTableDataSource, MatOption, MatPaginator, MatSort } from '@angular/material';
import * as XLSX from "xlsx";
import * as moment from "moment";
import { DatePipe } from '@angular/common';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kt-complete-patient-detail',
  templateUrl: './complete-patient-detail.component.html',
  styleUrls: ['./complete-patient-detail.component.scss']
})
export class CompletePatientDetailComponent implements OnInit {
  @ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;

  constructor(private _listMasterService: ListMasterService,
    private _referralMasterService: ReferralMasterService,
    private _cdr: ChangeDetectorRef) { }
  registrationTypeList = [];
  filterForm: FormGroup;
  progresspercent = 0;
  showProgress: boolean = false;
  showDownloadButton: boolean = false;
  completeData: any = [];
  ngOnInit() {
    this.filterForm = new FormGroup({
      registrationType: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required)
    });
    this.getRegistrationType();
  }

  getRegistrationType() {
    this._listMasterService.getStakeholder({ where: { type: "stakeholder", "name.canBeCreated": true }, order: "name.key ASC" }).subscribe(res => {
      this.registrationTypeList = res;
      this._cdr.detectChanges()
    });
  }
  selectSingle() {
    if (this.allRegistrationType.selected) {
      this.allRegistrationType.deselect();
      return false;
    }
    if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
      this.allRegistrationType.select();

  }
  selectAll() {
    if (this.allRegistrationType.selected) {
      this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name)]);
    } else {
      this.filterForm.controls.registrationType.patchValue([]);
    }
  }

  async viewData() {
    this.completeData.length = 0; //Reset
    this.showProgress = true;
    this.showDownloadButton = false;

    const selecteedRegistrationTypeValue = this.filterForm.value.registrationType.filter(rt => rt).map(rt => rt.value);
    const selecteedRegistrationTypeName = this.filterForm.value.registrationType.filter(rt => rt).map(rt => rt.key);

    for (let index = 0; index < selecteedRegistrationTypeValue.length; index++) {
      const registrationType = selecteedRegistrationTypeValue[index];
      const filterValue = { registrationType, fromDate: this.filterForm.value.date[0], toDate: this.filterForm.value.date[1] }

      this.progresspercent = Math.round(((index + 1) / (selecteedRegistrationTypeValue.length)) * 100);
      this._cdr.detectChanges();

      const registrationTypeWiseData = await this._referralMasterService.getCompletePatientDetail(filterValue);
      if (registrationTypeWiseData.length > 0) {
        this.completeData.push({ registrationType: selecteedRegistrationTypeName[index], data: registrationTypeWiseData });
      }
      this._cdr.detectChanges();
    }
    this.showProgress = false;
    if (this.completeData.length > 0) {
      this.showDownloadButton = true;
      this._cdr.detectChanges();

    } else {
      this.sweetalert("No Data Found !!", "No Data Found For Selected Filters.", "error");
      this._cdr.detectChanges();
    }
  }




  sweetalert(title, desc, icon) {
    (Swal as any).fire(`${title}`, `${desc}`, `${icon}`);
  }

  download() {
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    this.completeData.forEach(ele => {
		const dataToExport = ele.data.map((x) => {
		  console.log('data', x);
        // console.log(new Date(x.PatientRegistrationDate));
        return {
          //Registration
          "Registration Facility Name": x.RegistrationFacilityName || '---',
          "Registration Provider Name": x.RegistrationProviderName || "---",
          "Registration Provider HFID": x.RegistrationProviderId || "---",
          "Registration Provider Type": x.RegistrationProvidertype || "---",
          "Patient Name": x.PatientName || "---",
          "Patient Phone Number": x.PatientPhoneNumber || "---",
          "Patient ADITYA ID": x.PatientID || "---",
          "Patient Age": x.PatientAge || "---",
          "Patient Gender": x.PatientGender || "---",
          "Patient Registration Date": x.PatientRegistrationDate !== '---' ? new DatePipe('en-US').transform(new Date(x.PatientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "TB Symptoms": x.TBSymptoms.toString() || "---",
          "TB Indicative": x.IsTBIndicative === "true" ? "Yes" : "No",
          "OTP Verified": x.OTPVerified ? "Yes" : "No",
          //CXR
          "CXR Disbursement Date": typeof (x.CXRDisbursementDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.CXRDisbursementDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "CXR Voucher Code": x.CXRvoucherCode ? parseInt(x.CXRvoucherCode) : "-----",
          "CXR Redemption Date": typeof (x.CXRRedemptionDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.CXRRedemptionDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "CXR Result (Suggestive Of TB)": x.CXRResult || "---",
          "CXR Report Uploaded": x.CXRResultUploaded || "---",
          "Diagnostic Lab Facility Name (where CXR got redeemed)": x.DiagnosticLabFacilityName || "---",
          "Diagnostic Lab Provider Name": x.DiagnosticLabProviderName || "---",
          "Diagnostic Lab Provider HFID": x.DiagnosticLabProviderID || "---",
          //D-Consult
          "D Consult Voucher disbursed by Lab": x.DConsultVoucherdisbursedbyLab ? x.DConsultVoucherdisbursedbyLab : '-----',
          "D Consult Voucher disbursed by AYUSH": x.DConsultVoucherdisbursedbyAyush ? x.DConsultVoucherdisbursedbyAyush : '-----',
          "D Consult Issuing Provider HFID": x.DConsultIssuingProviderHFID ? x.DConsultIssuingProviderHFID : "-----",
          "D Consult Issuing Provider Name": x.DConsultIssuingProviderName ? x.DConsultIssuingProviderName : "-----",
          "D Consult Voucher Issue Date": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '-----',
          "D Consult Voucher Code": x.voucherCode ? parseInt(x.voucherCode) : '-----',
          "D Consult Redemption Date": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '-----',
          "Emapnelled Doctor Facility Name": x.EmapnelledDoctorFacilityName ? x.EmapnelledDoctorFacilityName : '-----',
          "Emapnelled Doctor Provider Name": x.EmapnelledDoctorProviderName ? x.EmapnelledDoctorProviderName : '-----',
          "Empanelled Doctor Provider ID": x.EmpanelledDoctorProviderID ? x.EmpanelledDoctorProviderID : '-----',
          //CBNAAT
          "CBNAAT Voucher Disbursed": x.CBNAATVoucherDisbursed,
          "CBNAAT Issuing Provider Name": x.providerName || "---",
          "CBNAAT Issuing Provider HFID": x.providerNikshayHFID || "---",
          "CBNAAT Voucher Issue Date": x.VoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.VoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "CBNAAT Voucher Code": x.cbnaatVoucherCode ? parseInt(x.cbnaatVoucherCode) : '-----',
          "CBNAAT Prescription": x.prescription ? {
            f: `=HYPERLINK("${environment.apiEndPoint}${x.prescription}", "View")`,
          } : '-----',
          "ATT Status": x.ATTStatus || "-----",
          "Sample Collection Type": x.SampleCollectionType || "-----",
          "Sample Collected": x.SampleCollected || "-----",
          "Schedule Time for Sample Deposit ": x.ScheduleTimeforSampleDeposit ? new DatePipe('en-US').transform(new Date(x.ScheduleTimeforSampleDeposit), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "SCT Agent Name": x.sctAgentName || '---',
          "Sample Collected Date Time": x.sampleCollectedAt ? new DatePipe('en-US').transform(new Date(x.sampleCollectedAt), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Report Delivery Date": x.reportDeliveryDate ? new DatePipe('en-US').transform(new Date(x.reportDeliveryDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Test Status": x.testStatus || "-----",
          "ResultMTB": x.resultMTB || "-----",
          //FDC
          "FDC Voucher Disbursed": x.FDCVoucherDisbursed || "-----",
          "FDC Voucher Issuing Provider Name": x.FDCVoucherIssuingProviderName || "-----",
          "FDC Voucher Code": x.FDCVoucherCode ? parseInt(x.FDCVoucherCode) : '-----',
          "FDC Voucher Issue Date": x.FDCVoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.FDCVoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Treatment Initiation Date": x.isPatientEnrolledAt ? new DatePipe('en-US').transform(new Date(x.isPatientEnrolledAt), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Adult/Pediatric": x.Adult_Pediatric || "-----",
          "Product Name": x.ProductName || "-----",
          "Weight Band": x.WeightBand || "-----",
          "Confirmed Weight Band": x.ConfirmedWeightBand || "-----",
          "No. of Blister": x.NoOfBlister || "-----",
          "No. of Days": x.NoOfDays || "-----",
          "FDC Voucher Redeemed": x.voucherRedeemed ? "Yes" : "No",
          "FDC Voucher Claim Date": x.FDCVoucherClaimDate ? new DatePipe('en-US').transform(new Date(x.FDCVoucherClaimDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "FDC Voucher Claim Chemist ID ": x.FDCVoucherClaimProviderHFID || "-----",
          "FDC Voucher Claim Provider Name": x.FDCVoucherClaimProviderName || "-----",
          "FDC Voucher Claim Facility Name": x.FDCVoucherClaimFacilityName || "-----",
          "Latest Unclaimed FDC Voucher Issuing Provider Name": x.NotClaimedFDCVoucherIssuingProviderName || "-----",
          "Latest Unclaimed FDC Voucher Issuing Provider HFID": x.NotClaimedFDCVoucherIssuingProviderHFID || "-----",
          "Latest Unclaimed FDC Voucher Code": x.NotClaimedFDCVoucherCode ? parseInt(x.NotClaimedFDCVoucherCode) : '-----',
          "Latest Unclaimed FDC Voucher Issue Date": x.NotClaimedFDCVoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.NotClaimedFDCVoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Latest Unclaimed Adult/Pediatric": x.NotClaimedAdult_Pediatric || "-----",
          "Latest Unclaimed Product Name": x.NotClaimedProductName || "-----",
          "Latest Unclaimed Weight Band": x.NotClaimedAdult_Pediatric || "-----",
          "Latest Unclaimed Confirmed Weight Band": x.NotClaimedConfirmedWeightBand || "-----",
          "Latest Unclaimed No. of Blister": x.NotClaimedNoOfDays || "-----",
          "Date of Prescription Upload": x.DateofPrescriptionUpload ? new DatePipe('en-US').transform(new Date(x.DateofPrescriptionUpload), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Latest Unclaimed No. of Days": x.NotClaimedNoOfDays || "-----",
          //Start Treatment
          "Treatment Started": x.TreatmentStarted || "-----",
          "Enrolling Provider Name (who is starting treatment)": x.EnrollingProviderName || "-----",
          "Enrolling Provider HFID": x.EnrollingProviderHFID || "-----",
          "Patient Address": x.PatientAddress || "-----",
          "Date of Diagnosis": x.DateofDiagnosis ? new DatePipe('en-US').transform(new Date(x.DateofDiagnosis), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Site of Disease": x.SiteofDisease || "-----",
          "Test Carried Out": x.TestCarriedOut || "-----",
          "Weight": x.Weight || "-----",
          "Upload Prescription": x.UploadPrescription ? {
            f: `=HYPERLINK("${environment.apiEndPoint}${x.UploadPrescription}", "View")`,
          } : '-----',
          "Nikshay ID": x.nikshayId || "-----",
          //End Treatment
          "Expected Treatment End Date": x.ExpectedTreatmentEndDate ? new DatePipe('en-US').transform(new Date(x.ExpectedTreatmentEndDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Actual Treatment End Date": x.ActualTreatmentEndDate ? new DatePipe('en-US').transform(new Date(x.ActualTreatmentEndDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Treatment Outcome": x.TreatmentOutcome || "-----",
			"Treatment Outcome Explanation": x.TreatmentOutcomeRemarks || "-----",
			"Direct Notification": x.directNotification,
		  "Presumptive Case Closure Status": x.presumptiveCaseClosureStatus,
		  "Presumptive Case Closure Reason": x.presumptiveCaseClosureReason,
		  "Confirmed TB Case End Treatment Status": x.confirmedTbCaseEndTreatmentStatus,
		  "Confirmed TB Case End Treatment Reason": x.confirmedTbCaseEndTreatmentReason,
        }

      });
      const workSheet = XLSX.utils.json_to_sheet(dataToExport);
      XLSX.utils.book_append_sheet(workBook, workSheet, ele.registrationType);
    });
    XLSX.writeFile(workBook, "Segregated Complete Patient Details.xlsx");
  }

  downloadAggregatedData() {
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    const finalData = [];
    this.completeData.forEach(ele => {
      const dataToExport = ele.data.map((x) => {
		console.log('dataAggregate', x);
        // console.log(new Date(x.PatientRegistrationDate));
        return {
          //Registration
          "Registration Facility Name": x.RegistrationFacilityName || '---',
          "Registration Provider Name": x.RegistrationProviderName || "---",
          "Registration Provider HFID": x.RegistrationProviderId || "---",
          "Registration Provider Type": x.RegistrationProvidertype || "---",
          "Patient Name": x.PatientName || "---",
          "Patient Phone Number": x.PatientPhoneNumber || "---",
          "Patient ADITYA ID": x.PatientID || "---",
          "Patient Age": x.PatientAge || "---",
          "Patient Gender": x.PatientGender || "---",
          "Patient Registration Date": x.PatientRegistrationDate !== '---' ? new DatePipe('en-US').transform(new Date(x.PatientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "TB Symptoms": x.TBSymptoms.toString() || "---",
          "TB Indicative": x.IsTBIndicative === "true" ? "Yes" : "No",
          "OTP Verified": x.OTPVerified ? "Yes" : "No",
          //CXR
          "CXR Disbursement Date": typeof (x.CXRDisbursementDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.CXRDisbursementDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "CXR Voucher Code": x.CXRvoucherCode ? parseInt(x.CXRvoucherCode) : "-----",
          "CXR Redemption Date": typeof (x.CXRRedemptionDate) === 'number' ? new DatePipe('en-US').transform(new Date(x.CXRRedemptionDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
          "CXR Result (Suggestive Of TB)": x.CXRResult || "---",
          "CXR Report Uploaded": x.CXRResultUploaded || "---",
          "Diagnostic Lab Facility Name (where CXR got redeemed)": x.DiagnosticLabFacilityName || "---",
          "Diagnostic Lab Provider Name": x.DiagnosticLabProviderName || "---",
          "Diagnostic Lab Provider HFID": x.DiagnosticLabProviderID || "---",
          //D-Consult
          "D Consult Voucher disbursed by Lab": x.DConsultVoucherdisbursedbyLab ? x.DConsultVoucherdisbursedbyLab : '-----',
          "D Consult Voucher disbursed by AYUSH": x.DConsultVoucherdisbursedbyAyush ? x.DConsultVoucherdisbursedbyAyush : '-----',
          "D Consult Issuing Provider HFID": x.DConsultIssuingProviderHFID ? x.DConsultIssuingProviderHFID : "-----",
          "D Consult Issuing Provider Name": x.DConsultIssuingProviderName ? x.DConsultIssuingProviderName : "-----",
          "D Consult Voucher Issue Date": x.voucherCreatedAt ? new DatePipe('en-US').transform(new Date(x.voucherCreatedAt), 'dd-MM-yyyy hh:mm:ss a') : '-----',
          "D Consult Voucher Code": x.voucherCode ? parseInt(x.voucherCode) : '-----',
          "D Consult Redemption Date": x.voucherRedeemedAt ? new DatePipe('en-US').transform(new Date(x.voucherRedeemedAt), 'dd-MM-yyyy hh:mm:ss a') : '-----',
          "Emapnelled Doctor Facility Name": x.EmapnelledDoctorFacilityName ? x.EmapnelledDoctorFacilityName : '-----',
          "Emapnelled Doctor Provider Name": x.EmapnelledDoctorProviderName ? x.EmapnelledDoctorProviderName : '-----',
          "Empanelled Doctor Provider ID": x.EmpanelledDoctorProviderID ? x.EmpanelledDoctorProviderID : '-----',
          //CBNAAT
          "CBNAAT Voucher Disbursed": x.CBNAATVoucherDisbursed,
          "CBNAAT Issuing Provider Name": x.providerName || "---",
          "CBNAAT Issuing Provider HFID": x.providerNikshayHFID || "---",
          "CBNAAT Voucher Issue Date": x.VoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.VoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "CBNAAT Voucher Code": x.cbnaatVoucherCode ? parseInt(x.cbnaatVoucherCode) : '-----',
          "CBNAAT Prescription": x.prescription ? {
            f: `=HYPERLINK("${environment.apiEndPoint}${x.prescription}", "View")`,
          } : '-----',
          "ATT Status": x.ATTStatus || "-----",
          "Sample Collection Type": x.SampleCollectionType || "-----",
          "Sample Collected": x.SampleCollected || "-----",
          "Schedule Time for Sample Deposit ": x.ScheduleTimeforSampleDeposit ? new DatePipe('en-US').transform(new Date(x.ScheduleTimeforSampleDeposit), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "SCT Agent Name": x.sctAgentName || '---',
          "Sample Collected Date Time": x.sampleCollectedAt ? new DatePipe('en-US').transform(new Date(x.sampleCollectedAt), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Report Delivery Date": x.reportDeliveryDate ? new DatePipe('en-US').transform(new Date(x.reportDeliveryDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Test Status": x.testStatus || "-----",
          "ResultMTB": x.resultMTB || "-----",
          //FDC
          "FDC Voucher Disbursed": x.FDCVoucherDisbursed || "-----",
          "FDC Voucher Issuing Provider Name": x.FDCVoucherIssuingProviderName || "-----",
          "FDC Voucher Code": x.FDCVoucherCode ? parseInt(x.FDCVoucherCode) : '-----',
          "FDC Voucher Issue Date": x.FDCVoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.FDCVoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Treatment Initiation Date": x.isPatientEnrolledAt ? new DatePipe('en-US').transform(new Date(x.isPatientEnrolledAt), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Adult/Pediatric": x.Adult_Pediatric || "-----",
          "Product Name": x.ProductName || "-----",
          "Weight Band": x.WeightBand || "-----",
          "Confirmed Weight Band": x.ConfirmedWeightBand || "-----",
          "No. of Blister": x.NoOfBlister || "-----",
          "No. of Days": x.NoOfDays || "-----",
          "FDC Voucher Redeemed": x.voucherRedeemed ? "Yes" : "No",
          "FDC Voucher Claim Date": x.FDCVoucherClaimDate ? new DatePipe('en-US').transform(new Date(x.FDCVoucherClaimDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "FDC Voucher Claim Chemist ID ": x.FDCVoucherClaimProviderHFID || "-----",
          "FDC Voucher Claim Provider Name": x.FDCVoucherClaimProviderName || "-----",
          "FDC Voucher Claim Facility Name": x.FDCVoucherClaimFacilityName || "-----",
          "Latest Unclaimed FDC Voucher Issuing Provider Name": x.NotClaimedFDCVoucherIssuingProviderName || "-----",
          "Latest Unclaimed FDC Voucher Issuing Provider HFID": x.NotClaimedFDCVoucherIssuingProviderHFID || "-----",
          "Latest Unclaimed FDC Voucher Code": x.NotClaimedFDCVoucherCode ? parseInt(x.NotClaimedFDCVoucherCode) : '-----',
          "Latest Unclaimed FDC Voucher Issue Date": x.NotClaimedFDCVoucherIssueDate ? new DatePipe('en-US').transform(new Date(x.NotClaimedFDCVoucherIssueDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Latest Unclaimed Adult/Pediatric": x.NotClaimedAdult_Pediatric || "-----",
          "Latest Unclaimed Product Name": x.NotClaimedProductName || "-----",
          "Latest Unclaimed Weight Band": x.NotClaimedAdult_Pediatric || "-----",
          "Latest Unclaimed Confirmed Weight Band": x.NotClaimedConfirmedWeightBand || "-----",
          "Latest Unclaimed No. of Blister": x.NotClaimedNoOfDays || "-----",
          "Latest Unclaimed No. of Days": x.NotClaimedNoOfDays || "-----",
          //Start Treatment
          "Treatment Started": x.TreatmentStarted || "-----",
          "Enrolling Provider Name (who is starting treatment)": x.EnrollingProviderName || "-----",
          "Enrolling Provider HFID": x.EnrollingProviderHFID || "-----",
          "Patient Address": x.PatientAddress || "-----",
          "Date of Diagnosis": x.DateofDiagnosis ? new DatePipe('en-US').transform(new Date(x.DateofDiagnosis), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Site of Disease": x.SiteofDisease || "-----",
          "Test Carried Out": x.TestCarriedOut || "-----",
          "Weight": x.Weight || "-----",
          "Date of Prescription Upload": x.DateofPrescriptionUpload ? new DatePipe('en-US').transform(new Date(x.DateofPrescriptionUpload), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Upload Prescription": x.UploadPrescription ? {
            f: `=HYPERLINK("${environment.apiEndPoint}${x.UploadPrescription}", "View")`,
          } : '-----',
          "Nikshay ID": x.nikshayId || "-----",
          //End Treatment
          "Expected Treatment End Date": x.ExpectedTreatmentEndDate ? new DatePipe('en-US').transform(new Date(x.ExpectedTreatmentEndDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Actual Treatment End Date": x.ActualTreatmentEndDate ? new DatePipe('en-US').transform(new Date(x.ActualTreatmentEndDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
          "Treatment Outcome": x.TreatmentOutcome || "-----",
			"Treatment Outcome Explanation": x.TreatmentOutcomeRemarks || "-----",
			"Direct Notification": x.directNotification,
			"Presumptive Case Closure Status": x.presumptiveCaseClosureStatus,
			"Presumptive Case Closure Reason": x.presumptiveCaseClosureReason,
			"Confirmed TB Case End Treatment Status": x.confirmedTbCaseEndTreatmentStatus,
			"Confirmed TB Case End Treatment Reason": x.confirmedTbCaseEndTreatmentReason,
        }

      });
      finalData.push(...dataToExport);
    });
    const workSheet = XLSX.utils.json_to_sheet(finalData);
    XLSX.utils.book_append_sheet(workBook, workSheet, "All");
    XLSX.writeFile(workBook, "Aggregated Complete Patient Details.xlsx");
  }

}
