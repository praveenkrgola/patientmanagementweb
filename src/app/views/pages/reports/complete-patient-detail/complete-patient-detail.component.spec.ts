import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletePatientDetailComponent } from './complete-patient-detail.component';

describe('CompletePatientDetailComponent', () => {
  let component: CompletePatientDetailComponent;
  let fixture: ComponentFixture<CompletePatientDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletePatientDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletePatientDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
