import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ListMasterService } from '../../../../../services/list-master/list-master.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatOption } from '@angular/material';
import { ReferralMasterService } from '../../../../../services/referral-master/referral-master.service';
import Swal from 'sweetalert2';
import * as XLSX from "xlsx";
import { DatePipe } from '@angular/common';
@Component({
  selector: 'kt-confirm-tbregister',
  templateUrl: './confirm-tbregister.component.html',
  styleUrls: ['./confirm-tbregister.component.scss']
})
export class ConfirmTBRegisterComponent implements OnInit {
  @ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;
  constructor(private _listMasterService: ListMasterService,
    private _referralMasterService: ReferralMasterService,
    private _cdr: ChangeDetectorRef) { }
  registrationTypeList = [];
  filterForm: FormGroup;
  showProcessing: boolean = false;
  showTable: boolean = false;
  displayedColumns: String[] = [
    "sn",
    "RegistrationFacilityName",
    "RegistrationProviderName",
    "RegistrationProviderId",
    "RegistrationProvidertype",
    "PatientName",
    "PatientPhoneNumber",
    "PatientID",
    "PatientRegistrationDate",
    "TBSymptoms",
    "CXRDisbursementDate",
    "CXRvoucherCode",
    "CXRRedemptionDate",
    "CXRResultUploaded",
    "CXRResult",
    "DiagnosticLabFacilityName",
    "DiagnosticLabProviderName",
    "DiagnosticLabProviderID",
    "DConsultVoucherdisbursedbyLab",
    "DConsultVoucherdisbursedbyAyush",
    "DConsultIssueDate",
    "DConsultVoucherCode",
    "DConsultRedemptionDate",
    "EmapnelledDoctorFacilityName",
    "EmapnelledDoctorProviderName",
    "EmpanelledDoctorProviderID",
    "StartTreatment",
    "CBNAATVoucherDisbursed",
    "FDCVoucherDisbursed",
    "SiteofDisease",
    "ProviderFacilityName",
    "ProviderName",
    "ProviderID",
    "TreatmentInitiation",
    "TreatmentInitiationDate",
    "PatientGender",
    "PatientAge"
  ];
  minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
  maxDate = new Date(new Date().setHours(0, 0, 0, 0));
  dataSource: MatTableDataSource<any>;

  ngOnInit() {
    this.filterForm = new FormGroup({
      registrationType: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required),

    });
    this.getRegistrationType();
  }

  getRegistrationType() {
    this._listMasterService.getStakeholder({ where: { type: "stakeholder", "name.canBeCreated": true }, order: "name.key ASC" }).subscribe(res => {
      this.registrationTypeList = res;
      this._cdr.detectChanges()
    });
  }

  viewData() {
    this.showProcessing = true;
    this.showTable = false;

    const filterValue = { registrationType: this.filterForm.value.registrationType, fromDate: new Date(this.filterForm.value.date[0]).getTime(), toDate: new Date(this.filterForm.value.date[1]).getTime() }
    this._referralMasterService.getConfirmTBRegister(filterValue).subscribe(res => {
      if (res.length > 0) {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.showProcessing = false;
        this.showTable = true;
        this._cdr.detectChanges();
      } else {
        this.showProcessing = false;
        this.showTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!! No Data Found.");
      }
    }, err => {
      this.showProcessing = false;
      this.showTable = false;
      this._cdr.detectChanges();
      this.showAlert("error", "Oops!! No Data Found.");
    });
  }

  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }


  exportTable() {

    let dataToExport = this.dataSource.data.map((x) => ({
      "Registration Facility Name": x.RegistrationFacilityName,
      "Registration Provider Name": x.RegistrationProviderName,
      "Registration Provider ID": x.RegistrationProviderId || '-----',
      "Registration Provider type": x.RegistrationProvidertype || '-----',
      "Patient Name": x.PatientName,
      "Patient Phone Number": x.PatientPhoneNumber,
      "Patient ID": x.PatientID,
      "Patient Registration Date": new DatePipe('en-US').transform(new Date(x.PatientRegistrationDate), 'dd-MM-yyyy hh:mm:ss a') || "-----",
      "TB Symptoms": x.TBSymptoms.toString(),
      "CXR Disbursement Date": x.CXRRedemptionDate ? new DatePipe('en-US').transform(new Date(x.CXRDisbursementDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "CXR Voucher Code": x.CXRvoucherCode ? parseInt(x.CXRvoucherCode):"-----",
      "CXR Redemption Date": x.CXRRedemptionDate ? new DatePipe('en-US').transform(new Date(x.CXRRedemptionDate), 'dd-MM-yyyy hh:mm:ss a') : "-----",
      "CXR Result":  x.CXRResult ? x.CXRResult : "-----",
      "CXR Report Uploaded":  x.CXRResultUploaded ? x.CXRResultUploaded : "-----",
      "Diagnostic Lab Facility Name": x.DiagnosticLabFacilityName,
      "Diagnostic Lab Provider Name": x.DiagnosticLabProviderName || '-----',
      "Diagnostic Lab Provider ID": x.DiagnosticLabProviderID || '-----',
      "D Consult Voucher disbursed by Lab": x.DConsultVoucherdisbursedbyLab ? x.DConsultVoucherdisbursedbyLab : '-----',
      "D Consult Voucher disbursed by AYUSH": x.DConsultVoucherdisbursedbyAyush ? x.DConsultVoucherdisbursedbyAyush : '-----',
      "D Consult Voucher Issue Date": x.DConsultIssueDate ? new DatePipe('en-US').transform(new Date(x.DConsultIssueDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "D Consult Voucher Code": x.DConsultVoucherCode ? parseInt(x.DConsultVoucherCode) : '-----',
      "D Consult Redemption Date": x.DConsultRedemptionDate ? new DatePipe('en-US').transform(new Date(x.DConsultRedemptionDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "Emapnelled Doctor Facility Name": x.EmapnelledDoctorFacilityName ? x.EmapnelledDoctorFacilityName : '-----',
      "Emapnelled Doctor Provider Name": x.EmapnelledDoctorProviderName ? x.EmapnelledDoctorProviderName : '-----',
      "Empanelled Doctor Provider ID": x.EmpanelledDoctorProviderID ? x.EmpanelledDoctorProviderID : '-----',
      "Start Treatment": x.StartTreatment,
      "CBNAAT Voucher Disbursed": x.CBNAATVoucherDisbursed ? x.CBNAATVoucherDisbursed : '-----',
      "FDC Voucher Disbursed": x.FDCVoucherDisbursed || '-----',
      "Site of Disease": x.SiteofDisease ? x.SiteofDisease.toUpperCase() : '-----',
      "Provider Facility Name": x.ProviderFacilityName || '-----',
      "Provider Name": x.ProviderName || '-----',
      "Provider ID": x.ProviderID || '-----',
      "Treatment Initiation": x.TreatmentInitiation || '-----',
      "Treatment Initiation Date": x.TreatmentInitiationDate ? new DatePipe('en-US').transform(new Date(x.TreatmentInitiationDate), 'dd-MM-yyyy hh:mm:ss a') : '-----',
      "Patient Gender": x.PatientGender ? x.PatientGender.toUpperCase() : '-----',
      "Patient Age": x.PatientAge || '-----',
      // link: {
      //   f: 'HYPERLINK("http://www.google.com", "Google")',
      
      // }      
    }));
    const workSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
    XLSX.writeFile(workBook, "Confirm TB Register .xlsx");
  }


  selectSingle() {
    if (this.allRegistrationType.selected) {
      this.allRegistrationType.deselect();
      return false;
    }
    if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
      this.allRegistrationType.select();

  }
  selectAll() {
    if (this.allRegistrationType.selected) {
      this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name.value)]);
    } else {
      this.filterForm.controls.registrationType.patchValue([]);
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

