import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmTBRegisterComponent } from './confirm-tbregister.component';

describe('ConfirmTBRegisterComponent', () => {
  let component: ConfirmTBRegisterComponent;
  let fixture: ComponentFixture<ConfirmTBRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmTBRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmTBRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
