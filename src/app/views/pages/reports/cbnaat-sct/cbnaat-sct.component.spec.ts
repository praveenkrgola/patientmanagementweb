import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CbnaatSctComponent } from './cbnaat-sct.component';

describe('CbnaatSctComponent', () => {
  let component: CbnaatSctComponent;
  let fixture: ComponentFixture<CbnaatSctComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbnaatSctComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CbnaatSctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
