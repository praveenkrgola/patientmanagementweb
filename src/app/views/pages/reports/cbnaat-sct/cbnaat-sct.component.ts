import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import Swal from 'sweetalert2';
import { ListMasterService } from '../../../../../../src/services/list-master/list-master.service';
import { ReferralMasterService } from '../../../../../../src/services/referral-master/referral-master.service';
import * as XLSX from "xlsx";
import { DatePipe } from '@angular/common';

@Component({
	selector: 'kt-cbnaat-sct',
	templateUrl: './cbnaat-sct.component.html',
	styleUrls: ['./cbnaat-sct.component.scss']
})
export class CbnaatSctComponent implements OnInit {
	registrationTypeList = [];
	filterForm: FormGroup;
	@ViewChild("paginator", { static: true }) paginator: MatPaginator;
	@ViewChild("matSort", { static: true }) sort: MatSort;
	@ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
	minDate = new Date(new Date("2021-01-01").setHours(0, 0, 0, 0));
	maxDate = new Date(new Date().setHours(0, 0, 0, 0));
	displayedColumns: String[] = [
		"sn",
		"sampleCollectedAtPlace",
		"sctAgentName",
		"providerName",
		"providerDesignation",
		"providerNikshayHFID",
		"fieldOfficerName",
		"patientNikshayId",
		"voucherGenerationTime",
		"voucherCode",
		"sampleCollectedAt",
		"numberOfSample",
		"sampleDeliveredToLabAt",
		"reportCollectionDate",
		"reportDeliveryDate",
		"testStatus",
		"resultMTB",
		"resultRIFResistance",
		"reportUploadedOnMIS",
		"sampleRejectedByLab",
		"sampleRejectedByLabAt",
		"sampleRejectedByLabReason",
	];
	dataSource: MatTableDataSource<any>;
	showTable: boolean = false;
	showProcessing: boolean;

	constructor(
		private _listMasterService: ListMasterService,
		private _referralMaster: ReferralMasterService,
		private _cdr: ChangeDetectorRef
	) { }

	ngOnInit() {
		this.createForm();
	}
	createForm() {
		this.filterForm = new FormGroup({
			registrationType: new FormControl(null, Validators.required),
			date: new FormControl(null, Validators.required),
		});
		this.getRegistrationType();
	}
	getRegistrationType() {
		this._listMasterService
			.getStakeholder({
				where: { type: "stakeholder", "name.canBeCreated": true },
				order: "name.key ASC",
			})
			.subscribe((res) => {
				this.registrationTypeList = res;
				// Updated on 20-07-2021 as per Shruti instruction
				this.registrationTypeList.push({
					"_id": "602a410e770cad18e4ec5105",
					"type": "stakeholder",
					"name": {
						"key": "SCT Agent",
						"value": "sctagent",
						"canBeCreated": false
					}
				});
				this._cdr.detectChanges();
			});
	}
	selectSingle() {
		if (this.allRegistrationType.selected) {
			this.allRegistrationType.deselect();
			return false;
		}
		if (this.filterForm.controls.registrationType.value.length == this.registrationTypeList.length)
			this.allRegistrationType.select();

	}
	selectAll() {
		if (this.allRegistrationType.selected) {
			this.filterForm.controls.registrationType.patchValue([0, ...this.registrationTypeList.map(type => type.name.value)]);
		} else {
			this.filterForm.controls.registrationType.patchValue([]);
		}
	}

	viewData() {
		this.showProcessing = true;
		this.showTable = false;
		const formValue = this.filterForm.value;
		const reqObject = {
			designation: formValue.registrationType,
			fromDate: formValue.date[0].setHours(0, 0, 0, 0),
			toDate: formValue.date[1].setHours(23, 59, 59, 0)
		};
		this._referralMaster.getCBNAATSCTReport(reqObject).subscribe((res) => {
			if (res.length > 0) {
				this.showProcessing = false;
				this.dataSource = new MatTableDataSource(res);
				this.dataSource.paginator = this.paginator;
				this.dataSource.sort = this.sort;
				this.showTable = true;
				this._cdr.detectChanges();
			} else {
				this.showTable = false;
				this.showProcessing = false;
				this._cdr.detectChanges();
				this.showAlert("error", "Oops!! No Data Found.");
			}
		});
	}
	showAlert(icon, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: "center",
			showConfirmButton: false,
			timer: 4000,
			timerProgressBar: true,
		});
		Toast.fire({
			icon: icon,
			title: title,
		});
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}



	exportTable() {
		let dataToExport = this.dataSource.data.map((x) => ({
			"Sample Collected At": x.sampleCollectedAtPlace || '---',			
			"SCT Agent Name": x.sctAgentName || '---',
			"Registration Provider Name": x.providerName || "---",
			"Registration Provider Type": x.providerDesignation || "---",
			"Registration Provider Nikshay HFID": x.providerNikshayHFID || "---",
			"Field Officer Name": x.fieldOfficerName || "---",
			"Patient Nikshay Id": x.patientNikshayId || "---",
			"Voucher Generation Date": x.voucherGenerationTime ? new DatePipe('en-US').transform(new Date(x.voucherGenerationTime), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Voucher Code": x.voucherCode || "---",
			"Sample Collected Date": x.sampleCollectedAt ? new DatePipe('en-US').transform(new Date(x.sampleCollectedAt), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Number Of Sample": x.numberOfSample || "---",
			"Sample Delivered To Lab Date": x.sampleDeliveredToLabAt ? new DatePipe('en-US').transform(new Date(x.sampleDeliveredToLabAt), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Report Collection Date": x.reportCollectionDate ? new DatePipe('en-US').transform(new Date(x.reportCollectionDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Report Delivery Date": x.reportDeliveryDate ? new DatePipe('en-US').transform(new Date(x.reportDeliveryDate), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Test Status": x.testStatus || "---",
			"ResultMTB": x.resultMTB || "---",
			"Result RIF Resistance": x.resultRIFResistance || "---",
			"Report Uploaded On MIS": x.reportUploadedOnMIS || "---",
			"Sample Rejected By Lab": x.sampleRejectedByLab || "---",
			"Sample Rejected By Lab Date": x.sampleRejectedByLabAt ? new DatePipe('en-US').transform(new Date(x.sampleRejectedByLabAt), 'dd-MM-yyyy hh:mm:ss a') : "---",
			"Sample Rejected By Lab Reason": x.sampleRejectedByLabReason || "---"
		}));
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "CBNAAT Details.xlsx");
	}
}
