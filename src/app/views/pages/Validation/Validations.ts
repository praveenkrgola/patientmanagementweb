import { Injectable } from "@angular/core";
import { UserLoginService } from '../../../../services/user-login/user-login.service';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { of } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class Validations {
    constructor(
        public _userLoginService: UserLoginService
    ) { }
    checkMobileIsAlreadyExist(userDetail?: { oldMobile: number }) {

        return (control: AbstractControl): ValidationErrors | null => {
            const mobileNumber = control.value;

            if (userDetail.oldMobile) {
                if (userDetail.oldMobile == mobileNumber) return of(null);
            }
            return this._userLoginService.getList({ type: "mobile", mobile: mobileNumber}).subscribe((res: any) => {
                if (res.length > 0) {
                    return { 'mobileAlreadyExist': true }
                } else {
                    return null
                }
            });
        }
    }
}