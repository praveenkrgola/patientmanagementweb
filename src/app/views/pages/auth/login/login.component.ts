// Angular
import {
	ChangeDetectorRef,
	Component,
	OnDestroy,
	OnInit,
	ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl,
} from "@angular/forms";
// RxJS
import { Observable, Subject } from "rxjs";

import { UserLoginService } from "../../../../../services/user-login/user-login.service";

@Component({
	selector: "kt-login",
	templateUrl: "./login.component.html",
	encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	mobileLoginForm: FormGroup;
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];
	showMobileDiv: boolean = true;
	private unsubscribe: Subject<any>;

	isValidMobileUsernamePassword: boolean = false;
	errorMessage: string = "";
	loginButtonDisabled = false;
	constructor(
		private router: Router,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		private _userLoginService: UserLoginService
	) {
		this.unsubscribe = new Subject();

		// this.mobileLoginForm = new FormGroup({
		// 	mobile: new FormControl(
		// 		"",
		// 		Validators.compose([
		// 			Validators.required,
		// 			Validators.min(1000000000),
		// 			Validators.max(9999999999),
		// 		])
		// 	),
		// 	otp: new FormControl(
		// 		"",
		// 		Validators.compose([
		// 			Validators.required,
		// 			Validators.minLength(6),
		// 			Validators.maxLength(6),
		// 		])
		// 	),
		// });
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.initLoginForm();
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		//this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		this.loginForm = this.fb.group({
			username: [
				"",
				Validators.compose([
					Validators.required,
					//Validators.email,
					Validators.minLength(2),
					Validators.maxLength(320), // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
				]),
			],
			password: [
				"",
				Validators.compose([
					Validators.required,
					Validators.minLength(2),
					Validators.maxLength(100),
				]),
			],
		});
	}

	/**
	 * Form Submit
	 */
	// generateOTP() {
	// 	this.errorMessage = "";
	// 	this.isValidMobileUsernamePassword = false;
	// 	this.mobileLoginForm.controls.otp.reset();
	// 	if (this.mobileLoginForm.controls.mobile.valid) {
	// 		this._userLoginService
	// 			.requestOTP(this.mobileLoginForm.controls.mobile.value)
	// 			.subscribe(
	// 				(res: any) => {
	// 					if (res.statusCode === 404) {
	// 						this.errorMessage = res.code;
	// 						this.isValidMobileUsernamePassword = true;
	// 						this.cdr.detectChanges();
	// 					} else {
	// 					}
	// 				},
	// 				(err) => {
	// 					//console.log(err);
	// 				}
	// 			);
	// 	}
	// }

	// loginByMobile() {
	// 	this.loginButtonDisabled = true;
	// 	this.loading = true;
	// 	this._userLoginService
	// 		.verifyOTP(
	// 			this.mobileLoginForm.controls.mobile.value,
	// 			this.mobileLoginForm.controls.otp.value
	// 		)
	// 		.subscribe(
	// 			(user) => {
	// 				if (user) {
	// 					const mobileNumber = this.mobileLoginForm.controls
	// 						.mobile.value;
	// 					this._userLoginService
	// 						.loginByMobile(mobileNumber, "Web")
	// 						.subscribe((userResult: any) => {
	// 							this.errorMessage = "";
	// 							this.isValidMobileUsernamePassword = false;
	// 							sessionStorage.setItem("user", JSON.stringify(userResult));
	// 							//this.router.navigateByUrl('/dashboard');
	// 							this._userLoginService.userPs$.next(userResult);
	// 							if (userResult.user.type.toLowerCase() === "student") {
	// 								this.router.navigate(["/verification/form",]);
	// 							} else {
	// 								this.router.navigate(["/referral"]);
	// 							}
	// 						}, err => {
	// 							this.errorMessage = err.error.error.message;
	// 							this.isValidMobileUsernamePassword = true;
	// 							this.loading = false;
	// 							this.loginButtonDisabled = false;
	// 							this.cdr.detectChanges();
	// 						});
	// 				}
	// 			},
	// 			(err) => {
	// 				this.errorMessage = err.error.error.message;
	// 				this.isValidMobileUsernamePassword = true;
	// 				this.loading = false;
	// 				this.loginButtonDisabled = false;
	// 				this.cdr.detectChanges();
	// 			}
	// 		);
	// }

	submit() {
		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach((controlName) =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;
		const authData = {
			username: controls.username.value,
			password: controls.password.value,
		};
		this._userLoginService
			.login(authData.username, authData.password)
			.subscribe(
				(user) => {
					this._userLoginService.userPs$.next(user);
					this.errorMessage = "";
					this.isValidMobileUsernamePassword = false;
					sessionStorage.setItem("user", JSON.stringify(user.user));
					if (user.user.designation === "programmanager") {
						this.router.navigate(["/program-manager"]);
					} else if (user.user.designation === "finance") {
						this.router.navigate(["/finance"])
					}
				},
				(error) => {
					this.errorMessage = "Invalid Username &/or Password";
					this.isValidMobileUsernamePassword = true;
					this.loading = false;
					this.cdr.detectChanges();
				}
			);
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(
		formName: string,
		controlName: string,
		validationType: string
	): boolean {
		let control;
		if (formName === "loginForm")
			control = this.loginForm.controls[controlName];
		else if (formName === "mobileLoginForm")
			control = this.mobileLoginForm.controls[controlName];

		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
}
