import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductStockComponent } from './deduct-stock.component';

describe('DeductStockComponent', () => {
  let component: DeductStockComponent;
  let fixture: ComponentFixture<DeductStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
