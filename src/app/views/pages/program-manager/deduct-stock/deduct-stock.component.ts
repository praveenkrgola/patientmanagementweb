import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UserLoginService } from '../../../../../services/user-login/user-login.service';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { StockAssignmentMasterService } from '../../../../../services/StockAssignmentMaster/stock-assignment-master.service';
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import Swal from "sweetalert2";

@Component({
  selector: 'kt-deduct-stock',
  templateUrl: './deduct-stock.component.html',
  styleUrls: ['./deduct-stock.component.scss']
})
export class DeductStockComponent implements OnInit {
  lastUpdated: any;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;
  constructor(
    private _userLoginService: UserLoginService,
    private _stockAssignmentMasterService: StockAssignmentMasterService,
    private _cdr: ChangeDetectorRef) { }
  user = JSON.parse(sessionStorage.getItem("user"));
  FOList: any = [];
  inventoryForm: FormGroup;
  showAssignProductTable: boolean = false;
  displayedColumns: String[] = ["sn",
    "name",
    "type",
    "content",
    "ageGroup",
    "status",
    "userPreviousBalance",
    "previousBalance",
    "stockOut",];
  dataSource: MatTableDataSource<any>;
  ngOnInit() {
    this.getFOList();
    this.inventoryForm = new FormGroup({
      fieldofficer: new FormControl(null, Validators.required),
      productArray: new FormArray([]),
    });
  }
  getFOList() {
    this._userLoginService.getList({ where: { designation: "fieldofficer" }, order: "name ASC", fields: ["id", "name"] }).subscribe(res => {
      this.FOList = res;
      this._cdr.detectChanges();
    });
  }

  proceedToAssignStock() {
    const FOId = this.inventoryForm.controls.fieldofficer.value;
    this._stockAssignmentMasterService.getStockDetails({ type: "FO", userId: FOId }).subscribe(res => {
      if (res) {
        this.lastUpdated = Math.max.apply(Math, res.map(data => new Date(data.updatedAt)));
        const productArray = this.inventoryForm.get("productArray") as FormArray;
        res.forEach((rs, i) => {
          const tempForm = new FormGroup({
            productId: new FormControl(rs.id),
            mainBalance: new FormControl(rs.previousBalance),
            previousBalance: new FormControl(rs.userPreviousBalance),
            stockOut: new FormControl(0, Validators.required)
          });
          productArray.push(tempForm);
        });
        this.inventoryForm.updateValueAndValidity();

        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.showAssignProductTable = true;
        this._cdr.detectChanges();
      }
    });
  }

  async deductStock() {
    const productArray = this.inventoryForm.value.productArray.filter(res => Boolean(res.stockOut));
    const total = productArray.reduce((global, current) => global + current.stockOut, 0);

    if (total > 0) {
      const FOId = this.inventoryForm.controls.fieldofficer.value;

      let obj = [];
      productArray.forEach(async (element, i) => {
        // console.log(i, " -- ", element.previousBalance, " -- ", element.stockOut);
        const temp = { stktransaction: "deduction", productId: element.productId, mainBalance: element.mainBalance, lastStock: element.previousBalance, currentStock: element.stockOut }
        obj.push(temp);
      });
      const finalObject = { type: "FO", userId: FOId, updatedBy: this.user.id, data: obj }
      this._stockAssignmentMasterService.DeductOrUpdateStockUserWise(finalObject).subscribe(res => {
        this.clearProductArray();
        this.proceedToAssignStock();
        this._cdr.detectChanges();
        this.showAlert("success", "Stock Deducted Successfully!!")

      }, err => {
        this.showAssignProductTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!!, Something Went Wrong, Please Try Again.");
      });


    } else {
      this.showAlert("error", "Please Enter Stock Atleast For 1 Product !!")
    }

  }

  checkValue(i, element) {
    const stockOut = ((this.inventoryForm.get("productArray") as FormArray).at(i) as FormGroup).get("stockOut").value;
    if (parseInt(stockOut) > element.userPreviousBalance) {
      ((this.inventoryForm.get('productArray') as FormArray).at(i) as FormGroup).get('stockOut').setValue(element.userPreviousBalance);
      this.showAlert("error", "You can't deduct stock less than available stock");
    }
  }
  clearProductArray() {
    this.inventoryForm.removeControl("productArray");
    this.inventoryForm.addControl("productArray", new FormArray([]));
  }
  getActualIndex(index: number) {
    return index + this.paginator.pageSize * this.paginator.pageIndex;
  }
  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }
}
