import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { PatientMasterService } from '../../../../../services/PatientMaster/patient-master.service';
import { ReferralMasterService } from '../../../../../services/referral-master/referral-master.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import _moment from "moment";
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { ProviderComponent } from '../../shared/provider/provider.component';
import { MatSlideToggleChange } from '@angular/material';

am4core.useTheme(am4themes_animated);
@Component({
  selector: 'kt-enrollment-view',
  templateUrl: './enrollment-view.component.html',
  styleUrls: ['./enrollment-view.component.scss']
})

export class EnrollmentViewComponent implements OnInit {
  chartdivenrollmentbyusertypedatewise: boolean = false;
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    elementIdOrContent: 'tableId1', // the id of html/table element
  }
  showProcessing: boolean = false;
  @ViewChild("callProviderComponent", { static: true })
  callProviderComponent: ProviderComponent;
  showToggleConfirmationType: boolean = false;
  constructor(
    private _patientMasterService: PatientMasterService,
    private __referralMasterService: ReferralMasterService,
    private _cdr: ChangeDetectorRef,
    private exportAsService: ExportAsService
  ) { }
  filterForm: FormGroup;
  tableData: any;
  showTable: boolean = false;
  currentDate = new Date();
  currentDay = (this.currentDate.getDate()) < 10 ? ("0" + (this.currentDate.getDate())) : (this.currentDate.getDate());
  currentMonth = (this.currentDate.getMonth() + 1) < 10 ? "0" + (this.currentDate.getMonth() + 1) : (this.currentDate.getMonth() + 1);
  currentYear = this.currentDate.getFullYear();
  fromDate = `${this.currentYear}-${this.currentMonth}-01`;
  toDate = `${this.currentYear}-${this.currentMonth}-${this.currentDay}`;
  fromDateInMilliSecond = new Date(this.fromDate).setHours(0, 0, 0, 0);
  toDateInMilliSecond = new Date().getTime();
  tilesData;
  showEnrollmentViewDateWiseGraph: boolean = false;
  showEnrollmentByUserTypeDateWise: boolean = false;

  threeMonthsAgo = new Date(_moment().subtract(3, 'months').format()).getMonth();
  threeMonthsAgoYear = new Date(_moment().subtract(3, 'months').format()).getFullYear();

  fromDateToDate: Date[] = [new Date(`${this.threeMonthsAgoYear}-${this.threeMonthsAgo + 1}-01`), new Date(this.toDate)];
  ngOnInit() {
    this.filterForm = new FormGroup({
      date: new FormControl(this.fromDateToDate, Validators.required),
      fieldOfficer: new FormControl("all"),
      fieldOfficerList: new FormControl(),

      designation: new FormControl("all"),
      designationList: new FormControl(),

      provider: new FormControl("all"),
      providerList: new FormControl(),

      siteOfDisease: new FormControl("all"),
      CXROffered: new FormControl("all"),
      CBNAATOffered: new FormControl("all"),
      FDCDone: new FormControl("all")

    });
    this.generateEnrollmentViewGraph();
    this.generateConfirmationGraphMonthWise();
    this.generateEnrollmentByUserTypeGraph();
    this.getDirectNotificationMonthWise();
    this.getNotificationByUserType();
  }
  getDirectNotificationMonthWise(formValue?) {
    this.__referralMasterService.getDirectNotification(this.filterForm.value).subscribe(res => {
      // Create chart instance
      let chart = am4core.create("directNotification", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Direct Notification Month Wise';
      chart.logo.disabled = true;

      chart.scrollbarX = new am4core.Scrollbar();

      // Add data
      chart.data = res;
      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "monthYearLabel";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 60;
      categoryAxis.tooltip.disabled = false;
      categoryAxis.renderer.labels.template.horizontalCenter = "middle";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.labels.template.rotation = 0;


      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.renderer.minWidth = 30;
      valueAxis.min = 0;
      // valueAxis.max = 100;
      valueAxis.maxPrecision = 0;
      valueAxis.cursorTooltipEnabled = false;
      valueAxis.title.text = "Monthly Notification";
      valueAxis.title.fontWeight = "600";

      // Create series
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.sequencedInterpolation = true;
      series.dataFields.valueY = "totalPatient";
      series.dataFields.categoryX = "monthYearLabel";
      series.name = "Monthly Notification";
      series.tooltipText = "Monthy Notification:[{categoryX}: bold]{valueY}";
      series.columns.template.strokeWidth = 0;

      series.tooltip.pointerOrientation = "vertical";

      series.columns.template.column.cornerRadiusTopLeft = 10;
      series.columns.template.column.cornerRadiusTopRight = 10;
      series.columns.template.column.fillOpacity = 0.8;
      series.columns.template.width = am4core.percent(15);

      // series.columns.template.events.on("hit", function (ev) {
      //   const data = ev.target.dataItem.dataContext;
      //   this.generateEnrollmentViewGraphDateWise(data);
      // }, this);

      // on hover, make corner radiuses bigger
      let hoverState = series.columns.template.column.states.create("hover");
      hoverState.properties.cornerRadiusTopLeft = 0;
      hoverState.properties.cornerRadiusTopRight = 0;
      hoverState.properties.fillOpacity = 1;

      let totalValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      totalValueAxis.title.text = "Total Notification";
      totalValueAxis.title.fontWeight = "600";
      totalValueAxis.renderer.opposite = true;
      totalValueAxis.min = 0;
      totalValueAxis.maxPrecision = 0;
      totalValueAxis.strictMinMax = false;
      totalValueAxis.renderer.grid.template.disabled = true;
      totalValueAxis.cursorTooltipEnabled = false;

      let paretoSeries = chart.series.push(new am4charts.LineSeries())
      paretoSeries.dataFields.valueY = "totalNotification";
      paretoSeries.dataFields.categoryX = "monthYearLabel";
      paretoSeries.name = "Total Notification";
      paretoSeries.yAxis = totalValueAxis;
      paretoSeries.tooltipText = "Total Notification: {valueY}";
      paretoSeries.bullets.push(new am4charts.CircleBullet());
      paretoSeries.strokeWidth = 2;
      paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
      paretoSeries.strokeOpacity = 0.5;

      // Cursor
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.behavior = "panX";

      chart.legend = new am4charts.Legend();
    });
  }
  //-------------Enrollment By User Type Graph-------------
  getNotificationByUserType() {

    this.__referralMasterService.getNotificationByUserType(this.filterForm.value).subscribe(res => {
      //Create chart instance
      let chart = am4core.create("notificationByUserType", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Notification By User Type Total';
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      chart.colors.list = [
        am4core.color("#26A69A"),
        am4core.color("#5ec85a"),
        am4core.color("#DBD634"),
        am4core.color("#7D8FFF"),
        am4core.color("#9641D6"),
      ];
      chart.data = res;

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "monthYearLabel";
      // categoryAxis.title.text = "Local country offices";
      categoryAxis.renderer.labels.template.rotation = -45;
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 20;
      categoryAxis.renderer.cellStartLocation = 0.1;
      categoryAxis.renderer.cellEndLocation = 0.9;

      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.title.text = "Monthly Notification Count";
      valueAxis.title.fontWeight = "600";
      // Create series

      function createSeries(field, name, stacked, that) {
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "monthYearLabel";
        series.name = name;
        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = stacked;
        series.columns.template.width = am4core.percent(30);

        // series.columns.template.events.on("hit", function (ev) {
        //   const data = ev.target.dataItem.dataContext;
        //   console.log(data);
        //   that.generateEnrollmentByUserTypeGraphDateWise(data);
        // }, this);

        let bullet1 = series.bullets.push(new am4charts.LabelBullet());
        bullet1.interactionsEnabled = false;
        // bullet1.label.text = "{valueY}";
        // bullet1.fontWeight = 'bold';
        bullet1.label.fill = am4core.color("#ffffff");
        bullet1.locationY = 0.5;
      }

      createSeries("chemist", "Chemist", false, this);
      createSeries("empanelledchemist", "Empanelled Chemist", false, this);
      createSeries("generaldoctor", "General Doctor", false, this);
      createSeries("ayushprovider", "AYUSH Provider", false, this);
      createSeries("empanelleddoctor", "Empanelled Doctor", false, this);

      // Add legend
      chart.legend = new am4charts.Legend();

    });

  }
  //---------Enrollment Runnuing Graph-----
  generateEnrollmentViewGraph(formValue?) {
    this.__referralMasterService.GetEnrollmentViewAndTile(this.filterForm.value).subscribe(res => {
      this.tilesData = res.TilesData;
      this._cdr.detectChanges();
      // Create chart instance
      let chart = am4core.create("chartdiv", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Enrollment Running Total';
      chart.logo.disabled = true;

      chart.scrollbarX = new am4core.Scrollbar();

      // Add data
      chart.data = res.StartTreatmentData;
      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "monthYearLabel";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 60;
      categoryAxis.tooltip.disabled = false;
      categoryAxis.renderer.labels.template.horizontalCenter = "middle";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.labels.template.rotation = 0;


      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.renderer.minWidth = 30;
      valueAxis.min = 0;
      // valueAxis.max = 100;
      valueAxis.maxPrecision = 0;
      valueAxis.cursorTooltipEnabled = false;
      valueAxis.title.text = "Monthly Enrollment";
      valueAxis.title.fontWeight = "600";

      // Create series
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.sequencedInterpolation = true;
      series.dataFields.valueY = "totalPatient";
      series.dataFields.categoryX = "monthYearLabel";
      series.name = "Month Enrollment";
      series.tooltipText = "Month Enrollment:[{categoryX}: bold]{valueY}";
      series.columns.template.strokeWidth = 0;

      series.tooltip.pointerOrientation = "vertical";

      series.columns.template.column.cornerRadiusTopLeft = 10;
      series.columns.template.column.cornerRadiusTopRight = 10;
      series.columns.template.column.fillOpacity = 0.8;
      series.columns.template.width = am4core.percent(15);

      series.columns.template.events.on("hit", function (ev) {
        const data = ev.target.dataItem.dataContext;
        this.generateEnrollmentViewGraphDateWise(data);
      }, this);

      // on hover, make corner radiuses bigger
      let hoverState = series.columns.template.column.states.create("hover");
      hoverState.properties.cornerRadiusTopLeft = 0;
      hoverState.properties.cornerRadiusTopRight = 0;
      hoverState.properties.fillOpacity = 1;

      let totalValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      totalValueAxis.title.text = "Total Enrollment";
      totalValueAxis.title.fontWeight = "600";
      totalValueAxis.renderer.opposite = true;
      totalValueAxis.min = 0;
      totalValueAxis.maxPrecision = 0;
      totalValueAxis.strictMinMax = false;
      totalValueAxis.renderer.grid.template.disabled = true;
      totalValueAxis.cursorTooltipEnabled = false;

      let paretoSeries = chart.series.push(new am4charts.LineSeries())
      paretoSeries.dataFields.valueY = "totalEnrollment";
      paretoSeries.dataFields.categoryX = "monthYearLabel";
      paretoSeries.name = "Total Enrollment";
      paretoSeries.yAxis = totalValueAxis;
      paretoSeries.tooltipText = "Total Enrollment: {valueY}";
      paretoSeries.bullets.push(new am4charts.CircleBullet());
      paretoSeries.strokeWidth = 2;
      paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
      paretoSeries.strokeOpacity = 0.5;

      // Cursor
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.behavior = "panX";

      chart.legend = new am4charts.Legend();
    });
  }
  generateEnrollmentViewGraphDateWise(data: any): void {
    const dateObject: any = this.getDateBasedOnMonthYear(data);
    const formData = {
      month: dateObject.month,
      year: dateObject.year,
      fromDate: dateObject.fromDate,
      toDate: dateObject.toDate,
      ...this.filterForm.value
    };
    this.__referralMasterService.getEnrollmentViewDateWise(formData).subscribe(res => {
      this.showEnrollmentViewDateWiseGraph = true;
      this._cdr.detectChanges();
      // Create chart instance
      let chart = am4core.create("chartdivdatewise", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Enrollment Running Total';
      chart.logo.disabled = true;

      chart.scrollbarX = new am4core.Scrollbar();

      // Add data
      chart.data = res;

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "date";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 60;
      categoryAxis.tooltip.disabled = false;
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.labels.template.rotation = 300;


      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.renderer.minWidth = 50;
      valueAxis.min = 0;
      valueAxis.maxPrecision = 0;
      valueAxis.cursorTooltipEnabled = false;
      valueAxis.title.text = "Day Enrollment Count";
      valueAxis.title.fontWeight = "600";

      // Create series
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.sequencedInterpolation = true;
      series.dataFields.valueY = "dateWiseCount";
      series.dataFields.categoryX = "date";
      series.name = "Day Enrollment";
      series.tooltipText = "Day Enrollment:[{categoryX}: bold]{valueY}";
      series.columns.template.strokeWidth = 0;
      series.tooltip.pointerOrientation = "vertical";
      series.columns.template.column.cornerRadiusTopLeft = 10;
      series.columns.template.column.cornerRadiusTopRight = 10;
      series.columns.template.column.fillOpacity = 0.8;
      series.columns.template.width = am4core.percent(20);


      // on hover, make corner radiuses bigger
      let hoverState = series.columns.template.column.states.create("hover");
      hoverState.properties.cornerRadiusTopLeft = 0;
      hoverState.properties.cornerRadiusTopRight = 0;
      hoverState.properties.fillOpacity = 1;


      let totalValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      totalValueAxis.title.text = "Total Enrollment";
      totalValueAxis.title.fontWeight = "600";
      totalValueAxis.renderer.opposite = true;
      totalValueAxis.min = 0;
      totalValueAxis.maxPrecision = 0;
      totalValueAxis.strictMinMax = false;
      totalValueAxis.renderer.grid.template.disabled = true;
      totalValueAxis.cursorTooltipEnabled = false;

      let paretoSeries = chart.series.push(new am4charts.LineSeries())
      paretoSeries.dataFields.valueY = "total";
      paretoSeries.dataFields.categoryX = "date";
      paretoSeries.name = "Total Enrollment";
      paretoSeries.yAxis = totalValueAxis;
      paretoSeries.tooltipText = "Total Enrollment: {valueY}";
      paretoSeries.bullets.push(new am4charts.CircleBullet());
      paretoSeries.strokeWidth = 2;
      paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
      paretoSeries.strokeOpacity = 0.5;

      // Cursor
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.behavior = "panX";

      chart.legend = new am4charts.Legend();
    });
  }
  //---------------END Enrollment Running Graph------------

  //-------------Enrollment By User Type Graph-------------
  generateEnrollmentByUserTypeGraph() {

    this.__referralMasterService.GetEnrollmentByUserType(this.filterForm.value).subscribe(res => {
      //Create chart instance
      let chart = am4core.create("chartdiv3", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Enrollment By User Type Total';
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      chart.colors.list = [
        am4core.color("#26A69A"),
        am4core.color("#5ec85a"),
        am4core.color("#DBD634"),
        am4core.color("#7D8FFF"),
        am4core.color("#9641D6"),
        am4core.color("#a60059"),
      ];
      chart.data = res;

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "monthYearLabel";
      // categoryAxis.title.text = "Local country offices";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 20;
      categoryAxis.renderer.cellStartLocation = 0.1;
      categoryAxis.renderer.cellEndLocation = 0.9;
      categoryAxis.renderer.labels.template.horizontalCenter = "middle";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.labels.template.rotation = 0;

      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.title.text = "Monthly Enrollment Count";
      valueAxis.title.fontWeight = "600";
      // Create series

      function createSeries(field, name, stacked, that) {
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "monthYearLabel";
        series.name = name;
        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = stacked;
        series.columns.template.width = am4core.percent(30);

        series.columns.template.events.on("hit", function (ev) {
          const data = ev.target.dataItem.dataContext;
          that.generateEnrollmentByUserTypeGraphDateWise(data);
        }, this);

        let bullet1 = series.bullets.push(new am4charts.LabelBullet());
        bullet1.interactionsEnabled = false;
        // bullet1.label.text = "{valueY}";
        // bullet1.fontWeight = 'bold';
        bullet1.label.fill = am4core.color("#ffffff");
        bullet1.locationY = 0.5;
      }

      createSeries("chemist", "Chemist", false, this);
      createSeries("empanelledchemist", "Empanelled Chemist", true, this);
      createSeries("generaldoctor", "General Doctor", true, this);
      createSeries("ayushprovider", "AYUSH Provider", true, this);
      createSeries("empanelleddoctor", "Empanelled Doctor", true, this);
      createSeries("sctagent", "SCT Agent", true, this);

      // Add legend
      chart.legend = new am4charts.Legend();

    });

  }

  generateEnrollmentByUserTypeGraphDateWise(data: any): void {
    const dateObject: any = this.getDateBasedOnMonthYear(data);
    const formData = {
      month: dateObject.month,
      year: dateObject.year,
      fromDate: dateObject.fromDate,
      toDate: dateObject.toDate,
      ...this.filterForm.value
    };
    this.__referralMasterService.GetEnrollmentByUserTypeDateWise(formData).subscribe(res => {
      this.showEnrollmentByUserTypeDateWise = true;
      this._cdr.detectChanges();
      //Create chart instance
      let chart = am4core.create("chartdivenrollmentbyusertypedatewise", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Day Enrollment By User Type';
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      chart.colors.list = [
        am4core.color("#26A69A"),
        am4core.color("#5ec85a"),
        am4core.color("#DBD634"),
        am4core.color("#7D8FFF"),
        am4core.color("#9641D6"),
        am4core.color("#a60059")
        
      ];
      chart.data = res;

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "date";
      // categoryAxis.title.text = "Local country offices";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 20;
      categoryAxis.renderer.cellStartLocation = 0.1;
      categoryAxis.renderer.cellEndLocation = 0.9;
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.renderer.labels.template.rotation = 300;

      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.maxPrecision = 0;
      valueAxis.cursorTooltipEnabled = false;
      valueAxis.title.text = "Day Enrollment Count";
      valueAxis.title.fontWeight = "600";
      // Create series
      function createSeries(field, name, stacked) {
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "date";
        series.name = name;
        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = stacked;
        series.columns.template.width = am4core.percent(30);

        let bullet1 = series.bullets.push(new am4charts.LabelBullet());
        bullet1.interactionsEnabled = false;
        // bullet1.label.text = "{valueY}";
        // bullet1.fontWeight = 'bold';
        bullet1.label.fill = am4core.color("#ffffff");
        bullet1.locationY = 0.5;
      }

      createSeries("chemist", "Chemist", false);
      createSeries("empanelledchemist", "Empanelled Chemist", true);
      createSeries("generaldoctor", "General Doctor", true);
      createSeries("ayushprovider", "AYUSH Provider", true);
      createSeries("empanelleddoctor", "Empanelled Doctor", true);
      createSeries("sctagent", "SCT Agent", true);

      // Add legend
      chart.legend = new am4charts.Legend();

    });

  }
  //--------------END Enrollment By User Type Graph---------

  //------------Confirmation Type Graph----------------------
  generateConfirmationGraphMonthWise() {
    let passingObject = this.filterForm.value
    this.__referralMasterService.GetConfirmationType(this.filterForm.value).subscribe(res => {
      // Create chart instance
      const chart = am4core.create("chartdiv2", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Confirmation Type';
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      // Increase contrast by taking evey second color
      chart.colors.step = 2;
      // Add data
      chart.data = res;//generateChartData();
      // Create axes
      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.minGridDistance = 50;
      // dateAxis.dateFormats.setKey("day", "MMMM dt");
      dateAxis.renderer.labels.template.horizontalCenter = "right";
      dateAxis.renderer.labels.template.verticalCenter = "middle";
      dateAxis.renderer.labels.template.rotation = 290;

      // Create series
      function createAxisAndSeries(field, name, opposite, bullet, that) {
        var valueAxis: any = chart.yAxes.push(new am4charts.ValueAxis());
        if (chart.yAxes.indexOf(valueAxis) != 0) {
          valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
        }

        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = field;
        series.dataFields.dateX = "monthYearLabel";
        series.strokeWidth = 2;
        series.yAxis = valueAxis;
        series.name = name;
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.tensionX = 0.8;
        series.showOnInit = true;


        let interfaceColors = new am4core.InterfaceColorSet();

        switch (bullet) {
          case "triangle":
            var bullet: any = series.bullets.push(new am4charts.Bullet());
            bullet.width = 12;
            bullet.height = 12;
            bullet.horizontalCenter = "middle";
            bullet.verticalCenter = "middle";

            bullet.events.on("hit", (event) => {
              const data = event.target.dataItem.dataContext;
              that.generateConfirmationGraph(data);
            });
            var triangle = bullet.createChild(am4core.Triangle);
            triangle.stroke = interfaceColors.getFor("background");
            triangle.strokeWidth = 2;
            triangle.direction = "top";
            triangle.width = 12;
            triangle.height = 12;
            break;

          default:
            var bullet: any = series.bullets.push(new am4charts.CircleBullet());
            bullet.circle.stroke = interfaceColors.getFor("background");
            bullet.circle.strokeWidth = 2;
            bullet.events.on("hit", (event) => {
              const data = event.target.dataItem.dataContext;
              that.generateConfirmationGraph(data);
            });
            break;
        }

        valueAxis.renderer.line.strokeOpacity = 1;
        valueAxis.renderer.line.strokeWidth = 2;
        valueAxis.renderer.line.stroke = series.stroke;
        valueAxis.renderer.labels.template.fill = series.stroke;
        valueAxis.renderer.opposite = opposite;
      }

      createAxisAndSeries("clinicalEnrollment", "Clinical Enrollment", false, "circle", this);
      createAxisAndSeries("microbiologicalEnrollment", "Microbiological Confirmation", true, "triangle", this);
      // createAxisAndSeries("hits", "Hits", true, "rectangle");

      // Add legend
      chart.legend = new am4charts.Legend();
      // Add cursor
      chart.cursor = new am4charts.XYCursor();


    });
  }
  generateConfirmationGraph(data) {
    this.showToggleConfirmationType = true;
    const dateObject: any = this.getDateBasedOnMonthYear(data);
    const formData = {
      month: dateObject.month,
      year: dateObject.year,
      fromDate: dateObject.fromDate,
      toDate: dateObject.toDate,
      ...this.filterForm.value
    };
    this.__referralMasterService.GetConfirmationTypeDateWise(formData).subscribe(res => {
      // Create chart instance
      const chart = am4core.create("chartdiv2", am4charts.XYChart);
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.filePrefix = 'Confirmation Type';
      chart.logo.disabled = true;


      // Increase contrast by taking evey second color
      chart.colors.step = 2;

      // Add data
      chart.data = res;//generateChartData();

      // Create axes


      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.minGridDistance = 50;
      dateAxis.dateFormats.setKey("day", "dd-MMM-yyyy");

      dateAxis.renderer.labels.template.horizontalCenter = "right";
      dateAxis.renderer.labels.template.verticalCenter = "middle";
      dateAxis.renderer.labels.template.rotation = 290;

      // Create series
      function createAxisAndSeries(field, name, opposite, bullet) {
        var valueAxis: any = chart.yAxes.push(new am4charts.ValueAxis());
        if (chart.yAxes.indexOf(valueAxis) != 0) {
          valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
        }

        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = field;
        series.dataFields.dateX = "date";
        series.strokeWidth = 2;
        series.yAxis = valueAxis;
        series.name = name;
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.tensionX = 0.8;
        series.showOnInit = true;

        let interfaceColors = new am4core.InterfaceColorSet();

        switch (bullet) {
          case "triangle":
            var bullet: any = series.bullets.push(new am4charts.Bullet());
            bullet.width = 12;
            bullet.height = 12;
            bullet.horizontalCenter = "middle";
            bullet.verticalCenter = "middle";

            var triangle = bullet.createChild(am4core.Triangle);
            triangle.stroke = interfaceColors.getFor("background");
            triangle.strokeWidth = 2;
            triangle.direction = "top";
            triangle.width = 12;
            triangle.height = 12;
            break;
          case "rectangle":
            var bullet: any = series.bullets.push(new am4charts.Bullet());
            bullet.width = 10;
            bullet.height = 10;
            bullet.horizontalCenter = "middle";
            bullet.verticalCenter = "middle";

            var rectangle = bullet.createChild(am4core.Rectangle);
            rectangle.stroke = interfaceColors.getFor("background");
            rectangle.strokeWidth = 2;
            rectangle.width = 10;
            rectangle.height = 10;
            break;
          default:
            var bullet: any = series.bullets.push(new am4charts.CircleBullet());
            bullet.circle.stroke = interfaceColors.getFor("background");
            bullet.circle.strokeWidth = 2;
            break;
        }

        valueAxis.renderer.line.strokeOpacity = 1;
        valueAxis.renderer.line.strokeWidth = 2;
        valueAxis.renderer.line.stroke = series.stroke;
        valueAxis.renderer.labels.template.fill = series.stroke;
        valueAxis.renderer.opposite = opposite;
      }

      createAxisAndSeries("clinicalEnrollment", "Clinical Enrollment", false, "circle");
      createAxisAndSeries("microbiologicalEnrollment", "Microbiological Confirmation", true, "triangle");
      // createAxisAndSeries("hits", "Hits", true, "rectangle");

      // Add legend
      chart.legend = new am4charts.Legend();
      // Add cursor
      chart.cursor = new am4charts.XYCursor();


    });
    this._cdr.detectChanges();
  }
  //----------------End Confirmation Type Graph--------------

  toggleConfirmationTypeMonthWise(event: MatSlideToggleChange) {
    this.showToggleConfirmationType = false;
    this.generateConfirmationGraphMonthWise();
    this._cdr.detectChanges();
  }

  getMonthOnMonthEnrollmentViewDashboard() {
    this.showTable = false;
    this.showProcessing = true;
    const filterValue = {
      fromDateYYYY_MM_DD: _moment(this.filterForm.value.date[0]).format('YYYY-MM-DD'),
      fromDate: new Date(this.filterForm.value.date[0]).getTime(),
      toDateYYYY_MM_DD: _moment(this.filterForm.value.date[1]).format('YYYY-MM-DD'),
      toDate: new Date(this.filterForm.value.date[1]).setHours(23, 59, 59)
    }

    this._patientMasterService.getMonthOnMonthEnrollmentViewDashboard(filterValue).subscribe(res => {
      this.tableData = res;
      this.showTable = true;
      this.showProcessing = false;
      this._cdr.detectChanges();
    }, err => {
      this.showTable = true;
      this.showProcessing = false;
      this._cdr.detectChanges();
    });

  }

  exportTable() {
    this.exportAsService.save(this.exportAsConfig, 'Enrollment Data Month Wise').subscribe(() => {
      // save started
    });
  }

  getDateBasedOnMonthYear(data: any): object {
    const month = data.month < 10 ? "0" + data.month : data.month;
    const fromDate = `${data.year}-${month}-01`;
    const toDate = `${data.year}-${month}-${data.month === ((new Date()).getMonth() + 1) ? new Date().getDate() : new Date(data.year, data.month, 0).getDate()}`;

    return { month: data.month, year: data.year, fromDate, toDate };
  }
  //-------------------------------
  getFieldOfficer(event) {
    this.filterForm.patchValue({ fieldOfficerList: event });

    const fieldOfficerList = this.filterForm.controls.fieldOfficerList.value;
    if (fieldOfficerList === null || fieldOfficerList.length === 0) {
      this.filterForm.controls.fieldOfficer.setValue("all");
    } else {
      this.filterForm.controls.fieldOfficer.setValue("selected");
    }
    this.changeMethod();
  }
  getDesignation(designation) {

    this.filterForm.patchValue({ designationList: designation });
    this.callProviderComponent.getUsersBasedOnDesignations(designation);

    const designationList = this.filterForm.controls.designationList.value;
    if (designationList === null || designationList.length === 0) {
      this.filterForm.controls.designation.setValue("all");
    } else {
      this.filterForm.controls.designation.setValue("selected");
    }
    this.changeMethod();

  }
  getProvider(event) {
    this.filterForm.patchValue({ providerList: event });

    const providerList = this.filterForm.controls.providerList.value;
    if (providerList === null || providerList.length === 0) {
      this.filterForm.controls.provider.setValue("all");
    } else {
      this.filterForm.controls.provider.setValue("selected");
    }
    this.changeMethod();

  }
  getDateRange(event) {
    const date = [
      _moment(event[0]).format('YYYY-MM-DD'),
      _moment(event[1]).format('YYYY-MM-DD'),
    ]
    this.filterForm.patchValue({ date });
    this.changeMethod();

  }
  getSiteOfDisease(event) {
    this.filterForm.patchValue({ siteOfDisease: event });
    this.changeMethod();
  }
  getCXROffered(event) {
    this.filterForm.patchValue({ CXROffered: event });
    this.changeMethod();
  }
  getCBNAATOffered(event) {
    this.filterForm.patchValue({ CBNAATOffered: event });
    this.changeMethod();
  }
  getFDC(event) {
    this.filterForm.patchValue({ FDCDone: event });
    this.changeMethod();
  }

  changeMethod() {
    this.showEnrollmentViewDateWiseGraph = false;
    this.showEnrollmentByUserTypeDateWise = false;
    this.generateEnrollmentViewGraph();
    this.generateEnrollmentByUserTypeGraph();
    this.generateConfirmationGraphMonthWise();
    this.getDirectNotificationMonthWise();
    this.getNotificationByUserType();
  }
}
