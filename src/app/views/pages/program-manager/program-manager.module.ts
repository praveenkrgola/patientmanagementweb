import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProgramManagerComponent } from "./program-manager.component";
import { ProgramManagerDashboardComponent } from "./program-manager-dashboard/program-manager-dashboard.component";
import { ErrorPageComponent } from "../../theme/content/error-page/error-page.component";
import { RouterModule } from "@angular/router";
import { PartialsModule } from "../../partials/partials.module";
import { CoreModule } from "../../../../../src/app/core/core.module";
import { ThemeModule } from "../../theme/theme.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FeedbackComponent } from "./feedback/feedback.component";
import { MapComponent } from "./map/map.component";
import { FacilityCreationComponent } from "./facility-creation/facility-creation.component";
import {
	MatDatepickerModule,
	MatDialogModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSlideToggleModule,
	MatTooltipModule,
	MatNativeDateModule,
	MatButtonToggleModule,
} from "@angular/material";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthTokenInterceptor } from "../../../../../src/services/util/interceptors/auth-token.interceptor";
import { InventoryManagementComponent } from './inventory-management/inventory-management.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SharedModule } from "../shared/shared.module";
import { EnrollmentViewComponent } from './enrollment-view/enrollment-view.component';
import { StockAssignmentComponent } from './stock-assignment/stock-assignment.component';
import { DeductStockComponent } from './deduct-stock/deduct-stock.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ProviderViewComponent } from './provider-view/provider-view.component';
import { FOViewComponent } from './fo-view/fo-view.component';
import { SCTViewComponent } from './sctview/sctview.component';
import { MatIconModule } from '@angular/material/icon';
import { UpdateAppComponent } from './update-app/update-app.component';
@NgModule({
	declarations: [
		ProgramManagerComponent,
		ProgramManagerDashboardComponent,
		FeedbackComponent,
		MapComponent,
		FacilityCreationComponent,
		InventoryManagementComponent,
		InvoiceComponent,
		EnrollmentViewComponent,
		StockAssignmentComponent,
		DeductStockComponent,
		ProviderViewComponent,
		FOViewComponent,
		SCTViewComponent,
		UpdateAppComponent,
	],
	imports: [
		PartialsModule,
		CoreModule,
		ThemeModule,
		ReactiveFormsModule,
		FormsModule,
		CommonModule,
		MatIconModule,
		MatSelectModule,
		MatRadioModule,
		MatTooltipModule,
		MatProgressSpinnerModule,
		MatDatepickerModule,
		MatSlideToggleModule,
		SharedModule,
		MatNativeDateModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		MatButtonToggleModule,
		RouterModule.forChild([
			{
				path: "",
				component: ProgramManagerComponent,
				// canActivate: [AdminGuard],
				children: [
					{
						path: "",
						redirectTo: "enrollmentview",
						pathMatch: "full",
					},
					// {
					// 	path: "dashboard",
					// 	component: ProgramManagerDashboardComponent,
					// 	data: { returnUrl: window.location.pathname },
					// },
					{
						path: "registration",
						component: FacilityCreationComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "inventory",
						component: InventoryManagementComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "feedback",
						component: FeedbackComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "invoice",
						component: InvoiceComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "enrollmentview",
						component: EnrollmentViewComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "providerview",
						component: ProviderViewComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "foview",
						component: FOViewComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "assignstock",
						component: StockAssignmentComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "deductstock",
						component: DeductStockComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "sctview",
						component: SCTViewComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "updateApp",
						component: UpdateAppComponent,
						data: { returnUrl: window.location.pathname },
					},
					{
						path: "error/403",
						component: ErrorPageComponent,
						data: {
							type: "error-v6",
							code: 403,
							title: "403... Access forbidden",
							desc:
								"Looks like you don't have permission to access for requested page.<br> Please, contact administrator",
						},
					},
					{ path: "error/:type", component: ErrorPageComponent },
					{
						path: "",
						redirectTo: "program-manager",
						pathMatch: "full",
					},
					{
						path: "**",
						redirectTo: "program-manager",
						pathMatch: "full",
					},
				],
			},
		]),
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthTokenInterceptor,
			multi: true,
		},
	],
})
export class ProgramManagerModule { }
