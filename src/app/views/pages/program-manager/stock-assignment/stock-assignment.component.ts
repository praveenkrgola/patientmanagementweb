import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UserLoginService } from '../../../../../services/user-login/user-login.service';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { StockAssignmentMasterService } from '../../../../../services/StockAssignmentMaster/stock-assignment-master.service';
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import Swal from "sweetalert2";
@Component({
  selector: 'kt-stock-assignment',
  templateUrl: './stock-assignment.component.html',
  styleUrls: ['./stock-assignment.component.scss']
})
export class StockAssignmentComponent implements OnInit {
  lastUpdated: any;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("matSort", { static: true }) sort: MatSort;
  constructor(
    private _userLoginService: UserLoginService,
    private _stockAssignmentMasterService: StockAssignmentMasterService,
    private _cdr: ChangeDetectorRef) { }
  user = JSON.parse(sessionStorage.getItem("user"));
  FOList: any = [];
  inventoryForm: FormGroup;
  showAssignProductTable: boolean = false;
  displayedColumns: String[] = ["sn", "name", "type", "content", "ageGroup", "status", "userPreviousBalance", "previousBalance", "stockIn",];
  dataSource: MatTableDataSource<any>;
  ngOnInit() {
    this.getFOList();
    this.inventoryForm = new FormGroup({
      fieldofficer: new FormControl(null, Validators.required),
      productArray: new FormArray([]),
    });
  }
  getFOList() {
    this._userLoginService.getList({ where: { designation: "fieldofficer" }, order: "name ASC", fields: ["id", "name"] }).subscribe(res => {
      this.FOList = res;
      this._cdr.detectChanges();
    });
  }

  proceedToAssignStock() {
    const FOId = this.inventoryForm.controls.fieldofficer.value;
    this._stockAssignmentMasterService.getStockDetails({ type: "FO", userId: FOId }).subscribe(res => {
      if (res) {
        this.lastUpdated = Math.max.apply(Math, res.map(data => new Date(data.updatedAt)));
        const productArray = this.inventoryForm.get("productArray") as FormArray;
        res.forEach((rs, i) => {
          const tempForm = new FormGroup({
            productId: new FormControl(rs.id),
            mainBalance: new FormControl(rs.previousBalance),
            previousBalance: new FormControl(rs.userPreviousBalance),
            stockIn: new FormControl(0, Validators.required)
          });
          productArray.push(tempForm);
        });
        this.inventoryForm.updateValueAndValidity();

        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.showAssignProductTable = true;
        this._cdr.detectChanges();
      }
    });
  }

  async submitStock() {
    const productArray = this.inventoryForm.value.productArray.filter(res => Boolean(res.stockIn));
    const total = productArray.reduce((global, current) => global + current.stockIn, 0);

    if (total > 0) {
      const FOId = this.inventoryForm.controls.fieldofficer.value;

      let obj = [];
      productArray.forEach(async (element, i) => {
        // console.log(i, " -- ", element.previousBalance, " -- ", element.stockIn);
        const temp = { stktransaction: "assignment", productId: element.productId, mainBalance: element.mainBalance, lastStock: element.previousBalance, currentStock: element.stockIn }
        obj.push(temp);
      });
      const finalObject = { type: "FO", userId: FOId, updatedBy: this.user.id, data: obj }
      this._stockAssignmentMasterService.SubmitOrUpdateStockUserWise(finalObject).subscribe(res => {
        this.clearProductArray();
        this.proceedToAssignStock();
        this._cdr.detectChanges();
        this.showAlert("success", "Stock Added Successfully!!")

      }, err => {
        this.showAssignProductTable = false;
        this._cdr.detectChanges();
        this.showAlert("error", "Oops!!, Something Went Wrong, Please Try Again.");
      });


    } else {
      this.showAlert("error", "Please Enter Stock Atleast For 1 Product !!")
    }

  }
  // addStock(data): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     this._productStockDetailsService.addStock(data).subscribe(res => {
  //       resolve(res)
  //     }, err => {
  //       reject(err)
  //     });
  //   });
  // }
  AddOrUpdateStockAssignmentMaster(data): Promise<any> {
    const commonDataObject = {
      type: "FO",
      userId: data.userId,
      productId: data.productId
    }
    return new Promise((resolve, reject) => {
      this._stockAssignmentMasterService.getCount(commonDataObject).subscribe(res => {
        if (res.count > 0) {
          const updateObject = { previousBalance: parseInt(data.lastStock) + parseInt(data.currentStock) }

          this._stockAssignmentMasterService.updateProduct(commonDataObject, updateObject).subscribe(res => {
            resolve(res)
          }, err => {
            reject(err)
          });
        } else {
          //When No Product Is Assigned....
          const obj = { ...commonDataObject, previousBalance: data.currentStock };
          this._stockAssignmentMasterService.addProduct(obj).subscribe(res => {
            resolve(res)
          }, err => {
            reject(err)
          });
        }
      });

    });
  }
  checkValue(i, element) {
    const stockIn = ((this.inventoryForm.get("productArray") as FormArray).at(i) as FormGroup).get("stockIn").value;
    if (parseInt(stockIn) > element.previousBalance) {
      ((this.inventoryForm.get('productArray') as FormArray).at(i) as FormGroup).get('stockIn').setValue(element.previousBalance);
      this.showAlert("error", "You can't assign stock more than available stock");
    }
  }
  clearProductArray() {
    this.inventoryForm.removeControl("productArray");
    this.inventoryForm.addControl("productArray", new FormArray([]));
  }
  getActualIndex(index: number) {
    return index + this.paginator.pageSize * this.paginator.pageIndex;
  }
  showAlert(icon, title) {
    const Toast = Swal.mixin({
      toast: true,
      position: "center",
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icon,
      title: title,
    });
  }
}
