import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAssignmentComponent } from './stock-assignment.component';

describe('StockAssignmentComponent', () => {
  let component: StockAssignmentComponent;
  let fixture: ComponentFixture<StockAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
