import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import _moment from "moment";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProviderComponent } from '../../shared/provider/provider.component';
import { ReferralMasterService } from '../../../../../services/referral-master/referral-master.service';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'kt-sctview',
  templateUrl: './sctview.component.html',
  styleUrls: ['./sctview.component.scss']
})
export class SCTViewComponent implements OnInit {
  constructor(private _referralMasterService: ReferralMasterService,
    private _cdr: ChangeDetectorRef,
    private exportAsService: ExportAsService) { }
  @ViewChild("callProviderComponent", { static: true })
  callProviderComponent: ProviderComponent;


  filterForm: FormGroup;
  currentDate = new Date();
  currentDay = (this.currentDate.getDate()) < 10 ? ("0" + (this.currentDate.getDate())) : (this.currentDate.getDate());
  currentMonth = (this.currentDate.getMonth() + 1) < 10 ? "0" + (this.currentDate.getMonth() + 1) : (this.currentDate.getMonth() + 1);
  currentYear = this.currentDate.getFullYear();
  toDate = `${this.currentYear}-${this.currentMonth}-${this.currentDay}`;
  threeMonthsAgo = new Date(_moment().subtract(3, 'months').format()).getMonth();
  threeMonthsAgoYear = new Date(_moment().subtract(3, 'months').format()).getFullYear();
  fromDateToDate: Date[] = [new Date(`${this.threeMonthsAgoYear}-${this.threeMonthsAgo + 1}-01`), new Date(this.toDate)];

  TilesData: any;
  TableData: any;
  showTable: boolean = false
  ngOnInit() {
    this.filterForm = new FormGroup({
      date: new FormControl(this.fromDateToDate),
      fieldOfficer: new FormControl("all"),
      fieldOfficerList: new FormControl(),

      designation: new FormControl("all"),
      designationList: new FormControl(),

      provider: new FormControl("all"),
      providerList: new FormControl(),

      siteOfDisease: new FormControl("all"),
      CXROffered: new FormControl("yes"),
      CBNAATOffered: new FormControl("yes"),
      FDCDone: new FormControl("yes"),

      sctAgent: new FormControl("all"),
      sctAgentList: new FormControl()
    });
    this.getTilesData();
    this.generateMTBStatusGraph();
    this.generateRifStatusGraph();
    this.getTableData();

  }

  getTilesData() {
    this._referralMasterService.GetTilesDataForSCTView(this.filterForm.value).subscribe((res) => {
      this.TilesData = res.TilesData;
      this._cdr.detectChanges();
    });
  }

  getTableData() {
    this._referralMasterService.GetTableDataForSCTView(this.filterForm.value).subscribe((res) => {
      console.log(res);
      this.TableData = res;
      this.showTable = true;
      this._cdr.detectChanges();
    });
  }


  generateMTBStatusGraph() {
    this._referralMasterService.GetMTB_Rif_StatusGraphForSCTView(this.filterForm.value).subscribe(res => {
      let chart = am4core.create('mtbstatusgraphdiv', am4charts.XYChart)
      chart.colors.step = 2;
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      chart.legend = new am4charts.Legend()
      chart.legend.position = 'top'
      chart.legend.paddingBottom = 20
      chart.legend.labels.template.maxWidth = 95

      let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
      xAxis.dataFields.category = 'monthYearLabel'
      xAxis.renderer.cellStartLocation = 0.1
      xAxis.renderer.cellEndLocation = 0.9
      xAxis.renderer.grid.template.location = 0;

      let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
      yAxis.min = 0;

      function createSeries(value, name) {
        let series = chart.series.push(new am4charts.ColumnSeries())
        series.dataFields.valueY = value
        series.dataFields.categoryX = 'monthYearLabel'
        series.name = name

        series.events.on("hidden", arrangeColumns);
        series.events.on("shown", arrangeColumns);

        let bullet = series.bullets.push(new am4charts.LabelBullet())
        bullet.interactionsEnabled = false
        bullet.dy = 30;
        bullet.label.text = '{valueY}'
        bullet.label.fill = am4core.color('#ffffff')

        return series;
      }
      console.log(res.MTBStatus);
      
      chart.data = res.MTBStatus;


      createSeries('TotalMtbDetected', 'Detected/Positive');
      createSeries('TotalMtbNotDetected', 'Not Detected/Negative');

      function arrangeColumns() {

        let series = chart.series.getIndex(0);

        let w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
        if (series.dataItems.length > 1) {
          let x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
          let x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
          let delta = ((x1 - x0) / chart.series.length) * w;
          if (am4core.isNumber(delta)) {
            let middle = chart.series.length / 2;

            let newIndex = 0;
            chart.series.each(function (series) {
              if (!series.isHidden && !series.isHiding) {
                series.dummyData = newIndex;
                newIndex++;
              }
              else {
                series.dummyData = chart.series.indexOf(series);
              }
            })
            let visibleCount = newIndex;
            let newMiddle = visibleCount / 2;

            chart.series.each(function (series) {
              let trueIndex = chart.series.indexOf(series);
              let newIndex = series.dummyData;

              let dx = (newIndex - trueIndex + middle - newMiddle) * delta

              series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
              series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
            })
          }
        }
      }

    });
  }
  generateRifStatusGraph() {
    this._referralMasterService.GetMTB_Rif_StatusGraphForSCTView(this.filterForm.value).subscribe(res => {

      let chart = am4core.create('rifstatusgraphdiv', am4charts.XYChart)
      chart.colors.step = 2;
      chart.scrollbarX = new am4core.Scrollbar();
      chart.logo.disabled = true;

      chart.legend = new am4charts.Legend()
      chart.legend.position = 'top'
      chart.legend.paddingBottom = 20
      chart.legend.labels.template.maxWidth = 95

      let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
      xAxis.dataFields.category = 'monthYearLabel'
      xAxis.renderer.cellStartLocation = 0.1
      xAxis.renderer.cellEndLocation = 0.9
      xAxis.renderer.grid.template.location = 0;

      let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
      yAxis.min = 0;

      function createSeries(value, name) {
        let series = chart.series.push(new am4charts.ColumnSeries())
        series.dataFields.valueY = value
        series.dataFields.categoryX = 'monthYearLabel'
        series.name = name

        series.events.on("hidden", arrangeColumns);
        series.events.on("shown", arrangeColumns);

        let bullet = series.bullets.push(new am4charts.LabelBullet())
        bullet.interactionsEnabled = false
        bullet.dy = 30;
        bullet.label.text = '{valueY}'
        bullet.label.fill = am4core.color('#ffffff')

        return series;
      }

      chart.data = res.RifStatus;


      createSeries('TotalRifNotDetected', 'Not Detected/Rif Sensitive');
      createSeries('TotalRifIndeterminateDetected', 'Indeterminate');
      createSeries('TotalRifDetected', 'Detected/Rif Resistant');

      function arrangeColumns() {

        let series = chart.series.getIndex(0);

        let w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
        if (series.dataItems.length > 1) {
          let x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
          let x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
          let delta = ((x1 - x0) / chart.series.length) * w;
          if (am4core.isNumber(delta)) {
            let middle = chart.series.length / 2;

            let newIndex = 0;
            chart.series.each(function (series) {
              if (!series.isHidden && !series.isHiding) {
                series.dummyData = newIndex;
                newIndex++;
              }
              else {
                series.dummyData = chart.series.indexOf(series);
              }
            })
            let visibleCount = newIndex;
            let newMiddle = visibleCount / 2;

            chart.series.each(function (series) {
              let trueIndex = chart.series.indexOf(series);
              let newIndex = series.dummyData;

              let dx = (newIndex - trueIndex + middle - newMiddle) * delta

              series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
              series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
            })
          }
        }
      }
    
    });  
  }

  getFieldOfficer(event) {
    this.filterForm.patchValue({ fieldOfficerList: event });

    const fieldOfficerList = this.filterForm.controls.fieldOfficerList.value;
    if (fieldOfficerList === null || fieldOfficerList.length === 0) {
      this.filterForm.controls.fieldOfficer.setValue("all");
    } else {
      this.filterForm.controls.fieldOfficer.setValue("selected");
    }
    this.changeMethod();
  }
  getDesignation(designation) {

    this.filterForm.patchValue({ designationList: designation });
    this.callProviderComponent.getUsersBasedOnDesignations(designation);

    const designationList = this.filterForm.controls.designationList.value;
    if (designationList === null || designationList.length === 0) {
      this.filterForm.controls.designation.setValue("all");
    } else {
      this.filterForm.controls.designation.setValue("selected");
    }
    this.changeMethod();
  }
  getProvider(event) {
    this.filterForm.patchValue({ providerList: event });

    const providerList = this.filterForm.controls.providerList.value;
    if (providerList === null || providerList.length === 0) {
      this.filterForm.controls.provider.setValue("all");
    } else {
      this.filterForm.controls.provider.setValue("selected");
    }
    this.changeMethod();

  }
  getDateRange(event) {
    const date = [
      _moment(event[0]).format('YYYY-MM-DD'),
      _moment(event[1]).format('YYYY-MM-DD'),
    ]
    this.filterForm.patchValue({ date });
    this.changeMethod();

  }
  getSiteOfDisease(event) {
    this.filterForm.patchValue({ siteOfDisease: event });
    this.changeMethod();
  }
  getCXROffered(event) {
    this.filterForm.patchValue({ CXROffered: event });
    this.changeMethod();
  }
  getCBNAATOffered(event) {
    this.filterForm.patchValue({ CBNAATOffered: event });
    this.changeMethod();
  }
  getFDC(event) {
    this.filterForm.patchValue({ FDCDone: event });
    this.changeMethod();

  }
  getSCT(event) {
    this.filterForm.patchValue({ sctAgentList: event });
    const sctAgentList = this.filterForm.controls.sctAgentList.value;
    if (sctAgentList === null || sctAgentList.length === 0) {
      this.filterForm.controls.sctAgent.setValue("all");
    } else {
      this.filterForm.controls.sctAgent.setValue("selected");
    }
    this.changeMethod();

  }

	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementIdOrContent: 'tableId1', // the id of html/table element
	  }
	exportTable() {
		this.exportAsService.save(this.exportAsConfig, 'SCT View Month Wise Data').subscribe(() => {
		  // save started
		});
	  }
  changeMethod() {
    this.getTilesData();
    this.generateMTBStatusGraph();
    this.generateRifStatusGraph();
    this.getTableData();
  }
}
