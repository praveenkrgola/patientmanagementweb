import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SCTViewComponent } from './sctview.component';

describe('SCTViewComponent', () => {
  let component: SCTViewComponent;
  let fixture: ComponentFixture<SCTViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SCTViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SCTViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
