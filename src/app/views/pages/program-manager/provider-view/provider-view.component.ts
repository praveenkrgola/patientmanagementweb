import { ChangeDetectorRef, Component, createPlatform, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ReferralMasterService } from '../../../../../../src/services/referral-master/referral-master.service';
import { ProviderComponent } from '../../shared/provider/provider.component';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import _moment from "moment";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

@Component({
	selector: "kt-provider-view",
	templateUrl: "./provider-view.component.html",
	styleUrls: ["./provider-view.component.scss"],
})
export class ProviderViewComponent implements OnInit {
	@ViewChild("callProviderComponent", { static: true })
	callProviderComponent: ProviderComponent;
	filterForm: FormGroup;
	CXRByUserType: any;
	consultationByUserType: any;
	CXRIssueRedeemedRatioData: any;
	issuedEnrolledRatioData: any;
	vouchersMonthWiseData: any;
	fromDateToDate: Date[];
	dataOfTiles: any;
	constructor(
		private _cdr: ChangeDetectorRef,
		private _referralMasterService: ReferralMasterService,
		private exportAsService: ExportAsService

	) {
		let today = new Date();
		let fromDate = new Date();
		fromDate.setDate(fromDate.getDate()-85);
    	const fromDate2= new Date(`${fromDate.getFullYear()}-${fromDate.getMonth()+1}-01`);
		this.fromDateToDate = [fromDate2, today];

		this.createForm();
	}
	createForm() {
		this.filterForm = new FormGroup({
			fieldOfficerList: new FormControl(),
			designationList: new FormControl(),
			providerList: new FormControl(),
			date: new FormControl(this.fromDateToDate),
			siteOfDisease: new FormControl(),
			cxrOffered: new FormControl(),
			cbnaatOffered: new FormControl(),
			fdc: new FormControl(),
		});
	}
	async ngOnInit() {

		const form = this.getForm();
		console.log(form);
		
		this._referralMasterService
			.getProviderViewDashboardData(form)
			.subscribe((res) => {
				this.CXRByUserType = res.CXRByUserType;
				this.consultationByUserType = res.consultationByUserType;
				this.generateCXRByUserTypeGraph();
				this.generateConsultationByUserTypeGraph();
			});
		await this.getCXRIssuedRedeemedRatioData(form);
		await this.getIssuedEnrolledRatioData(form);
		await this.getVouchersMonthWiseData(form);
		await this.getDataOfTiles(form);
		
		this.filterForm.valueChanges.subscribe(async (x) => {
			const form = this.getForm();
			await this.getCXRIssuedRedeemedRatioData(form);
			await this.getIssuedEnrolledRatioData(form); 
			await this.getVouchersMonthWiseData(form);
			await this.getDataOfTiles(form);
			this._referralMasterService
				.getProviderViewDashboardData(form)
				.subscribe((res) => {
					this.CXRByUserType = res.CXRByUserType;
					this.consultationByUserType = res.consultationByUserType;
					this.generateCXRByUserTypeGraph();
					this.generateConsultationByUserTypeGraph();
				});
		});
	}
	getDataOfTiles(filter: any):Promise<any> {
		return new Promise((resolve, reject) => {
			this._referralMasterService.getDataOfTilesForProviderViewDashboard(filter)
				.subscribe(
					(res) => {
						if (res) {
							this.dataOfTiles = res;
							resolve(res);
							this._cdr.detectChanges();
						}
					},
					(err) => reject(err)
				);
		})
	}
	getForm() {
		let form = this.filterForm.value;
		form = Object.entries(form).reduce(
			(a, [k, v]: any) =>
			v == null || v.length == 0 ? a : ((a[k] = v), a),
			{}
		);
		return form;
	}
	getVouchersMonthWiseData(filter: any):Promise<any> {
		return new Promise((resolve, reject) => {
			this._referralMasterService.getVouchersDataMonthWiseForProviderViewDashboard(filter)
				.subscribe(
					(res) => {
						if (res) {
							this.vouchersMonthWiseData = res;
							resolve(res);
							this._cdr.detectChanges();
						}
					},
					(err) => reject(err)
				);
		})
	}
	getCXRIssuedRedeemedRatioData(filter): Promise<any> {
		return new Promise((resolve, reject) => {
			this._referralMasterService
				.issuedRedeemedRatioGraphsForProviderViewDashboard(filter)
				.subscribe(
					(res) => {
						if (res) {
							this.CXRIssueRedeemedRatioData = res;
							this.generateCXRIssuedRedeemedRatioGraph(
								this.CXRIssueRedeemedRatioData
							);
							resolve(res);
						}
					},
					(err) => reject(err)
				);
		});
	}
	getIssuedEnrolledRatioData(filter): Promise<any> {
		return new Promise((resolve, reject) => {
			this._referralMasterService
				.issuesEnrolledRatioGraphsForProviderViewDashboard(filter)
				.subscribe(
					(res) => {
						if (res) {
							this.CXRIssueRedeemedRatioData = res;
							this.issuedEnrolledRatioData = res;
							this.generateIssuedEnrolledRatioGraph(
								this.issuedEnrolledRatioData
							);
							resolve(res);
						}
					},
					(err) => reject(err)
				);
		});
	}

	generateCXRIssuedRedeemedRatioGraph(data) {
		let chart = am4core.create("cxrIssuedClaimedRatio", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'CXR Issued/Claimed Ratio';
		chart.data = data;
		chart.logo.disabled = true;
		// Create axes
		let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "providerUsername";
		categoryAxis.numberFormatter.numberFormat = "#";
		categoryAxis.renderer.inversed = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;
		let label = categoryAxis.renderer.labels.template;
		label.wrap = true;
		label.maxWidth = 250;
		let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.opposite = true;
		valueAxis.max = 100;
		// Create series
		function createSeries(field, name) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueX = field;
			series.dataFields.categoryY = "providerUsername";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueX}%[/]";
			series.columns.template.height = 20; //am4core.percent(100);
			series.columns.template.width = am4core.percent(100);
			series.sequencedInterpolation = true;
			
			let valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = "{valueX}%";
			valueLabel.label.horizontalCenter = "left";
			valueLabel.label.dx = 10;
			valueLabel.label.hideOversized = false;
			valueLabel.label.truncate = false;

			let categoryLabel = series.bullets.push(
				new am4charts.LabelBullet()
			);
			// categoryLabel.label.text = "{name}";
			categoryLabel.label.horizontalCenter = "right";
			categoryLabel.label.dx = -10;
			categoryLabel.label.fill = am4core.color("#fff");
			categoryLabel.label.hideOversized = false;
			categoryLabel.label.truncate = false;
		}

		createSeries("ratio", "Ratio");
		chart.cursor = new am4charts.XYCursor();

		// createSeries("expenses", "Expenses");

	}
	generateIssuedEnrolledRatioGraph(data) {
		let chart = am4core.create("issuedEnrolledPatientRatio", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Issued/Enrolled Patient Ratio';
		chart.data = data;
		chart.logo.disabled = true;
		// Create axes
		let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "providerUsername";
		categoryAxis.numberFormatter.numberFormat = "#";
		categoryAxis.renderer.inversed = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;
		let label = categoryAxis.renderer.labels.template;
		label.wrap = true;
		label.maxWidth = 250;
		let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.opposite = true;
		valueAxis.max = 100;
		// Create series
		function createSeries(field, name) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueX = field;
			series.dataFields.categoryY = "providerUsername";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueX}%[/]";
			series.columns.template.height = 20; //am4core.percent(100);
			series.columns.template.width = am4core.percent(100);
			series.sequencedInterpolation = true;

			let valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = "{valueX}%";
			valueLabel.label.horizontalCenter = "left";
			valueLabel.label.dx = 10;
			valueLabel.label.hideOversized = false;
			valueLabel.label.truncate = false;

			let categoryLabel = series.bullets.push(
				new am4charts.LabelBullet()
			);
			// categoryLabel.label.text = "{name}";
			categoryLabel.label.horizontalCenter = "right";
			categoryLabel.label.dx = -10;
			categoryLabel.label.fill = am4core.color("#fff");
			categoryLabel.label.hideOversized = false;
			categoryLabel.label.truncate = false;
		}

		createSeries("ratio", "Ratio");
		chart.cursor = new am4charts.XYCursor();

		// createSeries("expenses", "Expenses");

		// chart.padding(40, 40, 40, 40);
		// let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		// categoryAxis.renderer.grid.template.location = 0;
		// categoryAxis.dataFields.category = "providerUsername";
		// categoryAxis.renderer.minGridDistance = 1;
		// categoryAxis.renderer.inversed = true;
		// categoryAxis.renderer.grid.template.disabled = true;

		// let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		// valueAxis.min = 0;

		// let series = chart.series.push(new am4charts.ColumnSeries());
		// series.dataFields.categoryY = "providerUsername";
		// series.dataFields.valueX = "ratio";
		// series.tooltipText = "{valueX.value}";
		// series.columns.template.strokeOpacity = 0;
		// series.columns.template.column.cornerRadiusBottomRight = 5;
		// series.columns.template.column.cornerRadiusTopRight = 5;

		// let labelBullet = series.bullets.push(new am4charts.LabelBullet());
		// labelBullet.label.horizontalCenter = "left";
		// labelBullet.label.dx = 10;
		// labelBullet.label.text =
		// 	"{values.valueX.workingValue.formatNumber('#.0as')}";
		// labelBullet.locationX = 1;

		// // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
		// series.columns.template.adapter.add("fill", function (fill, target) {
		// 	return chart.colors.getIndex(target.dataItem.index);
		// });

		// categoryAxis.sortBySeries = series;
		// chart.data = data;
	}

	getFieldOfficer(event) {
		console.log(event);
		this.filterForm.patchValue({ fieldOfficerList: event });
	}
	getDesignation(designation) {
		console.log(designation);
		this.filterForm.patchValue({ designationList: designation });
		this.callProviderComponent.getUsersBasedOnDesignations(designation);
	}
	getProvider(event) {
		console.log(event);
		this.filterForm.patchValue({ providerList: event });
	}
	getDateRange(event) {
		console.log(event);
		const date = [
			_moment(event[0]).format('YYYY-MM-DD'),
			_moment(event[1]).format('YYYY-MM-DD'),
		]
		
		this.filterForm.patchValue({ date: date });
	}
	getSiteOfDisease(event) {
		console.log(event);
		this.filterForm.patchValue({ siteOfDisease: event });
	}
	getCXROffered(event) {
		console.log(event);
		this.filterForm.patchValue({ cxrOffered: event });
	}
	getCBNAATOffered(event) {
		console.log(event);
		this.filterForm.patchValue({ cbnaatOffered: event });
	}
	getFDC(event) {
		console.log(event);
		this.filterForm.patchValue({ fdc: event });
	}

	generateCXRByUserTypeGraph() {
		const that = this;

		// Create chart instance
		let chart = am4core.create("cxrByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'CXR By User Type';
		chart.colors.list = [
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
			am4core.color("#67b7dc"),
			am4core.color("#f3c300"),
		];
		chart.logo.disabled = true;
		chart.data = this.CXRByUserType;
		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "voucherCreatedAtDate";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;
		
		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "voucherCreatedAtDate";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			// if (that.CXRByUserType.length > 5) {	
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("chemistCount", "Chemist", true);
		createSeries("empanelledChemistCount", "Empanelled Chemist", true);
		createSeries("generalDoctorCount", "General Doctor", true);
		createSeries("empanelledDoctorCount", "Empanelled Doctor", true);
		createSeries("ayushProviderCount", "Ayush", true);

		// Add legend
		chart.legend = new am4charts.Legend();
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.behavior = "panX";
		// chart.cursor.xAxis = categoryAxis;

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
		this._cdr.detectChanges();
	}
	generateConsultationByUserTypeGraph() {
		const that = this;
		// Create chart instance
		let chart = am4core.create("consultationByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Consultation By User Type';
		chart.colors.list = [
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
			am4core.color("#67b7dc"),
			am4core.color("#f3c300"),
		];
		chart.logo.disabled = true;
		chart.data = this.consultationByUserType;

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "voucherCreatedAtDate";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "voucherCreatedAtDate";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			// if (that.consultationByUserType.length > 5) {	
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("labCount", "Lab", false);
		createSeries("ayushProviderCount", "Ayush", false);

		// Add legend
		chart.legend = new am4charts.Legend();
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.behavior = "panX";

		// chart.preloader.disabled = true;
		// chart.cursor.xAxis = categoryAxis;

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
		this._cdr.detectChanges();
	}

	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementIdOrContent: 'tableId1', // the id of html/table element
	  }
	exportTable() {
		this.exportAsService.save(this.exportAsConfig, 'Vouchers Data Month Wise').subscribe(() => {
		  // save started
		});
	  }
}
