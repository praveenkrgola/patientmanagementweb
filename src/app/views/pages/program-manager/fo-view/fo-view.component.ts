import { ChangeDetectorRef, Component, ElementRef, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProviderComponent } from '../../shared/provider/provider.component';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import _moment from "moment";
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';
import { FieldOfficerActivityService } from '../../../../../../src/services/FieldOfficerActivity/field-officer-activity.service';
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

@Component({
	selector: 'kt-fo-view',
	templateUrl: './fo-view.component.html',
	styleUrls: ['./fo-view.component.scss']
})
export class FOViewComponent implements OnInit {
	@ViewChild("callProviderComponent", { static: true }) callProviderComponent: ProviderComponent;
	@ViewChild("filter", { static: true }) filter:ElementRef;
	@ViewChild("toggle", { static: true }) toggle:ElementRef;
	showFilterButton: boolean = false;

	filterForm: FormGroup;
	fromDateToDate: Date[];
	visitDataByUserType: any;
	mothWiseVisitDataByUserType: any;
	showVisitDataToggleButton: boolean = false;
	showConsultaionDataToggleButton: boolean = false;
	monthWiseConsultationData: any;
	dateWiseConsultationData: any;
	dataOfTiles: any;
	visitDataTable: any;
	servicePerformanceDataSource: any;
	constructor(
		private _cdr: ChangeDetectorRef,
		private _fieldOfficerActivityService: FieldOfficerActivityService,
		private exportAsService: ExportAsService
	) {

		let today = new Date();
		let fromDate = new Date();
		fromDate.setDate(fromDate.getDate() - 85);
		// Replace restrictDate with fromDate2 when FO Activity Data is updated with the providerId
		const fromDate2 = new Date(`${fromDate.getFullYear()}-${fromDate.getMonth() + 1}-01`);
		const restrictDate = new Date("2021-05-10");
		this.fromDateToDate = [restrictDate, today];
		this.createForm();
	}
	createForm() {
		this.filterForm = new FormGroup({
			fieldOfficerList: new FormControl(),
			designationList: new FormControl(),
			providerList: new FormControl(),
			date: new FormControl(this.fromDateToDate),
			siteOfDisease: new FormControl(),
			cxrOffered: new FormControl(),
			cbnaatOffered: new FormControl(),
			fdc: new FormControl(),
		});
	}
	async ngOnInit() {
		const form = this.getForm();
		console.log(form);
		await this.getMonthWiseDataByUserTypeForFOViewDashboard(form);
		await this.getMonthWiseConsultationByUserTypeForFODashboard(form);
		await this.getTilesDataForFODashboard(form);
		await this.getVisitDatatable(form);
		await this.servicePerformanceDatatable(form);
		
		// ********* Subscribe when filters change ***********
		this.filterForm.valueChanges.subscribe(async (x) => {
			const form = this.getForm();
			await this.getMonthWiseDataByUserTypeForFOViewDashboard(form);
			await this.getMonthWiseConsultationByUserTypeForFODashboard(form);
			await this.getTilesDataForFODashboard(form);
			await this.getVisitDatatable(form);
			await this.servicePerformanceDatatable(form);
		});
	}

	// ********** Tiles Data **************
	getTilesDataForFODashboard(form: any) {
		this._fieldOfficerActivityService.getTilesDataForFODashboard(form).subscribe(res => {
			this.dataOfTiles = res[0];
		})
		this._cdr.detectChanges();
	}
	// ********** Tiles Data Ends **************

	// ********** TABLE Data **************
	getVisitDatatable(form: any) {
		this._fieldOfficerActivityService.visitDataTableForFODashboard(form).subscribe(res => {
			this.visitDataTable = res;
			this._cdr.detectChanges();
		})
		this._cdr.detectChanges();
	}
	servicePerformanceDatatable(form: any) {
		this._fieldOfficerActivityService.servicePerformanceDatatable(form).subscribe(res => {
			this.servicePerformanceDataSource = res;
			this._cdr.detectChanges();
		})
		this._cdr.detectChanges();
	}
	// ********** Table Data Ends **************
	// ********** Consultation By User Type Starts **************
	getMonthWiseConsultationByUserTypeForFODashboard(form: any) {
		this._fieldOfficerActivityService.getMonthWiseConsultationByUserTypeForFODashboard(form).subscribe(res => {
			this.monthWiseConsultationData = res;
			this.generateMonthWiseConsultationGraph();
		})
	}
	generateMonthWiseConsultationGraph() {
		const that = this;

		// Create chart instance
		let chart = am4core.create("consultationByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Visit Data By User Type';
		chart.colors.list = [
			am4core.color("#f3c300"),
			am4core.color("#67b7dc"),
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
		];
		chart.logo.disabled = true;

		chart.data = this.monthWiseConsultationData;
		//   var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		//   dateAxis.renderer.minGridDistance = 50;
		//   dateAxis.dateFormats.setKey("day", "dd-MMM-yyyy");

		//   dateAxis.renderer.labels.template.horizontalCenter = "right";
		//   dateAxis.renderer.labels.template.verticalCenter = "middle";
		//   dateAxis.renderer.labels.template.rotation = 290;

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "monthYear";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked, that) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "monthYear";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.columns.template.events.on("hit", function (ev) {
				that.showConsultaionDataToggleButton = true;
				const data = ev.target.dataItem.dataContext;
				that.getDateWiseConsultationByUserType(data);
			}, this);
			series.stacked = stacked;
			// if (that.mothWiseVisitDataByUserType.length < 5) {
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			// bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("labCount", "Lab", false, this);
		createSeries("ayushProviderCount", "Ayush", false, this);

		// Add legend
		chart.legend = new am4charts.Legend();
		chart.cursor = new am4charts.XYCursor();

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
	}
	getDateWiseConsultationByUserType(data){

		const form = this.getForm();
		form["voucherCreatedAtMonth"] = data.voucherCreatedAtMonth;
		form["voucherCreatedAtYear"] = data.voucherCreatedAtYear;
		console.log(form);
		this._fieldOfficerActivityService.getConsultationByUserTypeForFODashboard(form).subscribe(res => {
			this.dateWiseConsultationData = res;
			this.generateDateWiseConsultaionGraph();
			this._cdr.detectChanges();
		});
	}
	generateDateWiseConsultaionGraph() {
		const that = this;

		// Create chart instance
		let chart = am4core.create("consultationByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Visit Data By User Type';
		chart.colors.list = [
			am4core.color("#f3c300"),
			am4core.color("#67b7dc"),
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
		];
		chart.logo.disabled = true;
		chart.data = this.dateWiseConsultationData;
		//   var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		//   dateAxis.renderer.minGridDistance = 50;
		//   dateAxis.dateFormats.setKey("day", "dd-MMM-yyyy");

		//   dateAxis.renderer.labels.template.horizontalCenter = "right";
		//   dateAxis.renderer.labels.template.verticalCenter = "middle";
		//   dateAxis.renderer.labels.template.rotation = 290;

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "voucherCreatedAtDate";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked, that) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "voucherCreatedAtDate";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			// if (that.mothWiseVisitDataByUserType.length < 5) {
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			// bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("labCount", "Lab", false, this);
		createSeries("ayushProviderCount", "Ayush", false, this);

		// Add legend
		chart.legend = new am4charts.Legend();
		chart.cursor = new am4charts.XYCursor();

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
	}
	toggleShowConsultaionDataToggle(val) {
		this.showConsultaionDataToggleButton = false;
		const form = this.getForm();
		this.getMonthWiseConsultationByUserTypeForFODashboard(form);		
	}
	// ********** Consultation By User Type Ends **************

	// ********** VISIT DATA By User Type Starts **************
	getMonthWiseDataByUserTypeForFOViewDashboard(form: any) {
		this._fieldOfficerActivityService.getMonthWiseDataByUserTypeForFOViewDashboard(form).subscribe(res => {
			this.mothWiseVisitDataByUserType = res;
			this.generateMonthWiseVisitDataByUserTypeGraph();
		});
	}
	generateMonthWiseVisitDataByUserTypeGraph() {
		const that = this;

		// Create chart instance
		let chart = am4core.create("visitDataByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Visit Data By User Type';
		chart.colors.list = [
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
			am4core.color("#67b7dc"),
			am4core.color("#f3c300"),
		];
		chart.logo.disabled = true;
		chart.data = this.mothWiseVisitDataByUserType;
		//   var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		//   dateAxis.renderer.minGridDistance = 50;
		//   dateAxis.dateFormats.setKey("day", "dd-MMM-yyyy");

		//   dateAxis.renderer.labels.template.horizontalCenter = "right";
		//   dateAxis.renderer.labels.template.verticalCenter = "middle";
		//   dateAxis.renderer.labels.template.rotation = 290;

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "monthYear";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked, that) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "monthYear";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.columns.template.events.on("hit", function (ev) {
				that.showVisitDataToggleButton = true;
				const data = ev.target.dataItem.dataContext;
				that.getDataByUserTypeForFOViewDashboard(data);
			}, this);
			series.stacked = stacked;
			// if (that.mothWiseVisitDataByUserType.length < 5) {
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			// bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("chemistCount", "Chemist", false, this);
		createSeries("empanelledChemistCount", "Empanelled Chemist", false, this);
		createSeries("generalDoctorCount", "General Doctor", false, this);
		createSeries("empanelledDoctorCount", "Empanelled Doctor", false, this);
		createSeries("ayushProviderCount", "Ayush", false, this);

		// Add legend
		chart.legend = new am4charts.Legend();
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.xAxis = categoryAxis;

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
	}

	getDataByUserTypeForFOViewDashboard(data) {

		const form = this.getForm();
		form["month"] = data.month;
		form["year"] = data.year;
		console.log(form);
		this._fieldOfficerActivityService.getDataByUserTypeForFOViewDashboard(form).subscribe(res => {
			this.visitDataByUserType = res.visitData;
			this.generateVisitDataByUserTypeGraph();
			this._cdr.detectChanges();
		});
	}
	generateVisitDataByUserTypeGraph() {
		const that = this;

		// Create chart instance
		let chart = am4core.create("visitDataByUserType", am4charts.XYChart);
		chart.exporting.menu = new am4core.ExportMenu();
		chart.exporting.filePrefix = 'Visit Data By User Type';
		chart.colors.list = [
			am4core.color("#d5433d"),
			am4core.color("#9c27b0"),
			am4core.color("#3f51b5"),
			am4core.color("#67b7dc"),
			am4core.color("#f3c300"),
		];
		chart.logo.disabled = true;
		chart.data = this.visitDataByUserType;
		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "submissionDateStr";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "submissionDateStr";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			// if (that.CXRByUserType.length > 5) {	
			// 	series.columns.template.width = 50;//am4core.percent(50);
			// }
			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			// bullet1.label.text = "{valueY}";
			// bullet1.fontWeight = "bold";
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("chemistCount", "Chemist", false);
		createSeries("empanelledChemistCount", "Empanelled Chemist", false);
		createSeries("generalDoctorCount", "General Doctor", false);
		createSeries("empanelledDoctorCount", "Empanelled Doctor", false);
		createSeries("ayushProviderCount", "Ayush", false);

		// Add legend
		chart.legend = new am4charts.Legend();
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.xAxis = categoryAxis;

		// Add scrollbar
		chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
	}
	toggleDataByUserType(val) {
		this.showVisitDataToggleButton = false;
		const form = this.getForm();
		this.getMonthWiseDataByUserTypeForFOViewDashboard(form);
	}
	// ********** VISIT DATA By User Type Ends **************

	getForm() {
		let form = this.filterForm.value;
		form = Object.entries(form).reduce(
			(a, [k, v]: any) =>
				v == null || v.length == 0 ? a : ((a[k] = v), a),
			{}
		);
		return form;
	}
	getFieldOfficer(event) {
		console.log(event);
		this.filterForm.patchValue({ fieldOfficerList: event });
	}
	getDesignation(designation) {
		console.log(designation);
		this.filterForm.patchValue({ designationList: designation });
		this.callProviderComponent.getUsersBasedOnDesignations(designation);
	}
	getProvider(event) {
		console.log(event);
		this.filterForm.patchValue({ providerList: event });
	}
	getDateRange(event) {
		console.log(event);
		const date = [
			_moment(event[0]).format('YYYY-MM-DD'),
			_moment(event[1]).format('YYYY-MM-DD'),
		]

		this.filterForm.patchValue({ date: date });
	}
	getSiteOfDisease(event) {
		console.log(event);
		this.filterForm.patchValue({ siteOfDisease: event });
	}
	getCXROffered(event) {
		console.log(event);
		this.filterForm.patchValue({ cxrOffered: event });
	}
	getCBNAATOffered(event) {
		console.log(event);
		this.filterForm.patchValue({ cbnaatOffered: event });
	}
	getFDC(event) {
		console.log(event);
		this.filterForm.patchValue({ fdc: event });
	}
	exportAsConfig: ExportAsConfig = {
		type: 'xlsx', // the type you want to download
		elementIdOrContent: 'tableId1', // the id of html/table element
	  }
	exportTable() {
		this.exportAsService.save(this.exportAsConfig, 'FO Month Wise Data').subscribe(() => {
		  // save started
		});
	}
	
	toggleFilter() {
		if (this.showFilterButton === false) {
			this.showFilterButton = true;
			// this.filter.nativeElement.style.left = "0px";
			// this.toggle.nativeElement.style.left = "calc(25% - 13px)";

			this.filter.nativeElement.classList.add('toggle_filter_box');
			this.toggle.nativeElement.classList.add('toggle_filter_button');
		} else {			
			this.showFilterButton = false;
			this.filter.nativeElement.classList.remove('toggle_filter_box');
			this.toggle.nativeElement.classList.remove('toggle_filter_button');
			// this.filter.nativeElement.style.left = "-100%";
			// this.toggle.nativeElement.style.left = "-13px";
			
		}
	}
}
