import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FOViewComponent } from './fo-view.component';

describe('FOViewComponent', () => {
  let component: FOViewComponent;
  let fixture: ComponentFixture<FOViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FOViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FOViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
