import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { UserLoginService } from "../../../../../../src/services/user-login/user-login.service";
import { ProductMasterService } from "../../../../../../src/services/product-master/product-master.service";
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import Swal from "sweetalert2";
import { ProductStockDetailsService } from '../../../../../services/ProductStockDetails/product-stock-details.service';


//FOR YEAR AND MONTH CALENDAR 
@Component({
	selector: "kt-inventory-management",
	templateUrl: "./inventory-management.component.html",
	styleUrls: ["./inventory-management.component.scss"]
})
export class InventoryManagementComponent implements OnInit {
	user = JSON.parse(sessionStorage.getItem("user"));

	lastUpdated: any;
	inventoryForm: FormGroup;
	productTypeList = [{ type: "RHZE" }, { type: "RHE" }, { type: "RH" }, { type: "RHZ" }, { type: "RH" }, { type: "H" }, { type: "E" }];
	contentList = [{ type: "150/75/400/275" }, { type: "150/75/275" }, { type: "150/75" }, { type: "400" }, { type: "150" }, { type: "400" }, { type: "300" }];

	allUsers = [];
	allUsersForSearching = [];
	allProducts: any;
	allProductsForSearching = [];
	displayedColumns: String[] = ["sn", "name", "type", "content", "ageGroup", "status", "previousBalance", "stockIn", "action"];

	showAssignProductTable: boolean = false;
	dataSource: MatTableDataSource<any>;
	@ViewChild("paginator", { static: true }) paginator: MatPaginator;
	@ViewChild("matSort", { static: true }) sort: MatSort;
	constructor(
		private _productMasterService: ProductMasterService,
		private _userLoginService: UserLoginService,
		private _fb: FormBuilder,
		private _cdr: ChangeDetectorRef,
		private _productStockDetailsService: ProductStockDetailsService

	) {
		this.inventoryForm = new FormGroup({
			productArray: new FormArray([]),

			sku: new FormControl(null, Validators.required),
			productType: new FormControl(null, Validators.required),
			content: new FormControl(null, Validators.required),
			ageGroup: new FormControl(null, Validators.required)
		});


	}

	clearProductArray() {
		this.inventoryForm.removeControl("productArray");
		this.inventoryForm.addControl("productArray", this._fb.array([]));
	}



	ngOnInit() {
		this.getAllProducts();
	}

	getAllProducts() {
		this._productMasterService.getProductList({ order: "name ASC" }).subscribe((res: any) => {
			this.lastUpdated = Math.max.apply(Math, res.map(data => data.lastUpdatedAt));

			const productArray = this.inventoryForm.get("productArray") as FormArray;
			res.forEach((rs, i) => {
				//console.log("name : ", rs.name, " -- status : ", rs.status);

				const tempForm = new FormGroup({
					i: new FormControl(i),
					productId: new FormControl(rs.id),
					previousBalance: new FormControl(rs.previousBalance),
					stockIn: new FormControl(0, Validators.required)
				});
				productArray.push(tempForm);

			});
			this.inventoryForm.updateValueAndValidity();
			this.dataSource = new MatTableDataSource(res);
			// this.allProducts = res;
			// this.allProductsForSearching = res;			
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;

			this.showAssignProductTable = true;

			this._cdr.detectChanges();
		});
	}

	// spaceReg = new RegExp(" ", "g");
	// // searchProduct(filterValue: string) {
	// // 	this.allProducts = this.allProductsForSearching.filter((a) =>
	// // 		a.name
	// // 			.replace(this.spaceReg, "")
	// // 			.toLowerCase()
	// // 			.trim()
	// // 			.startsWith(filterValue.toLowerCase())
	// // 	);
	// // }
	// searchUser(filterValue: string) {
	// 	this.allUsers = this.allUsersForSearching.filter((a) =>
	// 		a.name
	// 			.replace(this.spaceReg, "")
	// 			.toLowerCase()
	// 			.trim()
	// 			.startsWith(filterValue.toLowerCase())
	// 	);
	// }

	addProduct() {
		const formValue = this.inventoryForm.value;
		const obj = {
			"name": formValue.sku,
			"type": formValue.productType,
			"content": formValue.content,
			"ageGroup": formValue.ageGroup,
			"previousBalance": 0,
			"status": true,
			"updatedBy": this.user.id
		}
		this._productMasterService.addProduct(obj).subscribe(res => {
			if (res) {
				const Toast = Swal.mixin({
					toast: true,
					position: "center",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "success",
					title: `New Product Added Successfully !!`,
				});
				// this.inventoryForm.reset();
				this.clearProductArray();
				this.getAllProducts();
				this._cdr.detectChanges();

			}
			else {
				const Toast = Swal.mixin({
					toast: true,
					position: "center",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "error",
					title: `Oops!!, Something Went Wrong, Please Try Again.`,
				});
				this._cdr.detectChanges();

			}
		});
	}

	async submitStock() {
		const productArray = this.inventoryForm.value.productArray.filter(res => Boolean(res.stockIn));
		const total = productArray.reduce((global, current) => global + current.stockIn, 0);

		if (total > 0) {
			let obj = [];
			let productIds = [];
			for (let i = 0; i < productArray.length; i++) {
				const element = productArray[i]
				// console.log(i, " -- ", element.previousBalance, " -- ", element.stockIn);
				obj.push({ productId: element.productId, lastStock: element.previousBalance, currentStock: element.stockIn });
				const updatedProductMaster = await this.updateProductMaster({ id: element.productId }, { previousBalance: parseInt(element.previousBalance) + parseInt(element.stockIn) })
			}
			await this.addStock(obj);
			this.clearProductArray();
			this.getAllProducts();
			this._cdr.detectChanges();
			this.showAlert("success", "Stock Added Successfully!!")

		} else {
			this.showAlert("error", "Please Enter Stock Atleast For 1 Product !!")
		}

	}
	addStock(data): Promise<any> {
		return new Promise((resolve, reject) => {
			this._productStockDetailsService.addStock(data).subscribe(res => {
				resolve(res)
			}, err => {
				reject(err)
			});
		});
	}
	updateProductMaster(where, data): Promise<any> {
		return new Promise((resolve, reject) => {
			this._productMasterService.updateProduct(where, data).subscribe(res => {
				resolve(res)
			}, err => {
				reject(err)
			});
		});
	}


	changeProductStatus(i, row) {
		Swal.fire({
			title: "Are you sure want to change the status.",
			icon: "info",
			allowOutsideClick: false,
			allowEscapeKey: false,
			showCancelButton: true,
			confirmButtonColor: "#16a184",
			cancelButtonColor: "#f4516c",
			confirmButtonText: "Yes",
		}).then((alertRes) => {
			if (alertRes.value) {
				((this.inventoryForm.get('productArray') as FormArray).at(i) as FormGroup).get('stockIn').setValue(0);
				this.inventoryForm.updateValueAndValidity()
				this._productMasterService.updateProduct({ id: row.id }, { status: !row.status }).subscribe(res => {
					if (res) {
						this.getAllProducts();
						this._cdr.detectChanges();
						this.showAlert("success", "Product Status Updated Successfull!!");

					}
				}, err => {
					this.getAllProducts();
					this._cdr.detectChanges();
					this.showAlert("error", "Something Went Wrong, Please Try Again.");
				});
			} else if (alertRes.dismiss) {
				this.getAllProducts()
			}
			this._cdr.detectChanges();

		});
	}
	getActualIndex(index: number) {
		return index + this.paginator.pageSize * this.paginator.pageIndex;
	}
	showAlert(icon, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: "center",
			showConfirmButton: false,
			timer: 4000,
			timerProgressBar: true,
		});
		Toast.fire({
			icon: icon,
			title: title,
		});
	}
}
