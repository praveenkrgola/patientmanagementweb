import { ESmsType } from './../../../../../models/sms.dto';
import { SmsService } from './../../../../../services/SMS/sms.service';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import {
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import { UserLoginService } from "../../../../../../src/services/user-login/user-login.service";
import {
	bankAccountValidator,
	passwordValidator,
	uniqueEmailValidator,
	uniqueMobileValidator,
	uniqueUserNameValidator,
} from "../../../../../../src/services/util/validation/custom-validation";
import { ListMasterService } from "../../../../../../src/services/list-master/list-master.service";
import Swal from "sweetalert2";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import * as XLSX from "xlsx";
import { HttpEventType } from "@angular/common/http";
import { ContainerService } from "../../../../../../src/services/container/container.service";
import { DatePipe } from '@angular/common';
import { BlockService } from "../../../../../../src/services/block/block.service";

@Component({
	selector: "kt-facility-creation",
	templateUrl: "./facility-creation.component.html",
	styleUrls: ["./facility-creation.component.scss"],
})
export class FacilityCreationComponent implements OnInit {
	facilityCreationForm: FormGroup;
	isNikshayRegProvider: boolean = false;
	incentiveReferredToCompounder: boolean = false;
	nikshayTU = [];
	nikshayTUForSearching = [];
	// blocks = [];
	// blocksForSearching = [];
	facilities = [];
	facilitiesForSearching = [];
	qualifications = [];
	qualificationsForSearching = [];
	// Mat-TABLE
	@ViewChild("paginator", { static: true }) paginator: MatPaginator;
	@ViewChild("sort", { static: true }) sort: MatSort;
	displayedColumns: string[] = [
		"sn",
		"name",
		"mobile",
		"email",
		"contactPersonName",
		"contactPersonDesignation",
		"address",
		"qualification",
		// "nikshayRegProvider",
		"nikshayTU",
		"NikshayHFID",
		"facilityType",
		"facilityName",
		"govtRegNum",
		"fieldOfficer",
		"incentiveReferredToCompounder",
		"compounderOrAttendantName",
		"compounderOrAttendantMobile",
		"compounderOrAttendantBankAccountNum",
		"compounderOrAttendantBankIFSC",
		"compounderOrAttendantPANCard",
		"PANCard",
		"beneficiaryPhoto",
		"bankAccountPhoto",
		"providerPANCardNumber",
		"providerBankAccountNumber",
		"providerBankIFSCCode",
		"username",
		"password",
		"status",
		"createdAt",
		"action"
	];
	dataSource: MatTableDataSource<any>;
	fieldOfficerList = [];
	selectedPANCardFile: File;
	selectedBeneficiaryPhotoFile: File;
	providerPANCardUrl;
	providerBeneficiaryPhotoUrl;
	fileProgress: number;
	selectedProviderPANCardFileId: any;
	selectedProviderBeneficiaryPhotoFileId: any;
	editMode: boolean = false;
	selectedEditUser: any;
	selectedProviderBankAccountPhotoFile: File;
	providerBankAccountPhotoUrl;
	smsType = ESmsType;
	selectedProviderBankAccountPhotoFileId: any;
	selectedProviderBankAccountPhotoFileModifiedFileName: any;
	selectedProviderPANCardFileModifiedFileName: any;
	selectedProviderBeneficiaryPhotoFileModifiedFileName: any;
	showProcessing: boolean = false;;
	constructor(
		private _listMasterService: ListMasterService,
		private _userLoginService: UserLoginService,
		private _containerService: ContainerService,
		private _blockService : BlockService,
		public fb: FormBuilder,
		private _changeDetectorRef: ChangeDetectorRef,
		private smsService: SmsService,
	) {
		this.createForm(fb);
	}
	createForm(fb: FormBuilder) {
		this.facilityCreationForm = fb.group({
			nikshayRegProvider: new FormControl(null, Validators.required),
			nikshayTU: new FormControl(null, Validators.required),
			facilityType: new FormControl(null, Validators.required),
			facilityName: new FormControl(null, Validators.required),
			govtRegNum: new FormControl(null),
			fieldOfficerId: new FormControl(null, Validators.required),

			// Incentive Referred To Compounder
			isIncentiveReferredToCompounder: new FormControl(null, Validators.required),

			// Personal Information
			name: new FormControl(null, Validators.required),
			mobile: new FormControl(
				null,
				Validators.compose([
					Validators.required,
					Validators.minLength(10),
					Validators.maxLength(10),
				]),
				uniqueMobileValidator(this._userLoginService)
			),
			email: new FormControl(
				null,
				// Validators.compose([
				// 	Validators.pattern(
				// 		"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"
				// 	),
				// ])
			),

			address: new FormControl(
				null,
				Validators.compose([Validators.required])
			),
			qualification: new FormControl(null),

			providerPANCardNumber : new FormControl(null),
			providerBankAccountNumber : new FormControl(null),
			providerConfirmBankAccountNumber: new FormControl(null,
			Validators.compose([bankAccountValidator])),
			providerBankIFSCCode : new FormControl(null),
			// Uploads
			providerBankAccountPhoto : new FormControl(null),
			providerPANCard: new FormControl(null),
			providerBeneficiaryPhoto: new FormControl(null),
			// Login Credentials
			username: new FormControl(
				null,
				Validators.compose([Validators.required]),
				uniqueUserNameValidator(this._userLoginService)
			),
			password: new FormControl(
				null,
				Validators.compose([Validators.required])
			),
			confirmPassword: new FormControl(
				null,
				Validators.compose([Validators.required, passwordValidator])
			),
		});
	}
	// get f() {
	// 	return this.facilityCreationForm.controls;
	// }

	get username() {
		return this.facilityCreationForm.get("username") as FormControl;
	}
	get mobile() {
		return this.facilityCreationForm.get("mobile") as FormControl;
	}
	get email() {
		return this.facilityCreationForm.get("email") as FormControl;
	}
	get password() {
		return this.facilityCreationForm.get("password") as FormControl;
	}
	get confirmPassword() {
		return this.facilityCreationForm.get("confirmPassword") as FormControl;
	}
	get providerBankAccountNumber() {
		return this.facilityCreationForm.get("providerBankAccountNumber") as FormControl;
	}
	get providerConfirmBankAccountNumber() {
		return this.facilityCreationForm.get("providerConfirmBankAccountNumber") as FormControl;
	}
	ngOnInit() {
		this.isRegisteredProvider(false);
		this.isIncentiveReferredToCompounder("No");
		this.getAllLists();
		this.getAllUsers();
		this.getBlocks();

	}
	getBlocks() {
		this._blockService.getBlocks({}).subscribe((res) => {
			this.nikshayTU = res;
			console.log(this.nikshayTU);

			this.nikshayTUForSearching = res;
		})
	}
	isRegisteredProvider(value) {
		if (value === true) {
			this.facilityCreationForm.patchValue({ nikshayRegProvider: true });
			this.isNikshayRegProvider = true;
			this.facilityCreationForm.addControl(
				"nikshayHFID",
				new FormControl(null)
			);
			this.facilityCreationForm.get('nikshayHFID').valueChanges.subscribe((res) => {
				if (this.facilityCreationForm.controls['nikshayHFID'].value) {
				   this.facilityCreationForm.patchValue({username:this.facilityCreationForm.controls['nikshayHFID'].value})
				}
			  });
			} else {
				this.facilityCreationForm.patchValue({ nikshayRegProvider: false });
				this.isNikshayRegProvider = false;
				this.facilityCreationForm.removeControl("nikshayHFID");
			}
		}
	isIncentiveReferredToCompounder(value) {
			if (value === "Yes") {
				this.facilityCreationForm.patchValue({ isIncentiveReferredToCompounder: "Yes" });
				this.incentiveReferredToCompounder = true;
				this.facilityCreationForm.addControl("compounderOrAttendantName", new FormControl(null, Validators.required));
				this.facilityCreationForm.addControl("compounderOrAttendantMobile", new FormControl(null, Validators.required));
				this.facilityCreationForm.addControl("compounderOrAttendantBankAccountNum", new FormControl(null, Validators.required));
				this.facilityCreationForm.addControl("compounderOrAttendantBankIFSC", new FormControl(null, Validators.required));
				this.facilityCreationForm.addControl("compounderOrAttendantPANCard", new FormControl(null, Validators.required));
			} else {
				this.facilityCreationForm.patchValue({ isIncentiveReferredToCompounder: "No" });
				this.incentiveReferredToCompounder = false;
				this.facilityCreationForm.removeControl("compounderOrAttendantName");
				this.facilityCreationForm.removeControl("compounderOrAttendantMobile");
				this.facilityCreationForm.removeControl("compounderOrAttendantBankAccountNum");
				this.facilityCreationForm.removeControl("compounderOrAttendantBankIFSC");
				this.facilityCreationForm.removeControl("compounderOrAttendantPANCard");
		}
	}
	getAllLists() {
		const filter = {
			type: ["stakeholder", "qualification", "nikshayTU"],
		};
		this._listMasterService.getList(filter).subscribe((res) => {
			res.forEach((element) => {
				// if (element.type == "nikshayTU") {
				// 	this.nikshayTU = element.data;
				// 	this.nikshayTUForSearching = element.data;
				// } else
					if (element.type == "qualification") {
					this.qualifications = element.data;
					this.qualificationsForSearching = element.data;
				} else if (element.type == "stakeholder") {
					// this.facilities = element.data;
					this.facilities = element.data.filter(i=>i.canBeCreated === true);
					this.facilitiesForSearching = this.facilities;
				}
			});
		});
	}
	getAllUsers() {
		this.showProcessing = true;
		const filter = {
			where : { },
			include: [
				{ relation: "fieldOfficer" },
				{ relation: "beneficiaryPhoto" },
				{ relation: "panCard" },
				{ relation: "bankAccountPhoto"}
			]
		};
		this._userLoginService.getList(filter).subscribe((res: any) => {
			if (res.length > 0) {
				this.showProcessing = false;
				const newArr = res.map((item, i) => {
					const item2 = {};
					item2["id"] = item.id;
					item2["name"] = item.name;
					item2["contactPersonName"] = item.contactPersonName;
					item2["designation"] = item.designation;
					item2["designationName"] = item.designationName;
					item2["mobile"] = item.mobile;
					item2["username"] = item.username;
					item2["password"] = item.viewPassword;
					item2["address"] = item.address;
					item2["status"] = item.status === true ? "Active" : "Inactive";
					item2["incentiveReferredToCompounder"] = item.incentiveReferredToCompounder || '--';
					item2["compounderOrAttendantName"] = item.compounderOrAttendantName || '--';
					item2["compounderOrAttendantBankAccountNum"] = item.compounderOrAttendantBankAccountNum || '--';
					item2["compounderOrAttendantBankIFSC"] = item.compounderOrAttendantBankIFSC || '--';
					item2["compounderOrAttendantMobile"] = item.compounderOrAttendantMobile || '--';
					item2["compounderOrAttendantPANCard"] = item.compounderOrAttendantPANCard || '--';
					item2["fieldOfficer"] = item.fieldOfficer ? item.fieldOfficer.name : "---";
					item2["fieldOfficerId"] = item.fieldOfficer ? item.fieldOfficer.id : "---";
					item2["blockId"] = item.blockId ? item.blockId : "---";
					item2["panCard"] = item.panCard;
					item2["providerPANCard"] = item.providerPANCard ? item.providerPANCard: "---";
					item2["beneficiaryPhoto"] = item.beneficiaryPhoto ;
					item2["providerBeneficiaryPhoto"] = item.providerBeneficiaryPhoto ? item.providerBeneficiaryPhoto : "---" ;
					item2["bankAccountPhoto"] = item.bankAccountPhoto ,
					item2["providerBankAccountPhoto"] = item.providerBankAccountPhoto ? item.providerBankAccountPhoto : "---";
					item2["providerBankAccountNumber"] = item.providerBankAccountNumber ? item.providerBankAccountNumber : "---";
					item2["providerPANCardNumber"] = item.providerPANCardNumber ? item.providerPANCardNumber : "---";
					item2["providerBankIFSCCode"] = item.providerBankIFSCCode ? item.providerBankIFSCCode : "---";
					item2["createdAt"] = item.createdAt,
					item.profile.map((i) => {
						if (i.key == "NikshayTU") {
							item2["nikshayTU"] = i.value;
						}
						if (i.key == "NikshayHFID") {
							item2["NikshayHFID"] = i.value;
						}
						if (i.key == "facilityType") {
							item2["facilityType"] = i.value;
						}
						if (i.key == "contactPersonName") {
							item2["contactPersonName"] = i.value;
						}
						if (i.key == "facilityName") {
							item2["facilityName"] = i.value;
						}
						if (i.key == "govtRegistrationNumber") {
							item2["govtRegistrationNumber"] = i.value;
						}

						if (i.key == "qualifications") {
							item2["qualification"] = i.value;
						}
						if (i.key == "email") {
							item2["email"] = i.value;
						}
					});
					return item2;
				});
				this.dataSource = new MatTableDataSource(newArr);
				setTimeout(() => {
					this.dataSource.paginator = this.paginator;
					this.dataSource.sort = this.sort;
				}, 200);

				this.fieldOfficerList = newArr.filter(arr => arr.designation === "fieldofficer");

			}
		});
		this._changeDetectorRef.detectChanges();
	}

	// ============== Upload PAN Card =====
	getProviderBankAccountPhotoFile(e) {
		if (e.target.files && e.target.files[0]) {
			this.selectedProviderBankAccountPhotoFile = <File>e.target.files[0];
			let fReader = new FileReader();
			if (e.target.files[0].size > 1048576) {
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "error",
					title: `Max Size Limit Exceeds !!`,
					text: `This file size is: ${(
						e.target.files[0].size /
						1024 /
						1024
					).toFixed(2)} MB`,
				});
				this.facilityCreationForm.patchValue({ providerBankAccountPhoto: "" });
				this.providerBankAccountPhotoUrl = "";
			} else {
				fReader.readAsDataURL(this.selectedProviderBankAccountPhotoFile);
				fReader.onload = (event: any) => {
					this.facilityCreationForm.patchValue({
						providerBankAccountPhoto: this.selectedProviderBankAccountPhotoFile.name,
					});
					this.providerBankAccountPhotoUrl = event.target.result;
				};
				this._containerService
					.uploadFile("BankAccountPhoto", "providerBankAccountPhoto", this.selectedProviderBankAccountPhotoFile)
					.subscribe((events) => {
						if (events.type === HttpEventType.UploadProgress) {
							this.fileProgress = Math.round(
								(events.loaded / events.total) * 100
							);
							!this._changeDetectorRef["destroyed"]
								? this._changeDetectorRef.detectChanges()
								: null;
						} else if (events.type === HttpEventType.Response) {
							this.selectedProviderBankAccountPhotoFileId = events.body["fileId"];
							this.selectedProviderBankAccountPhotoFileModifiedFileName = events.body["modifiedFileName"];

							const Toast = Swal.mixin({
								toast: true,
								position: "top-end",
								showConfirmButton: false,
								timer: 4000,
								timerProgressBar: true,
							});
							Toast.fire({
								icon: "success",
								title: `Bank account photo uploaded successfully !!`
							});
						}
					});
			}
		}
	}
	async removeUploadedBankAccountPhoto() {
		const alertMessage = await this.alert();
		if (alertMessage) {
			this.facilityCreationForm.patchValue({ providerBankAccountPhoto: "" });
			this.providerBankAccountPhotoUrl = "";
		}
	}
	// ============== Upload PAN Card =====
	getPANCardFile(e) {
		if (e.target.files && e.target.files[0]) {
			this.selectedPANCardFile = <File>e.target.files[0];
			let fReader = new FileReader();
			if (e.target.files[0].size > 1048576) {
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "error",
					title: `Max Size Limit Exceeds !!`,
					text: `This file size is: ${(
						e.target.files[0].size /
						1024 /
						1024
					).toFixed(2)} MB`,
				});
				this.facilityCreationForm.patchValue({ providerPANCard: "" });
				this.providerPANCardUrl = "";
			} else {
				fReader.readAsDataURL(this.selectedPANCardFile);
				fReader.onload = (event: any) => {
					this.facilityCreationForm.patchValue({
						providerPANCard: this.selectedPANCardFile.name,
					});
					this.providerPANCardUrl = event.target.result;
				};
				this._containerService
					.uploadFile("PANCard", "PANCardUpload", this.selectedPANCardFile)
					.subscribe((events) => {
						if (events.type === HttpEventType.UploadProgress) {
							this.fileProgress = Math.round(
								(events.loaded / events.total) * 100
							);
							!this._changeDetectorRef["destroyed"]
								? this._changeDetectorRef.detectChanges()
								: null;
						} else if (events.type === HttpEventType.Response) {
							this.selectedProviderPANCardFileId = events.body["fileId"];
							this.selectedProviderPANCardFileModifiedFileName = events.body["modifiedFileName"];
							const Toast = Swal.mixin({
								toast: true,
								position: "top-end",
								showConfirmButton: false,
								timer: 4000,
								timerProgressBar: true,
							});
							Toast.fire({
								icon: "success",
								title: `PAN card uploaded successfully !!`
							});
						}
					});
			}
		}
	}
	async removeUploadedPANCard() {
		const alertMessage = await this.alert();
		if (alertMessage) {
			this.facilityCreationForm.patchValue({ providerPANCard: "" });
			this.providerPANCardUrl = "";
		}
	}

	// ============== Upload Beneficiary Photo =====
	getBeneficiaryPhotoFile(e) {
			if (e.target.files && e.target.files[0]) {
				this.selectedBeneficiaryPhotoFile = <File>e.target.files[0];
				let fReader = new FileReader();
				if (e.target.files[0].size > 1048576) {
					const Toast = Swal.mixin({
						toast: true,
						position: "top-end",
						showConfirmButton: false,
						timer: 4000,
						timerProgressBar: true,
					});
					Toast.fire({
						icon: "error",
						title: `Max Size Limit Exceeds !!`,
						text: `This file size is: ${(
							e.target.files[0].size /
							1024 /
							1024
						).toFixed(2)} MB`,
					});
					this.facilityCreationForm.patchValue({ providerBeneficiaryPhoto: "" });
					this.providerBeneficiaryPhotoUrl = "";
				} else {
					fReader.readAsDataURL(this.selectedBeneficiaryPhotoFile);
					fReader.onload = (event: any) => {
						this.facilityCreationForm.patchValue({
							providerBeneficiaryPhoto: this.selectedBeneficiaryPhotoFile.name,
						});
						this.providerBeneficiaryPhotoUrl = event.target.result;
					};
					this._containerService
						.uploadFile("beneficiaryPhoto", "beneficiaryPhotoUpload", this.selectedBeneficiaryPhotoFile)
						.subscribe((events) => {
							if (events.type === HttpEventType.UploadProgress) {
								this.fileProgress = Math.round(
									(events.loaded / events.total) * 100
								);
								!this._changeDetectorRef["destroyed"]
									? this._changeDetectorRef.detectChanges()
									: null;
							} else
							if (events.type === HttpEventType.Response) {
								this.selectedProviderBeneficiaryPhotoFileId = events.body["fileId"];
								this.selectedProviderBeneficiaryPhotoFileModifiedFileName = events.body["modifiedFileName"];

											const Toast = Swal.mixin({
												toast: true,
												position: "top-end",
												showConfirmButton: false,
												timer: 4000,
												timerProgressBar: true,
											});
											Toast.fire({
												icon: "success",
												title: `Beneficiary Photo uploaded successfully !!`
											});
							}
						});
				}
			}
	}
	async removeUploadedBeneficiaryPhoto() {
		const alertMessage = await this.alert();
		if (alertMessage) {
			this.facilityCreationForm.patchValue({ providerBeneficiaryPhoto: "" });
			this.providerBeneficiaryPhotoUrl = "";
		}
	}

	spaceReg = new RegExp(" ", "g");
	searchNikshayTU(filterValue: string) {
		this.nikshayTU = this.nikshayTUForSearching.filter((a) =>
			a.name
				.replace(this.spaceReg, "")
				.toLowerCase()
				.trim()
				.startsWith(filterValue.toLowerCase())
		);
	}
	searchFacilityType(filterValue: string) {
		this.facilities = this.facilitiesForSearching.filter((a) =>
			a.key
				.replace(this.spaceReg, "")
				.toLowerCase()
				.trim()
				.startsWith(filterValue.toLowerCase())
		);
	}
	searchQualificationType(filterValue: string) {
		this.qualifications = this.qualificationsForSearching.filter((a) =>
			a.key
				.replace(this.spaceReg, "")
				.toLowerCase()
				.trim()
				.startsWith(filterValue.toLowerCase())
		);
	}

	createFacility() {
		// let params = Object.assign({}, this.facilityCreationForm.value);
		const formName = this.facilityCreationForm.value;
		let params2 = {
			stateId: "57a3e5a04b80a50555312b28", //Static State ID
			districtId: "585e3ae050ce3f10dbdaa124", //Static District ID
			name: formName.facilityName,
			mobile: formName.mobile,
			designation: formName.facilityType.value,
			designationName: formName.facilityType.key,
			username: (formName.username).toString(),
			password: formName.password,
			viewPassword: formName.password,
			fieldOfficerId: formName.fieldOfficerId,
			blockId: formName.nikshayTU.id,
			status: true,
			address: formName.address,
			incentiveReferredToCompounder: formName.isIncentiveReferredToCompounder,
			providerPANCard: this.selectedProviderPANCardFileId ? this.selectedProviderPANCardFileId : "---",
			providerBeneficiaryPhoto: this.selectedProviderBeneficiaryPhotoFileId ? this.selectedProviderBeneficiaryPhotoFileId : "---",
			profile: [
				{
					key: "NikshayTU",
					value: formName.nikshayTU.name,
					label: "Nikshay TU",
				},
				{
					key: "facilityType",
					value: formName.facilityType.value,
					label: "Facility Type",
				},
				{
					key: "facilityName",
					value: formName.facilityName,
					label: "Facility Name",
				},
				{
					key: "govtRegistrationNumber",
					value: formName.govtRegNum,
					label: "Govt Registration Number",
				},
				{
					key: "contactPersonName",
					value: formName.name,
					label: "Name",
				},
				{
					key: "mobileNumber",
					value: formName.mobile,
					label: "Mobile Number",
				},
				{
					key: "email",
					value: formName.email,
					label: "Email",
				},
				{
					key: "address",
					value: formName.address,
					label: "Address",
				},
				{
					key: "qualifications",
					value: formName.qualification,
					label: "Qualifications",
				},
			],
			"contactPersonName" : formName.name,
			"facilityName" : formName.facilityName,
			"govtRegistrationNumber": formName.govtRegNum,
			providerPANCardNumber: formName.providerPANCardNumber,
			providerBankAccountNumber : formName.providerBankAccountNumber,
			providerBankIFSCCode: formName.providerBankIFSCCode,
			providerBankAccountPhoto : this.selectedProviderBankAccountPhotoFileId ? this.selectedProviderBankAccountPhotoFileId : "---",
			//createdAt: new Date().getTime()
		};

		// Static iconId based on the facilityType
		if (formName.facilityType.value == "chemist") {
			params2["iconId"] = "60028319773f321f48fe3f18";
		}

		if (formName.nikshayRegProvider == true) {
			params2["NikshayHFID"] = formName.nikshayHFID;
			params2.profile.push({
				key: "NikshayHFID",
				value: formName.nikshayHFID,
				label: "Nikshay HFID",
			});
		} else {
			params2.profile.push({
				key: "NikshayHFID",
				value: "---",
				label: "Nikshay HFID",
			});
		}
		if (formName.isIncentiveReferredToCompounder === "Yes") {
			params2["compounderOrAttendantName"] = formName.compounderOrAttendantName;
			params2["compounderOrAttendantMobile"] = formName.compounderOrAttendantMobile;
			params2["compounderOrAttendantBankAccountNum"] = formName.compounderOrAttendantBankAccountNum;
			params2["compounderOrAttendantBankIFSC"] = formName.compounderOrAttendantBankIFSC;
			params2["compounderOrAttendantPANCard"] = formName.compounderOrAttendantPANCard;
		} else {
			params2["compounderOrAttendantName"] = "---";
			params2["compounderOrAttendantMobile"] = "---";
			params2["compounderOrAttendantBankAccountNum"] = "---";
			params2["compounderOrAttendantBankIFSC"] = "---";
			params2["compounderOrAttendantPANCard"] = "---";

		}
		this._userLoginService.add(params2).subscribe(async (res) => {
			if (res) {
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "success",
					title: `User Registered Successfully !!`,
				});

				const where = {
					where: {
						type: this.smsType.userRegister
					}
				};

				const getSmsData = await this.smsService.getSMSData(where).toPromise();
				let newDataArr = [];
				for (let i = 0; i < getSmsData.length; i++) {
					let message = getSmsData[i].msg.replace("<LOGIN ID>", res.username);
					message = message.replace("<PASS>", res.viewPassword);
					let newUrl = getSmsData[i].url;
					newUrl = getSmsData[i].url.replace("<msg>", message);
					newUrl = newUrl.replace("<mob>", res.mobile)
					newDataArr.push(newUrl);
				}
				this.smsService.userRegistrationSms(newDataArr).subscribe(data => {
					console.log(data);
				}, err => {
					console.log(err);
				});

				this.facilityCreationForm.reset();
				this.providerBeneficiaryPhotoUrl = "";
				this.selectedProviderBeneficiaryPhotoFileId = "";
				this.providerBankAccountPhotoUrl = "";
				this.selectedProviderBankAccountPhotoFileId = "";
				this.providerPANCardUrl = "";
				this.selectedProviderPANCardFileId = "";
				this.getAllUsers();
				this.isRegisteredProvider(false);
				this.isIncentiveReferredToCompounder("No");
				this.createForm(this.fb);
			}
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	exportTable() {
		let dataToExport = this.dataSource.data.map((x) => ({
			Name: x.name,
			Mobile: x.mobile,
			Email: x.email,
			"Provider Designation": x.designation,
			Address: x.address,
			Qualification: x.qualification,
			// "Provider Registered in Nikshay": x.nikshayRegProvider == true ? "Yes":"No",
			"Nikshay HFID": x.NikshayHFID,
			"Nikshay TU": x.nikshayTU,
			"Facility Type": x.facilityType,
			"Facility Name": x.facilityName,
			"Govt. Registration Num": x.govtRegNum,
			"Field Officer" : x.fieldOfficer,
			"Incentive Referred To Compounder" : x.incentiveReferredToCompounder,
			"Compounder Name" : x.compounderOrAttendantName,
			"Compounder Bank Account Number" : x.compounderOrAttendantBankAccountNum,
			"Compounder Bank IFSC" : x.compounderOrAttendantBankIFSC,
			"Compounder Mobile" : x.compounderOrAttendantMobile,
			"Compounder PAN Card": x.compounderOrAttendantPANCard,
			"Provider Bank Account Number" : x.providerBankAccountNumber,
			"Provider PAN Card Number" : x.providerPANCardNumber,
			"Provider Bank IFSC Code" : x.providerBankIFSCCode,
			Username: x.username,
			Password: x.password,
			"User Registration Date" : new DatePipe('en-US').transform(new Date(x.createdAt), 'dd-MM-yyyy hh:mm:ss a'),
			Status: x.status
			
		}));
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Registered Users.xlsx");
	}
	changeProviderStatus(row) {
		(Swal as any)
		.fire({
			title: "Are you sure want to change the status.",
			icon: "info",
			allowOutsideClick: false,
			allowEscapeKey: false,
			showCancelButton: true,
			confirmButtonColor: "#16a184",
			cancelButtonColor: "#f4516c",
			confirmButtonText: "Yes",
		})
		.then((alertRes) => {
			if (alertRes.value) {
				const where = {
					_id: row.id
				}
				let changeStatus:boolean;
				let changeStatusLabel;
				if (row.status == "Active") {
					changeStatus = false;
					changeStatusLabel = "Inactive"
				} else {
					changeStatus = true;
					changeStatusLabel = "Active"
				}
				const data = {
					status: changeStatus,
				}
				this._userLoginService.updateUser(where, data).subscribe(res => {
					this.getAllUsers();
					const Toast = Swal.mixin({
						toast: true,
						position: "top-end",
						showConfirmButton: false,
						timer: 4000,
						timerProgressBar: true,
					});
					Toast.fire({
						icon: "success",
						title: `Status of ${row.name} has been changed to ${changeStatusLabel} !!`
					});
					this._changeDetectorRef.detectChanges();
				})
			} else {
				this.getAllUsers();
			}
			this._changeDetectorRef.detectChanges();
		});

	}
	editProvider(row) {
		(Swal as any)
		.fire({
			title: "Are you sure want to edit the provider.",
			icon: "info",
			allowOutsideClick: false,
			allowEscapeKey: false,
			showCancelButton: true,
			confirmButtonColor: "#16a184",
			cancelButtonColor: "#f4516c",
			confirmButtonText: "Yes",
		})
			.then((alertRes) => {

				if (alertRes.value) {
					this.editMode = true;
					this.scroll("#user-registration-form");
					this.selectedEditUser = row;
				if (row.NikshayHFID) {
					this.isRegisteredProvider(true);
					this.facilityCreationForm.patchValue({
						nikshayHFID : row.NikshayHFID
					})
					}
					console.log(this.nikshayTU,'------> ',row);

				const nikshayIndex = this.nikshayTU.findIndex(i => i.id == row.blockId);
				if (nikshayIndex !== -1) {
					this.facilityCreationForm.patchValue({
						nikshayTU: this.nikshayTU[nikshayIndex]
					})
				}
				const facilityIndex = this.facilities.findIndex(i => i.value === row.designation);
				if (facilityIndex !== -1) {
					this.facilityCreationForm.patchValue({
						facilityType: this.facilities[facilityIndex]
					})
				}
				const fieldOfficerIndex = this.fieldOfficerList.findIndex(i => i.id === row.fieldOfficerId);
				if (fieldOfficerIndex !== -1) {
					this.facilityCreationForm.patchValue({
						fieldOfficerId: this.fieldOfficerList[fieldOfficerIndex].id
					})
					}
					if (row.incentiveReferredToCompounder === "Yes") {
						this.isIncentiveReferredToCompounder("Yes");
						this.facilityCreationForm.patchValue({
							compounderOrAttendantName : row.compounderOrAttendantName,
							compounderOrAttendantMobile : row.compounderOrAttendantMobile,
							compounderOrAttendantBankAccountNum : row.compounderOrAttendantBankAccountNum,
							compounderOrAttendantBankIFSC : row.compounderOrAttendantBankIFSC,
							compounderOrAttendantPANCard : row.compounderOrAttendantPANCard,
						})
					}
				this.facilityCreationForm.patchValue({
					facilityName: row.facilityName,
					govtRegNum: row.govtRegistrationNumber,
					name : row.contactPersonName,
					email : row.email,
					mobile: row.mobile,
					address: row.address,
					providerPANCardNumber: row.providerPANCardNumber,
					providerBankAccountNumber: row.providerBankAccountNumber,
					providerConfirmBankAccountNumber: row.providerBankAccountNumber,
					providerBankIFSCCode: row.providerBankIFSCCode,
				})

					if (row.panCard) {
						this.selectedProviderPANCardFileModifiedFileName = row.panCard.modifiedFileName;
						this._containerService
							.downloadFile(row.panCard.container, row.panCard.modifiedFileName)
							.subscribe((logoImgRes) => {
								this.createImageFromBlob(logoImgRes, "PANCard");
								this.facilityCreationForm.patchValue({
									providerPANCard : row.panCard.originalFileName
								})
								this.selectedProviderPANCardFileId = row.providerPANCard;
							});
					}
					if (row.beneficiaryPhoto) {
						this.selectedProviderBeneficiaryPhotoFileModifiedFileName = row.beneficiaryPhoto.modifiedFileName;
						this._containerService
							.downloadFile(row.beneficiaryPhoto.container, row.beneficiaryPhoto.modifiedFileName)
							.subscribe((logoImgRes) => {
								this.createImageFromBlob(logoImgRes, "beneficiaryPhoto");
								this.facilityCreationForm.patchValue({
									providerBeneficiaryPhoto : row.beneficiaryPhoto.originalFileName
								})
								this.selectedProviderBeneficiaryPhotoFileId =  row.providerBeneficiaryPhoto
							});
					}
					if (row.bankAccountPhoto) {
						this.selectedProviderBankAccountPhotoFileModifiedFileName = row.bankAccountPhoto.modifiedFileName;
						this._containerService
							.downloadFile(row.bankAccountPhoto.container, row.bankAccountPhoto.modifiedFileName)
							.subscribe((logoImgRes) => {
								this.createImageFromBlob(logoImgRes, "bankAccountPhoto");
								this.facilityCreationForm.patchValue({
									providerBankAccountPhoto : row.bankAccountPhoto.originalFileName
								})
								this.selectedProviderBankAccountPhotoFileId =  row.providerBankAccountPhoto
							});
					}



			}


		});
	}

	createImageFromBlob(image: Blob, blobFor) {
		let reader = new FileReader();
		reader.addEventListener(
			"load",
			() => {
				if (blobFor == "PANCard") {
					this.providerPANCardUrl = reader.result;
				}
				else if (blobFor == "beneficiaryPhoto") {
					this.providerBeneficiaryPhotoUrl = reader.result;
				}
				else if (blobFor == "bankAccountPhoto") {
					this.providerBankAccountPhotoUrl = reader.result;
				}
				!this._changeDetectorRef["destroyed"] ? this._changeDetectorRef.detectChanges() : null;
			},
			false
			);
			if (image) {
				reader.readAsDataURL(image);
			}
			!this._changeDetectorRef["destroyed"] ? this._changeDetectorRef.detectChanges() : null;
	}
	updateProvider() {
		// let params = Object.assign({}, this.facilityCreationForm.value);
		const formName = this.facilityCreationForm.value;
		console.log(formName);

		let params2 = {
			stateId: "57a3e5a04b80a50555312b28", //Static State ID
			districtId: "585e3ae050ce3f10dbdaa124", //Static District ID
			name: formName.facilityName,
			mobile: formName.mobile,
			designation: formName.facilityType.value,
			designationName: formName.facilityType.key,
			fieldOfficerId : formName.fieldOfficerId,
			blockId : formName.nikshayTU.id,
			status: true,
			address: formName.address,
			incentiveReferredToCompounder: formName.isIncentiveReferredToCompounder,
			providerPANCard: this.selectedProviderPANCardFileId ? this.selectedProviderPANCardFileId : "---",
			providerBeneficiaryPhoto: this.selectedProviderBeneficiaryPhotoFileId ? this.selectedProviderBeneficiaryPhotoFileId : "---",
			profile: [
				{
					key: "NikshayTU",
					value: formName.nikshayTU.name,
					label: "Nikshay TU",
				},
				{
					key: "facilityType",
					value: formName.facilityType.value,
					label: "Facility Type",
				},
				{
					key: "facilityName",
					value: formName.facilityName,
					label: "Facility Name",
				},
				{
					key: "govtRegistrationNumber",
					value: formName.govtRegNum,
					label: "Govt Registration Number",
				},
				{
					key: "contactPersonName",
					value: formName.name,
					label: "Name",
				},
				{
					key: "mobileNumber",
					value: formName.mobile,
					label: "Mobile Number",
				},
				{
					key: "email",
					value: formName.email,
					label: "Email",
				},
				{
					key: "address",
					value: formName.address,
					label: "Address",
				},
				{
					key: "qualifications",
					value: formName.qualification,
					label: "Qualifications",
				},
			],
			"contactPersonName" : formName.name,
			"facilityName" : formName.facilityName,
			"govtRegistrationNumber": formName.govtRegNum,
			providerBankAccountPhoto: this.selectedProviderBankAccountPhotoFileId ? this.selectedProviderBankAccountPhotoFileId : "---",
			providerPANCardNumber: formName.providerPANCardNumber ? formName.providerPANCardNumber : "---",
			providerBankAccountNumber: formName.providerBankAccountNumber ? formName.providerBankAccountNumber : "---",
			providerBankIFSCCode: formName.providerBankIFSCCode ? formName.providerBankIFSCCode : "---",

		};

		// Static iconId based on the facilityType
		if (formName.facilityType == "chemist") {
			params2["iconId"] = "60028319773f321f48fe3f18";
		} else if (formName.facilityType == "empanelledchemist") {
			params2["iconId"] = "600282fe773f321f48fe3f16";
		} else if (formName.facilityType == "lab") {
			params2["iconId"] = "600281a7773f321f48fe3f12";
		} else if (formName.facilityType == "generaldoctor") {
			params2["iconId"] = "60028341773f321f48fe3f1c";
		} else if (formName.facilityType == "empanelleddoctor") {
			params2["iconId"] = "6002832f773f321f48fe3f1a";
		} else if (formName.facilityType == "ayushprovider") {
			params2["iconId"] = "600282d0773f321f48fe3f14";
		}


		if (formName.nikshayRegProvider == true) {
			params2["NikshayHFID"] = formName.nikshayHFID;
			params2.profile.push({
				key: "NikshayHFID",
				value: formName.nikshayHFID,
				label: "Nikshay HFID",
			});
		} else {
			params2.profile.push({
				key: "NikshayHFID",
				value: "---",
				label: "Nikshay HFID",
			});
		}
		if (formName.isIncentiveReferredToCompounder === "Yes") {
			params2["compounderOrAttendantName"] = formName.compounderOrAttendantName;
			params2["compounderOrAttendantMobile"] = formName.compounderOrAttendantMobile;
			params2["compounderOrAttendantBankAccountNum"] = formName.compounderOrAttendantBankAccountNum;
			params2["compounderOrAttendantBankIFSC"] = formName.compounderOrAttendantBankIFSC;
			params2["compounderOrAttendantPANCard"] = formName.compounderOrAttendantPANCard;
		} else {
			params2["compounderOrAttendantName"] = "---";
			params2["compounderOrAttendantMobile"] = "---";
			params2["compounderOrAttendantBankAccountNum"] = "---";
			params2["compounderOrAttendantBankIFSC"] = "---";
			params2["compounderOrAttendantPANCard"] = "---";

		}
		const where = {
			_id : this.selectedEditUser.id
		}
		this._userLoginService.updateUser(where,params2).subscribe((res) => {
			if (res) {
				const Toast = Swal.mixin({
					toast: true,
					position: "top-end",
					showConfirmButton: false,
					timer: 4000,
					timerProgressBar: true,
				});
				Toast.fire({
					icon: "success",
					title: `User Updated Successfully !!`,
				});
				this.facilityCreationForm.reset();
				this.editMode = false;
				// this.removeUploadedPANCard();
				// this.removeUploadedBeneficiaryPhoto();
				// this.removeUploadedBankAccountPhoto();
				this.providerBeneficiaryPhotoUrl = "";
				this.providerBankAccountPhotoUrl = "";
				this.providerPANCardUrl = "";
				this.getAllUsers();
				this.createForm(this.fb)
			}
			!this._changeDetectorRef["destroyed"]
				? this._changeDetectorRef.detectChanges()
				: null;
		});
	}
	scroll(elem: string) {
        setTimeout(() => {
            // document
            //     .querySelector(elem)
			// 	.scrollIntoView({ behavior: "smooth" , block: "start", inline: "start"});
				scrollTo({
					top: 0,
					behavior: 'smooth',
				  })
		}, 100);
		this._changeDetectorRef.detectChanges();
	}
	downloadFile(container, modifiedFileName) {
        this._containerService
            .downloadFile(container, modifiedFileName)
            .subscribe((res) => {
                var fileURL = window.URL.createObjectURL(res);
                // let tab = window.open();
                // tab.location.href = fileURL;
                window.open(
                    fileURL,
                    "Proof",
                    "toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
                );
            });
    }

	alert(): Promise<any> {
		return new Promise((resolve, reject) => {
			(Swal as any)
			.fire({
				title: "Are you sure want to delete the photo?",
				icon: "info",
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: "#16a184",
				cancelButtonColor: "#f4516c",
				confirmButtonText: "Yes",
			})
			.then((alertRes) => {
				if (alertRes.value) {
					resolve(alertRes.value);
				}
			},(err) => {
				reject(err);
			});
		})
	}

}
