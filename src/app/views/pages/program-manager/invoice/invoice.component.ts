import { SmsService } from './../../../../../services/SMS/sms.service';
import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	Inject,
	OnInit,
	ViewChild,
} from "@angular/core";
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import {
	MatDialog,
	MatDialogRef,
	MatPaginator,
	MatSort,
	MatTableDataSource,
	MAT_DIALOG_DATA,
} from "@angular/material";
import { InvoiceHistoryService } from "../../../../../../src/services/invoice-history/invoice-history.service";
import Swal from "sweetalert2";
import { ReferralMasterService } from "../../../../../../src/services/referral-master/referral-master.service";
import { ContainerService } from "../../../../../../src/services/container/container.service";
import { SelectionModel } from "@angular/cdk/collections";
import { PatientInfoComponent } from "../../shared/patient-info/patient-info.component";
import { InvoiceRejectedVoucherService } from "../../../../../../src/services/invoice-rejected-voucher/invoice-rejected-voucher.service";
import { forkJoin } from "rxjs";
import { DatePipe } from "@angular/common";
import * as XLSX from "xlsx";
import { ListMasterService } from "../../../../../../src/services/list-master/list-master.service";
import { api } from '../../../../../../src/services/util/api';
import { environment } from "../../../../../../src/environments/environment";
import { ESmsType } from '../../../../../models/sms.dto';
@Component({
	selector: "kt-invoice",
	templateUrl: "./invoice.component.html",
	styleUrls: ["./invoice.component.scss"],
})
export class InvoiceComponent implements OnInit {
	currentUser = JSON.parse(sessionStorage.getItem("user"));
	invoiceForm: FormGroup;
	smsType = ESmsType;
	// Mat-TABLE
	@ViewChild("paginator", { static: false }) paginator: MatPaginator;
	@ViewChild("paginatoruserwise", { static: false })
	paginatorUserWise: MatPaginator;
	@ViewChild("rejectedVouchersPaginator", { static: false })
	rejectedVouchersPaginator: MatPaginator;
	@ViewChild("sort", { static: true }) sort: MatSort;
	@ViewChild("sort2", { static: true }) sort2: MatSort;
	@ViewChild("sort3", { static: true }) sort3: MatSort;
	@ViewChild("invoicesTable", { static: false }) invoicesTable: ElementRef;
	months = [
		{ key: "January", value: 1 },
		{ key: "February", value: 2 },
		{ key: "March", value: 3 },
		{ key: "April", value: 4 },
		{ key: "May", value: 5 },
		{ key: "June", value: 6 },
		{ key: "July", value: 7 },
		{ key: "August", value: 8 },
		{ key: "September", value: 9 },
		{ key: "October", value: 10 },
		{ key: "November", value: 11 },
		{ key: "December", value: 12 },
	];
	years = [];
	displayedColumns: string[] = [
		"sn",
		"providerName",
		"providerType",
		"mappedFieldOfficer",
		"month",
		"year",
		"totalVouchers",
		"annualAmountEarned",
		"totalPayableAmount",
		// "voucherAcceptedByPT",
		// "voucherAcceptedAmountByPT",
		// "voucherRejectedByPT",
		// "voucherRejectedAmountByPT",
		"lastMonthRejectedVoucher",
		"lastMonthRejectedVoucherAmount",
		"billNumber",
		"billDate",
		"PDFInvoice",
		// "bankName",
		"accountNumber",
		"IFSCCode",
		"PANNumber",
		"bankAccountPhoto",
		"PANCard",
	];
	displayedColumnsUserWise: string[] = [
		"select",
		"sn",
		"patientName",
		"mobile",
		"adityaId",
		"nikshayId",
		"voucherName",
		"voucherCode",
		"voucherCreatedAt",
		"voucherRedeemedAt",
		"incentiveAmount",
		"proofOfService",
	];
	rejectedVouchersDisplayedColumns: string[] = [
		"select",
		"sn",
		"patientName",
		"mobile",
		"adityaId",
		"nikshayId",
		"voucherName",
		"voucherCode",
		"month",
		"year",
		"incentiveAmount",
		"voucherCreatedAt",
		"voucherRedeemedAt",
		"voucherRejectedAt",
		"rejectionRemarks",
		"oldProofOfService",
		"newProofOfService",
	];
	selection = new SelectionModel(true, []);
	selectionRejected = new SelectionModel(true, []);
	dataSource: MatTableDataSource<any>;
	dataSourceUserWise: MatTableDataSource<any>;
	showUserDetailsTable: boolean = false;
	showUserWiseDetailedTable: boolean = false;
	selectedUser: any;
	showProcessing: boolean = false;
	rejectedVouchersdataSource: MatTableDataSource<any>;
	showRejectedVouchersDetailedTable: boolean = false;
	constructor(
		private _fb: FormBuilder,
		private _invoiceHistoryService: InvoiceHistoryService,
		private _referralMasterService: ReferralMasterService,
		private _containerService: ContainerService,
		private _listMasterService: ListMasterService,
		private _cdr: ChangeDetectorRef,
		public dialog: MatDialog,
		private _invoiceRejectedVoucherService: InvoiceRejectedVoucherService,
		private smsService: SmsService,
	) {
		this.createForm();
	}
	createForm() {
		this.invoiceForm = this._fb.group({
			month: new FormControl("", Validators.required),
			year: new FormControl("", Validators.required),
		});
	}
	ngOnInit() {
		let counter = 2;
		let currentYear = new Date().getFullYear();
		for (let i = 0; i < counter; i++) {
			this.years.push({
				key: currentYear,
			});
			currentYear--;
		}
	}

	getAllGeneratedInvoices() {
		this.showProcessing = true;
		this.showUserWiseDetailedTable = false;
		this.showRejectedVouchersDetailedTable = false;
		const filter = {
			where: {
				month: this.invoiceForm.value.month,
				year: this.invoiceForm.value.year,
				invoiceStatus: {
					nin: [
						"approvedByPT",
						"rejectedByPT",
						"approvedByFT",
						"rejectedByFT",
					],
				},
			},
			include: [
				{
					relation: "user",
					scope: {
						include: ["fieldOfficer","panCard", "bankAccountPhoto"],
					},
				},
			],
		};
		const date = new Date(
			`${this.invoiceForm.value.month}-1-${this.invoiceForm.value.year}`
		).setHours(0, 0, 0, 0);
		console.log("date ... > ", date);

		const rejectedFilter = {
			invoiceRejectedDate: date,
		};

		const allInvoicesHistory = this._invoiceHistoryService.getInvoiceHistory(
			filter
		);
		const getAllRejectedVouchers = this._invoiceRejectedVoucherService.getRejectedVoucherList(
			rejectedFilter
		);
		forkJoin([allInvoicesHistory, getAllRejectedVouchers]).subscribe(
			([data1, data2]: any) => {
				this.showProcessing = false;
				if (data1.length == 0) {
					const Toast = Swal.mixin({
						toast: true,
						position: "center",
						showConfirmButton: false,
						timer: 4000,
						timerProgressBar: true,
					});
					Toast.fire({
						icon: "error",
						title: `No Invoice Found !!`,
					});
					this.showUserDetailsTable = false;
					this._cdr.detectChanges();
				} else {
					const updatedData = data1.map((data1Res) => {
						let obj = {
							id: data1Res.id,
							notificationReferralId: data1Res.notificationReferralId
								? data1Res.notificationReferralId
								: [],
							reimbursementFDCReferralId: data1Res.reimbursementFDCReferralId
								? data1Res.reimbursementFDCReferralId
								: [],
							reimbursementXRAYReferralId: data1Res.reimbursementXRAYReferralId
								? data1Res.reimbursementXRAYReferralId
								: [],
							reimbursementDOCTORReferralId: data1Res.reimbursementDOCTORReferralId
								? data1Res.reimbursementDOCTORReferralId
								: [],

							name: data1Res.user.name,
							mobile: data1Res.user.mobile,
							designationName: data1Res.user.designationName,
							month: data1Res.month,
							year: data1Res.year,
							invoiceId: data1Res.id,
							userId: data1Res.userId,
							fieldOfficerName: data1Res.user.fieldOfficer ? data1Res.user.fieldOfficer.name : "---",
							billDate: data1Res.billDate
								? new DatePipe("en-US").transform(
										new Date(data1Res.billDate),
										"dd-MM-yyyy hh:mm:ss a"
								  )
								: "----",
							billNumber: data1Res.invoiceBillNumber,
							pdfLink: data1Res.pdfLink,
							invoiceAmount: data1Res.invoiceAmount,
							currMonthVoucherRejectedAmountByPT : data1Res.currMonthVoucherRejectedAmountByPT || 0,
							voucherAcceptedAmountByPT : data1Res.voucherAcceptedAmountByPT || 0,
							rejectedVoucherRejectedAmountByPT : data1Res.rejectedVoucherRejectedAmountByPT || 0,
							rejectedVoucherAcceptedAmountByPT : data1Res.rejectedVoucherAcceptedAmountByPT || 0,
							totalVouchers:
								data1Res.notificationTotalVoucher +
								data1Res.reimbursementTotalVoucher +
								(data1Res.invoiceRejectedVoucherId ? data1Res.invoiceRejectedVoucherId.length : 0),
							// bankName: "----",
							accountNumber:
								data1Res.user.providerBankAccountNumber,
							IFSCCode: data1Res.user.providerBankIFSCCode,
							PANNumber: data1Res.user.providerPANCardNumber,
							bankAccountPhoto: data1Res.user.bankAccountPhoto,
							PANCard: data1Res.user.panCard,
						};

						const rejectedVoucherFoundIndex = data2.findIndex(
							(i) =>
								i.userId.toString() ===
								data1Res.user.id.toString()
						);
						if (rejectedVoucherFoundIndex !== -1) {
							const rejData = data2[rejectedVoucherFoundIndex];
							obj["lastMonthRejectedVoucher"] =
								rejData.totalVoucher;
							obj["lastMonthRejectedVoucherAmount"] =
								rejData.totalAmount;
							obj["invoiceRejectedVoucherId"] =
								rejData.invoiceRejectedVoucherId;
						}

						return obj;
					});
					this.showUserDetailsTable = true;
					updatedData.sort((a, b) => a.name.localeCompare(b.name));
					this.dataSource = new MatTableDataSource(updatedData);
					setTimeout(() => {
						this.dataSource.paginator = this.paginator;
						this.dataSource.sort = this.sort;
					}, 200);
					this.scroll("#invoices-table");
					this._cdr.detectChanges();
				}
			}
		);
	}
	getDetailedInfoUserWise(data) {
		console.log('getDetailedInfoUserWise..> ',data);

		this.showProcessing = true;
		this.selection.clear();
		this.selectedUser = data;
		let referralIds = [];
		referralIds = referralIds
			.concat(
				this.selectedUser.notificationReferralId,
				this.selectedUser.reimbursementFDCReferralId,
				this.selectedUser.reimbursementXRAYReferralId,
				this.selectedUser.reimbursementDOCTORReferralId
			)
			.filter(Boolean);
		if (data.voucherRejectedByPT && data.voucherRejectedByPT.length > 0) {
			referralIds = referralIds.filter(
				(el) => !data.voucherRejectedByPT.includes(el)
			);
		}
		const filter = {
			where: {
				_id: { inq: referralIds },
				invoiceStatus: {
					nin: ["voucherAcceptedByPT", "voucherRejectedByPT"],
				},
			},
			include: [
				{
					relation: "patient",
					scope: {},
				},
				{
					relation: "file",
				},
				{
					relation: "FDCRedeemedFile",
				},
				{
					relation: "voucher",
					scope: {
						include: ["incentive"],
					},
				},
			],
		};
		const rejectedFilter = {
			where: {
				_id: { inq: data.invoiceRejectedVoucherId },
			},
			include: [
				{
					relation: "user",
				},
				{
					relation: "patient",
				},
				{
					relation: "referral",
					scope: {
						include: ["file", "FDCRedeemedFile"],
					},
				},
				{
					relation: "file",
				},
				{
					relation: "voucher",
					scope: {
						include: ["incentive"],
					},
				},
			],
		};
		const allVouchers = this._referralMasterService.getReferralData(filter);
		const allRejectedVouchers = this._invoiceRejectedVoucherService.getRejectedVoucherDetails(
			rejectedFilter
		);
		forkJoin([allVouchers, allRejectedVouchers]).subscribe(
			([data1, data2]: any) => {
				if (data1.length > 0) {
					this.showProcessing = false;
					this.dataSourceUserWise = new MatTableDataSource(data1);
					setTimeout(() => {
						this.dataSourceUserWise.paginator = this.paginatorUserWise;
						this.dataSourceUserWise.sort = this.sort2;
					}, 200);
					this.showUserWiseDetailedTable = true;
					this.scroll("#userWiseDetailedTable");
					this._cdr.detectChanges();
				} else {
					const invoiceHIstoryWhere = {
						_id: this.selectedUser.id,
					};
					const updateInvoiceHistoryData = {
						currentVoucherStatus: true,
					};
					this._invoiceHistoryService
						.updateInvoiceHistory(
							invoiceHIstoryWhere,
							updateInvoiceHistoryData
						)
						.subscribe((updateInvoiceHistoryRes) => {
							this._cdr.detectChanges();
						});
				}
				if (data2.length > 0) {
					this.showProcessing = false;
					this.rejectedVouchersdataSource = new MatTableDataSource(
						data2
					);
					setTimeout(() => {
						this.rejectedVouchersdataSource.paginator = this.rejectedVouchersPaginator;
						this.rejectedVouchersdataSource.sort = this.sort3;
					}, 200);
					this.showRejectedVouchersDetailedTable = true;
					data1.legth > 0
						? this.scroll("#userWiseDetailedTable")
						: this.scroll("#userWiseRejectedTable");
					this._cdr.detectChanges();
				} else {
					const invoiceHIstoryWhere = {
						_id: this.selectedUser.id,
					};
					const updateInvoiceHistoryData = {
						rejectedVoucherStatus: true,
					};
					this._invoiceHistoryService
						.updateInvoiceHistory(
							invoiceHIstoryWhere,
							updateInvoiceHistoryData
						)
						.subscribe((updateInvoiceHistoryRes) => {
							this._cdr.detectChanges();
						});
				}
			}
		);
		this._cdr.detectChanges();
	}
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSourceUserWise.data.length;
		return numSelected === numRows;
	}
	masterToggle() {
		this.isAllSelected()
			? this.selection.clear()
			: this.dataSourceUserWise.data.forEach((row) =>
					this.selection.select(row)
			  );
	}
	checkboxLabel(row): string {
		if (!row) {
			return `${this.isAllSelected() ? "select" : "deselect"} all`;
		}
		return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${
			row.position + 1
		}`;
	}
	isAllRejectedSelected() {
		const numSelected = this.selectionRejected.selected.length;
		const numRows = this.rejectedVouchersdataSource.data.length;
		return numSelected === numRows;
	}

	masterToggleRejected() {
		this.isAllRejectedSelected()
			? this.selectionRejected.clear()
			: this.rejectedVouchersdataSource.data.forEach((row) =>
					this.selectionRejected.select(row)
			  );
	}

	rejectedCheckboxLabel(row): string {
		if (!row) {
			return `${
				this.isAllRejectedSelected() ? "select" : "deselect"
			} all`;
		}
		return `${
			this.selectionRejected.isSelected(row) ? "deselect" : "select"
		} row ${row.position + 1}`;
	}

	downloadFile(container, modifiedFileName) {
		this._containerService
			.downloadFile(container, modifiedFileName)
			.subscribe((res) => {
				var fileURL = window.URL.createObjectURL(res);
				// let tab = window.open();
				// tab.location.href = fileURL;
				window.open(
					fileURL,
					"Proof",
					"toolbar=yes, resizable=yes, scrollbars=yes, width=400, height=400, top=200 , left=500"
				);
			});
	}

	getPatientDetail(row) {
		console.log(row);

		const dialogRef = this.dialog.open(PatientInfoComponent, {
			data: row,
		});

		dialogRef.afterClosed().subscribe((result) => {
			console.log(`Dialog result: ${result}`);
		});
	}

	getRejectionRemarks(): Promise<any> {
		return new Promise((resolve, reject) => {
			this._listMasterService
				.getStakeholder({
					where: { type: "rejection" },
					order: "name.key ASC",
				})
				.subscribe(
					(res) => {
						resolve(res[0].name);
					},
					(err) => {
						reject(err);
					}
				);
		});
	}

	async rejectInvoice() {
		if (this.selection.selected.length == 0) {
			const Toast = Swal.mixin({
				toast: true,
				position: "center",
				showConfirmButton: false,
				timer: 4000,
				timerProgressBar: true,
			});
			Toast.fire({
				icon: "error",
				title: "Please select atleast one voucher.",
			});
		} else {
			let selectOptions = await this.getRejectionRemarks();
			(Swal as any)
				.fire({
					title: "Please select a reason for the denial of voucher.",
					icon: "info",
					// input: "textarea",
					// inputOptions:""
					// inputPlaceholder: "Type here...",
					input: "select",
					inputOptions: { select: "Select One", ...selectOptions },
					inputValidator: (value) => {
						if (!value || value === "select") {
							return "You need to choose a reason!";
						}
					},
					allowOutsideClick: false,
					allowEscapeKey: false,
					showCancelButton: true,
					confirmButtonColor: "#16a184",
					cancelButtonColor: "#f4516c",
					confirmButtonText: "Submit",
				})
				.then(async (reasonRes) => {


					if (reasonRes.value) {
						let reason = reasonRes;
						if (reasonRes.value === "other") {
							const otherValue = await (Swal as any).fire({
							title: "Please type a reason for the denial of voucher.",
							icon: "info",
							input: "textarea",
							inputPlaceholder: "Type here...",
							inputValidator: value => {
							  if (!value) {
								return "You need to type a reason!";
							  }
							},
							showConfirmButton: true,
							showCancelButton: true
						  });
						  reason = otherValue;
						}

						// Tarun - 19:16 - Rejected SMS Function
						const where = {
							where: {
								type: this.smsType.userInvoicePaymentClaimRejection
							}
						};
						const getSmsData = await this.smsService.getSMSData(where).toPromise();
						let newDataArr = [];
						for (let i = 0; i < getSmsData.length; i++) {
							let message = getSmsData[i].msg;
							let newUrl = getSmsData[i].url;
							newUrl = getSmsData[i].url.replace("<msg>", message);
							newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
							newDataArr.push(newUrl);
						}
						this.smsService.userRegistrationSms(newDataArr).subscribe(data => {
							console.log(data);
						}, err => {
							console.log(err);
						});
						// Tarun - 19:16 - Rejected SMS Function

						const update = await this.rejectSelectedVouchers(reason);
					} else {
						const Toast = Swal.mixin({
							toast: true,
							position: "center",
							showConfirmButton: false,
							timer: 4000,
							timerProgressBar: true,
						});
						Toast.fire({
							icon: "info",
							title: "Cancelled.",
						});
					}
				});
		}
	}
	rejectSelectedVouchers( reasonRes ): Promise<any> {
		return new Promise((resolve, reject) => {

			let referralIds = this.selection.selected.map(
				(item) => {
					return item.id;
				}
			);
			console.log(referralIds);

			const where = {
				_id: { inq: referralIds },
			};
			const updateData = {
				invoiceStatus: "voucherRejectedByPT",
				voucherRejectedByPTRemarks: reasonRes.value,
				voucherRejectedByPTAt: new Date().getTime(),
			};
			this._referralMasterService
				.updateReferralData(where, updateData)
				.subscribe((res) => {
					console.log(res);
					if (res) {
						const whereObj = {
							where: { _id: this.selectedUser.id },
						};
						this._invoiceHistoryService
							.getInvoiceHistory(whereObj)
							.subscribe((getInvoiceHistoryRes) => {
								console.log(getInvoiceHistoryRes);
								let rejectedAmount = this.selection.selected.reduce(
									(acc, curr) => {
										return (
											acc +
											curr.voucher
												.incentive[0].amount
										);
									},
									0
								);

								if (
									getInvoiceHistoryRes[0]
										.currMonthVoucherRejectedAmountByPT &&
									getInvoiceHistoryRes[0]
										.currMonthVoucherRejectedAmountByPT >
										0
								) {
									rejectedAmount =
										rejectedAmount +
										getInvoiceHistoryRes[0]
											.currMonthVoucherRejectedAmountByPT;
								}
								if (
									getInvoiceHistoryRes[0]
										.voucherRejectedByPT &&
									getInvoiceHistoryRes[0]
										.voucherRejectedByPT
										.length > 0
								) {
									referralIds = referralIds.concat(
										getInvoiceHistoryRes[0]
											.voucherRejectedByPT
									);
									referralIds = referralIds.filter(
										(item, pos) =>
											referralIds.indexOf(
												item
											) === pos
									);
								}

								const where = {
									_id: getInvoiceHistoryRes[0].id,
								};

								const updateData = {
									currMonthVoucherRejectedAmountByPT: rejectedAmount,
									voucherRejectedByPT: referralIds,
								};
								if (
									this.dataSourceUserWise.data
										.length ==
									this.selection.selected.length
								) {
									updateData["invoiceStatus"] =
										"approvedByPT";
									updateData[
										"voucherAcceptedByPTAt"
									] = new Date().getTime();
								}
								this._invoiceHistoryService
									.updateInvoiceHistory(
										where,
										updateData
									)
									.subscribe(
										(
											updateInvoiceHistoryRes
										) => {
											console.log(
												"selection.selected >> ",
												this.selection.selected
											);
											const rejectedDate = `${this.invoiceForm.value.month}-1-${this.invoiceForm.value.year}`;
											let rejectedVouchers = this.selection.selected.map(
												(item) => {
													let data = {
														invoiceId: this.selectedUser.id,
														billNumber: this.selectedUser.billNumber,
														userId: this.selectedUser.userId,
														month: this.selectedUser.month,
														year: this.selectedUser.year,
														referralId: item.id,
														patientId: item.patientId,
														voucherId: item.voucherId,
														voucherCode: item.voucherCode,
														voucherCreatedAt: item.voucherCreatedAt,
														voucherRedeemedAt: item.voucherRedeemedAt,
														mobile: item.mobile,
														adityaId: item.adityaId,
														unitCost:item.voucher.incentive[0].amount,
														voucherRejectedByPTRemarks: reasonRes.value,
														voucherRejectedByPTAt: new Date().getTime(),
														invoiceRejectedDate: new Date(rejectedDate).setMonth(new Date(rejectedDate).getMonth()+1),
														rejectedBy: this.currentUser.id,
														invoiceStatus:"notClear"
													};
													if (item.voucherId == "5ff565416dede21747a682b9") {
														data["fileId"] = item.fileId;
													} else if (item.voucherId == "5ff5659f6dede21747a682bd") {
														data["fileId"] = item.FDCRedeemedFileId;
													} else if (item.voucherId == "601112b06dede219e6eac8e7") {
														data["fileId"] = item.CBNAATReportFileId;
													} else if (item.voucherId == "602127ad6dede214bc03966c") {
														data["fileId"] = item.fileId;
													}
													return data;
												}
											);
											console.log(
												"rejectedVouchers .. > ",
												rejectedVouchers
											);
											this._invoiceRejectedVoucherService
												.postRejectedVoucher(
													rejectedVouchers
												)
												.subscribe(
													(
														rejectedVouchersRes
													) => {
														console.log(
															rejectedVouchersRes
														);
														if (
															rejectedVouchersRes
														) {
															resolve(rejectedVouchersRes);
															this.showUserWiseDetailedTable = false;
															this.getAllGeneratedInvoices();
															this.selection.clear();
															this._cdr.detectChanges();
														}
													},(err) => {
																	reject(err);
																}
												);
										}
									);
								this._cdr.detectChanges();
							});
					}
				});
		});
	}

	acceptInvoice() {
		if (this.selection.selected.length == 0) {
			const Toast = Swal.mixin({
				toast: true,
				position: "center",
				showConfirmButton: false,
				timer: 4000,
				timerProgressBar: true,
			});
			Toast.fire({
				icon: "error",
				title: "Please select atleast one voucher.",
			});
		} else {
			let referralIds = this.selection.selected.map((item) => {
				return item.id;
			});
			console.log(referralIds);

			const where = {
				_id: { inq: referralIds },
			};
			const updateData = {
				invoiceStatus: "voucherAcceptedByPT",
				voucherAcceptedByPTAt: new Date().getTime(),
			};
			this._referralMasterService
				.updateReferralData(where, updateData)
				.subscribe((res) => {
					console.log(res);
					if (res) {
						const whereObj = {
							where: { _id: this.selectedUser.id },
						};
						this._invoiceHistoryService
							.getInvoiceHistory(whereObj)
							.subscribe((getInvoiceHistoryRes) => {
								console.log(getInvoiceHistoryRes);

								let acceptedAmount = this.selection.selected.reduce(
									(acc, curr) => {
										return (
											acc +
											curr.voucher.incentive[0].amount
										);
									},
									0
								);
								console.log(
									"acceptedAmount ..> ",
									acceptedAmount
								);
								if (
									getInvoiceHistoryRes[0]
										.voucherAcceptedAmountByPT &&
									getInvoiceHistoryRes[0]
										.voucherAcceptedAmountByPT > 0
								) {
									acceptedAmount =
										acceptedAmount +
										getInvoiceHistoryRes[0]
											.voucherAcceptedAmountByPT;
								}
								if (
									getInvoiceHistoryRes[0]
										.voucherAcceptedByPT &&
									getInvoiceHistoryRes[0].voucherAcceptedByPT
										.length > 0
								) {
									referralIds = referralIds.concat(
										getInvoiceHistoryRes[0]
											.voucherAcceptedByPT
									);
									referralIds = referralIds.filter(
										(item, pos) =>
											referralIds.indexOf(item) === pos
									);
								}
								const where = {
									_id: getInvoiceHistoryRes[0].id,
								};
								let updateData = {
									voucherAcceptedAmountByPT: acceptedAmount,
									voucherAcceptedByPT: referralIds,
								};
								console.log(
									"dataSourceUserWise.data",
									this.dataSourceUserWise.data.length,
									"==",
									this.selection.selected.length
								);

								if (
									this.dataSourceUserWise.data.length ==
									this.selection.selected.length
								) {
									if (getInvoiceHistoryRes[0].rejectedVoucherStatus === true) {
										updateData["invoiceStatus"] =  "approvedByPT";
										updateData["actionTakenByPTId"] =  this.currentUser.id;
                    					updateData["currentVoucherStatus"] = true;
										updateData["voucherAcceptedByPTAt"] = new Date().getTime();
									} else {
										updateData["currentVoucherStatus"] = true;
									}
								}
								this._invoiceHistoryService
									.updateInvoiceHistory(where, updateData)
									.subscribe(async (updateInvoiceHistoryRes) => {

										// Tarun - 19:16 - Rejected SMS Function
										const whereAccept = {
											where: {
												type: this.smsType.user_invoice_payment_claim_approval
											}
										};
										const whereInitialize = {
											where: {
												type: this.smsType.user_invoice_payment_initialize
											}
										};
										const getSmsData = await this.smsService.getSMSData(whereAccept).toPromise();
										let newDataArr = [];
										for (let i = 0; i < getSmsData.length; i++) {
											let message = getSmsData[i].msg;
											let newUrl = getSmsData[i].url;
											newUrl = getSmsData[i].url.replace("<msg>", message);
											newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
											newDataArr.push(newUrl);
										}
										this.smsService.userRegistrationSms(newDataArr).subscribe(data => {
											console.log(data);
										}, err => {
											console.log(err);
										});

										const getSmsDataSecond = await this.smsService.getSMSData(whereInitialize).toPromise();
										let newDataArr2 = [];
										for (let i = 0; i < getSmsDataSecond.length; i++) {
											let message = getSmsDataSecond[i].msg;
											let newUrl = getSmsDataSecond[i].url;
											newUrl = getSmsDataSecond[i].url.replace("<msg>", message);
											newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
											newDataArr2.push(newUrl);
										}
										this.smsService.userRegistrationSms(newDataArr2).subscribe(data => {
											console.log(data);
										}, err => {
											console.log(err);
										});
										// Tarun - 19:16 - Rejected SMS Function

										this.showUserWiseDetailedTable = false;
										this.selection.clear();
										this.getAllGeneratedInvoices();
										this._cdr.detectChanges();
									});
								// } else {
								//     this.showUserWiseDetailedTable = false;
								//     this.selection.clear();
								//     this.getAllGeneratedInvoices();
								//     this._cdr.detectChanges();
								// }
							});
					}
				});
			this._cdr.detectChanges();
		}
	}
	acceptRejectedVouchers() {
		if (this.selectionRejected.selected.length == 0) {
			const Toast = Swal.mixin({
				toast: true,
				position: "center",
				showConfirmButton: false,
				timer: 4000,
				timerProgressBar: true,
			});
			Toast.fire({
				icon: "error",
				title: "Please select atleast one voucher.",
			});
		} else {
			console.log(
				"selectionRejected .. >",
				this.selectionRejected.selected
			);

			let referralIds = this.selectionRejected.selected.map((item) => {
				return item.referral.id;
			});
			console.log(referralIds);
			let rejectedVoucherIds = this.selectionRejected.selected.map(
				(item) => {
					return item.id;
				}
			);
			console.log(rejectedVoucherIds);

			// Update InvoiceRejectedVoucher Collection
			const invoiceRejectedWhere = {
				_id: { inq: rejectedVoucherIds },
			};
			const invoiceRejectedData = {
				invoiceStatus: "clear",
				invoiceAcceptedByPTAt: new Date().getTime(),
				acceptedBy: this.currentUser.id,
			};
			const invoiceRejectedVoucher = this._invoiceRejectedVoucherService.updateData(
				invoiceRejectedWhere,
				invoiceRejectedData
			);
			// Update ReferralMaster Collection
			let rejectedReferralIds = this.selectionRejected.selected.map(
				(item) => {
					return item.referral.id;
				}
			);
			const referralMasterWhere = {
				_id: { inq: rejectedReferralIds },
			};
			const referralMasterData = {
				invoiceStatus: "voucherAcceptedByPT",
				voucherAcceptedByPTAt: new Date().getTime(),
			};
			const referralMaster = this._referralMasterService.updateReferralData(
				referralMasterWhere,
				referralMasterData
			);

			const invoiceHistoryFilter = {
				where: {
					_id: this.selectedUser.id,
				},
			};
			const invoiceHistory = this._invoiceHistoryService.getInvoiceHistory(
				invoiceHistoryFilter
			);
			forkJoin([
				invoiceRejectedVoucher,
				referralMaster,
				invoiceHistory,
			]).subscribe(([data1, data2, data3]: any) => {
				console.log(data1, data2, data3);

				let acceptedAmount = this.selectionRejected.selected.reduce(
					(acc, curr) => {
						return acc + curr.voucher.incentive[0].amount;
					},
					0
				);
				console.log("acceptedAmount..> ", acceptedAmount);

				if (
					data3[0].rejectedVoucherAcceptedAmountByPT &&
					data3[0].rejectedVoucherAcceptedAmountByPT > 0
				) {
					acceptedAmount =
						acceptedAmount +
						data3[0].rejectedVoucherAcceptedAmountByPT;
				}
				if (
					data3[0].rejectedVoucherAcceptedByPT &&
					data3[0].rejectedVoucherAcceptedByPT.length > 0
				) {
					rejectedReferralIds = rejectedReferralIds.concat(
						data3[0].rejectedVoucherAcceptedByPT
					);
					rejectedReferralIds = rejectedReferralIds.filter(
						(item, pos) => rejectedReferralIds.indexOf(item) === pos
					);
				}
				const invoiceHIstoryWhere = {
					_id: data3[0].id,
				};
				let updateInvoiceHistoryData = {
					rejectedVoucherAcceptedAmountByPT: acceptedAmount,
					rejectedVoucherAcceptedByPT: rejectedReferralIds,
				};
				console.log(
					"dataSourceUserWise.data",
					this.rejectedVouchersdataSource.data.length,
					"==",
					this.selectionRejected.selected.length
				);
				if (
					this.rejectedVouchersdataSource.data.length ===
					this.selectionRejected.selected.length
				) {
					if (data3[0].currentVoucherStatus === true) {
						updateInvoiceHistoryData["invoiceStatus"] =
							"approvedByPT";
						updateInvoiceHistoryData[
							"voucherAcceptedByPTAt"
						] = new Date().getTime();
					} else {
						updateInvoiceHistoryData[
							"rejectedVoucherStatus"
						] = true;
					}
				}
				this._invoiceHistoryService
					.updateInvoiceHistory(
						invoiceHIstoryWhere,
						updateInvoiceHistoryData
					)
					.subscribe(async (updateInvoiceHistoryRes) => {

						// Tarun - 19:16 - Rejected SMS Function
						const whereAccept = {
							where: {
								type: this.smsType.user_invoice_payment_claim_approval
							}
						};
						const whereInitialize = {
							where: {
								type: this.smsType.user_invoice_payment_initialize
							}
						};
						const getSmsData = await this.smsService.getSMSData(whereAccept).toPromise();
						let newDataArr = [];
						for (let i = 0; i < getSmsData.length; i++) {
							let message = getSmsData[i].msg;
							let newUrl = getSmsData[i].url;
							newUrl = getSmsData[i].url.replace("<msg>", message);
							newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
							newDataArr.push(newUrl);
						}
						this.smsService.userRegistrationSms(newDataArr).subscribe(data => {
							console.log(data);
						}, err => {
							console.log(err);
						});

						const getSmsDataSecond = await this.smsService.getSMSData(whereInitialize).toPromise();
						let newDataArr2 = [];
						for (let i = 0; i < getSmsDataSecond.length; i++) {
							let message = getSmsDataSecond[i].msg;
							let newUrl = getSmsDataSecond[i].url;
							newUrl = getSmsDataSecond[i].url.replace("<msg>", message);
							newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
							newDataArr2.push(newUrl);
						}
						this.smsService.userRegistrationSms(newDataArr2).subscribe(data => {
							console.log(data);
						}, err => {
							console.log(err);
						});
						// Tarun - 19:16 - Rejected SMS Function

						this.showUserWiseDetailedTable = false;
						this.showRejectedVouchersDetailedTable = false;
						this.selection.clear();
						this.selectionRejected.clear();
						this.getAllGeneratedInvoices();
						this._cdr.detectChanges();
					});
			});
			this._cdr.detectChanges();
		}
	}
	async rejectRejectedVouchers() {
		if (this.selectionRejected.selected.length == 0) {
			const Toast = Swal.mixin({
				toast: true,
				position: "center",
				showConfirmButton: false,
				timer: 4000,
				timerProgressBar: true,
			});
			Toast.fire({
				icon: "error",
				title: "Please select atleast one voucher.",
			});
		} else {
			let selectOptions = await this.getRejectionRemarks();
			(Swal as any)
				.fire({
					title: "Please enter a reason for the denial of voucher.",
					icon: "info",
					input: "select",
					inputOptions: { select: "Select One", ...selectOptions },
					inputValidator: (value) => {
						if (!value || value === "select") {
							return "You need to choose a reason!";
						}
					},
					allowOutsideClick: false,
					allowEscapeKey: false,
					showCancelButton: true,
					confirmButtonColor: "#16a184",
					cancelButtonColor: "#f4516c",
					confirmButtonText: "Submit",
				})
				.then( async (reasonRes) => {
					if (reasonRes.value) {
						let reason = reasonRes;
						if (reasonRes.value === "other") {
							const otherValue = await (Swal as any).fire({
							title: "Please type a reason for the denial of voucher.",
							icon: "info",
							input: "textarea",
							inputPlaceholder: "Type here...",
							inputValidator: value => {
							  if (!value) {
								return "You need to type a reason!";
							  }
							},
							showConfirmButton: true,
							showCancelButton: true
						  });
						  reason = otherValue;
						}

						let referralIds = this.selectionRejected.selected.map(
							(item) => {
								return item.referral.id;
							}
						);
						console.log(referralIds);
						let rejectedVoucherIds = this.selectionRejected.selected.map(
							(item) => {
								return item.id;
							}
						);
						console.log(rejectedVoucherIds);

						// Update InvoiceRejectedVoucher Collection
						const invoiceRejectedWhere = {
							_id: { inq: rejectedVoucherIds },
						};
						const invoiceRejectedData = {
							invoiceStatus:"notClear",
							voucherRejectedByPTRemarks: reason.value,
							invoiceRejectedByPTAt: new Date().getTime(),
							rejectedBy: this.currentUser.id,
						};
						const invoiceRejectedVoucher = this._invoiceRejectedVoucherService.updateData(
							invoiceRejectedWhere,
							invoiceRejectedData
						);
						// Update ReferralMaster Collection
						let rejectedReferralIds = this.selectionRejected.selected.map(
							(item) => {
								return item.referral.id;
							}
						);
						const referralMasterWhere = {
							_id: { inq: rejectedReferralIds },
						};
						const referralMasterData = {
							invoiceStatus: "voucherRejectedByPT",
							voucherRejectedByPTRemarks: reason.value,
							voucherRejectedByPTAt: new Date().getTime(),
						};
						const referralMaster = this._referralMasterService.updateReferralData(
							referralMasterWhere,
							referralMasterData
						);

						const invoiceHistoryFilter = {
							where: {
								_id: this.selectedUser.id,
							},
						};
						const invoiceHistory = this._invoiceHistoryService.getInvoiceHistory(
							invoiceHistoryFilter
						);
						forkJoin([
							invoiceRejectedVoucher,
							referralMaster,
							invoiceHistory,
						]).subscribe(([data1, data2, data3]: any) => {
							console.log(data1, data2, data3);

							let rejectedAmount = this.selectionRejected.selected.reduce(
								(acc, curr) => {
									return (
										acc + curr.voucher.incentive[0].amount
									);
								},
								0
							);
							if (
								data3[0].rejectedVoucherRejectedAmountByPT &&
								data3[0].rejectedVoucherRejectedAmountByPT > 0
							) {
								rejectedAmount =
									rejectedAmount +
									data3[0].rejectedVoucherRejectedAmountByPT;
							}
							if (
								data3[0].rejectedVoucherRejectedByPT &&
								data3[0].rejectedVoucherRejectedByPT.length > 0
							) {
								rejectedReferralIds = rejectedReferralIds.concat(
									data3[0].rejectedVoucherRejectedByPT
								);
								rejectedReferralIds = rejectedReferralIds.filter(
									(item, pos) =>
										rejectedReferralIds.indexOf(item) ===
										pos
								);
							}
							const invoiceHIstoryWhere = {
								_id: data3[0].id,
							};
							let updateInvoiceHistoryData = {
								rejectedVoucherRejectedAmountByPT: rejectedAmount,
								rejectedVoucherRejectedByPT: rejectedReferralIds,
							};
							console.log(
								"dataSourceUserWise.data",
								this.rejectedVouchersdataSource.data.length,
								"==",
								this.selectionRejected.selected.length
							);
							if (
								this.rejectedVouchersdataSource.data.length ===
								this.selectionRejected.selected.length
							) {
								if (data3[0].currentVoucherStatus === true) {
									updateInvoiceHistoryData["invoiceStatus"] =
										"approvedByPT";
									updateInvoiceHistoryData[
										"voucherRejectedByPTAt"
									] = new Date().getTime();
								} else {
									updateInvoiceHistoryData[
										"rejectedVoucherStatus"
									] = true;
								}
							}
							this._invoiceHistoryService
								.updateInvoiceHistory(
									invoiceHIstoryWhere,
									updateInvoiceHistoryData
								)
								.subscribe(async (updateInvoiceHistoryRes) => {
									// Tarun - 19:16 - Rejected SMS Function
									const where = {
										where: {
											type: this.smsType.userInvoicePaymentClaimRejection
										}
									};

									const getSmsData = await this.smsService.getSMSData(where).toPromise();
									let newDataArr = [];
									for (let i = 0; i < getSmsData.length; i++) {
										let message = getSmsData[i].msg;
										let newUrl = getSmsData[i].url;
										newUrl = getSmsData[i].url.replace("<msg>", message);
										newUrl = newUrl.replace("<mob>", this.selectedUser.mobile)
										newDataArr.push(newUrl);
									}
									this.smsService.userRegistrationSms(newDataArr).subscribe(data => {
										console.log(data);
									}, err => {
										console.log(err);
									});
									// Tarun - 19:16 - Rejected SMS Function

									this.showUserWiseDetailedTable = false;
									this.showRejectedVouchersDetailedTable = false;
									this.selection.clear();
									this.selectionRejected.clear();
									this.getAllGeneratedInvoices();
									this._cdr.detectChanges();
								});
						});
						this._cdr.detectChanges();
					} else {
						const Toast = Swal.mixin({
							toast: true,
							position: "center",
							showConfirmButton: false,
							timer: 4000,
							timerProgressBar: true,
						});
						Toast.fire({
							icon: "error",
							title: "Cancelled",
						});
					}
				});
		}
	}

	scroll(elem: string) {
		setTimeout(() => {
			document
				.querySelector(elem)
				.scrollIntoView({ behavior: "smooth", block: "start" });
		}, 100);
	}
	openInvoice(fileURL) {
		console.log("fileURL.. >", fileURL);

		window.open(
			fileURL,
			"Proof",
			"toolbar=yes, resizable=yes, scrollbars=yes, width=600, height=400, top=200 , left=500"
		);
	}
	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	applyFilterDataSourceUserWise(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSourceUserWise.filter = filterValue.trim().toLowerCase();

		if (this.dataSourceUserWise.paginator) {
			this.dataSourceUserWise.paginator.firstPage();
		}
	}

	exportDataSourceTable() {
		let dataToExport = this.dataSource.data.map((x) => ({
			Name: x.name || "---",
			"Designation Name": x.designationName || "---",
			Month: x.month || "---",
			Year: x.year || "---",
			"Field Officer Name": x.fieldOfficerName || "---",
			"Bill Number": x.billNumber || "---",
			"Bill Date": x.billDate || "---",
			"Invoice Amount": x.invoiceAmount || "---",
			"Total Vouchers": x.totalVouchers || "---",
			"Invoice Link": {
				f: `=HYPERLINK("${x.pdfLink}", "Invoice PDF")`,
			},
			"Bank Name": x.bankName || "---",
			"Account Number": x.accountNumber || "---",
			"Bank IFSC Code": x.IFSCCode || "---",
			"Bank Account Photo": {
				f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.bankAccountPhoto.container}/download/${x.bankAccountPhoto.modifiedFileName}", "PHOTO LINK")`,
			},
			"PAN Card": {
				f: `=HYPERLINK("${environment.apiEndPoint}containers/${x.PANCard.container}/download/${x.PANCard.modifiedFileName}", "PAN Card LINK")`,
			}
		}));
		const workSheet = XLSX.utils.json_to_sheet(dataToExport);
		const workBook: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(workBook, workSheet, "SheetName");
		XLSX.writeFile(workBook, "Invoices Details.xlsx");
	}
}
