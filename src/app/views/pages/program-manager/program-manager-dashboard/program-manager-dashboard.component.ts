import { Component, OnInit } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { MatTableDataSource } from "@angular/material";
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

@Component({
	selector: 'kt-program-manager-dashboard',
	templateUrl: './program-manager-dashboard.component.html',
	styleUrls: ['./program-manager-dashboard.component.scss']
})
export class ProgramManagerDashboardComponent implements OnInit {
	mapChardDiv4 = "durgMapDiv";
	//Table Data
	REQUESTS = [
		{
			skus: "Moxifloxin",
			ob: 100,
			stockIn: 600,
			stockOut: 300,
			cb: 400,
			change: 300,
			bg: "black-svg",
		},
		{
			skus: "Avelox",
			ob: 200,
			stockIn: 300,
			stockOut: 250,
			cb: 250,
			change: 50,
			bg: "red-svg",
		},
		{
			skus: "RifaPin",
			ob: 50,
			stockIn: 500,
			stockOut: 450,
			cb: 100,
			change: 50,
			bg: "black-svg",
		},
		{
			skus: "Floxacin",
			ob: 100,
			stockIn: 300,
			stockOut: 180,
			cb: 120,
			change: 20,
			bg: "red-svg",
		},

		{
			skus: "Cipro",
			ob: 130,
			stockIn: 200,
			stockOut: 300,
			cb: 30,
			change: -100,
			bg: "red-svg",
		},
	];
	dataSource = new MatTableDataSource(this.REQUESTS);
	displayedColumns = ["skus", "ob", "stockIn", "stockOut", "cb", "change"];
	constructor() {
		this.dataSource.data = this.REQUESTS;
	}
	ngOnInit() {
		this.generateFinancialGraph1();
		this.generateFinancialGraph2();
		this.generateLineGraph();
		this.generatefunnelChart();
	}
	generateFinancialGraph1() {
		// Create chart instance
		let chart = am4core.create("chartdiv", am4charts.XYChart);

		chart.data = [
			{
				date: "Jul'20",
				value1: 35,
				value2: 25,
			},
			{
				date: "Aug'20",
				value1: 65,
				value2: 35,
			},
			{
				date: "Sep'20",
				value1: 55,
				value2: 45,
			},
			{
				date: "Oct'20",
				value1: 60,
				value2: 48,
			},
			{
				date: "Nov'20",
				value1: 47,
				value2: 30,
			},
			{
				date: "Dec'20",
				value1: 50,
				value2: 43,
			},
		];

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "date";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "date";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			series.columns.template.width = am4core.percent(95);

			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			bullet1.label.text = "{valueY}";
			bullet1.fontWeight = 'bold';
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("value1", "Entitle Incentive", false);
		createSeries("value2", "Provided Incentive", true);

		// Add legend
		chart.legend = new am4charts.Legend();
	}
	generateFinancialGraph2() {
		// Create chart instance
		let chart = am4core.create("chartdiv2", am4charts.XYChart);
		chart.colors.list = [
			am4core.color("#283250"),
			am4core.color("#d5433d"),
			am4core.color("#f05440"),
		];
		chart.data = [
			{
				date: "Durg",
				value1: 40,
				value2: 55,
				value3: 25,
			},
			{
				date: "Dhamdha",
				value1: 30,
				value2: 78,
				value3: 45,
			},
			{
				date: "Patan",
				value1: 27,
				value2: 40,
				value3: 12,
			},
		];

		// Create axes
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "date";
		// categoryAxis.title.text = "Local country offices";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 20;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.min = 0;
		// valueAxis.title.text = "Expenditure (M)";

		// Create series
		function createSeries(field, name, stacked) {
			let series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueY = field;
			series.dataFields.categoryX = "date";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
			series.stacked = stacked;
			series.columns.template.width = am4core.percent(50);

			let bullet1 = series.bullets.push(new am4charts.LabelBullet());
			bullet1.interactionsEnabled = false;
			bullet1.label.text = "{valueY}";
			bullet1.fontWeight = 'bold';
			bullet1.label.fill = am4core.color("#ffffff");
			bullet1.locationY = 0.5;
		}

		createSeries("value1", "Entitled Reimbursement", false);
		createSeries("value2", "Proof Uploaded", true);
		createSeries("value3", "Reimbursement Receipt", true);

		// Add legend
		chart.legend = new am4charts.Legend();
	}

	generateLineGraph() {
		// CART 4
		const chart4 = am4core.create("lineGraph", am4charts.XYChart);
		chart4.paddingRight = 20;

		// chart4.colors.list = colors;

		// Create axes
		var dateAxis = chart4.xAxes.push(new am4charts.DateAxis());
		dateAxis.renderer.minGridDistance = 50;
		dateAxis.renderer.grid.template.location = 0.5;
		dateAxis.startLocation = 0.5;
		dateAxis.endLocation = 0.5;

		// Create value axis
		var valueAxis = chart4.yAxes.push(new am4charts.ValueAxis());

		chart4.legend = new am4charts.Legend();
		chart4.legend.align = "center";
		chart4.legend.fontSize = 12;
		// chart4.legend.labels.template.text = '{category}';
		// chart4.legend.valueLabels.template.text = '';

		chart4.legend.markers.template.marginRight = 2;
		chart4.legend.markers.template.width = 16;
		chart4.legend.markers.template.height = 16;

		chart4.cursor = new am4charts.XYCursor();
		chart4.cursor.tooltipText = "test";

		const chart4data = [
			{
				category: "Jan'20",
				"column-1": 600,
				"column-2": 550,
				"column-3": 500,
				"column-4": 40,
				"column-5": 300,
			},
			{
				category: "Feb'20",
				"column-1": 510,
				"column-2": 450,
				"column-3": 395,
				"column-4": 34,
				"column-5": 100,
			},
			{
				category: "Mar'20",
				"column-1": 500,
				"column-2": 430,
				"column-3": 398,
				"column-4": 10,
				"column-5": 290,
			},
			{
				category: "April'20",
				"column-1": 540,
				"column-2": 452,
				"column-3": 393,
				"column-4": 26,
				"column-5": 200,
			},
			{
				category: "May'20",
				"column-1": 400,
				"column-2": 390,
				"column-3": 300,
				"column-4": 16,
				"column-5": 201,
			},
			{
				category: "June'20",
				"column-1": 600,
				"column-2": 510,
				"column-3": 421,
				"column-4": 12,
				"column-5": 201,
			},
		];

		var xySeries201 = chart4.series.push(new am4charts.LineSeries());
		xySeries201.dataFields.valueY = "column-1";
		xySeries201.dataFields.dateX = "category";
		xySeries201.strokeWidth = 3;
		xySeries201.tensionX = 0.8;
		xySeries201.bullets.push(new am4charts.CircleBullet());
		xySeries201.data = chart4data;
		xySeries201.name = "Patients Sent For CBNAAT";
		xySeries201.tooltipText = "{name}: [bold]{valueY}[/]";
		xySeries201.strokeWidth = 1;

		var xySeries202 = chart4.series.push(new am4charts.LineSeries());
		xySeries202.dataFields.valueY = "column-2";
		xySeries202.dataFields.dateX = "category";
		xySeries202.strokeWidth = 3;
		xySeries202.tensionX = 0.8;
		xySeries202.bullets.push(new am4charts.CircleBullet());
		xySeries202.data = chart4data;
		xySeries202.name = "Samples Collected";
		xySeries202.tooltipText = "{name}: [bold]{valueY}[/]";
		xySeries202.strokeWidth = 1;

		var xySeries3 = chart4.series.push(new am4charts.LineSeries());
		xySeries3.dataFields.valueY = "column-3";
		xySeries3.dataFields.dateX = "category";
		xySeries3.strokeWidth = 3;
		xySeries3.tensionX = 0.8;
		xySeries3.bullets.push(new am4charts.CircleBullet());
		xySeries3.data = chart4data;
		xySeries3.name = "Samples Delivered";
		xySeries3.tooltipText = "{name}: [bold]{valueY}[/]";
		xySeries3.strokeWidth = 1;

		var xySeries4 = chart4.series.push(new am4charts.LineSeries());
		xySeries4.dataFields.valueY = "column-4";
		xySeries4.dataFields.dateX = "category";
		xySeries4.strokeWidth = 3;
		xySeries4.tensionX = 0.8;
		xySeries4.bullets.push(new am4charts.CircleBullet());
		xySeries4.data = chart4data;
		xySeries4.name = "Microbiologically Confirmed TB Cases";
		xySeries4.tooltipText = "{name}: [bold]{valueY}[/]";
		xySeries4.strokeWidth = 1;

		var xySeries4 = chart4.series.push(new am4charts.LineSeries());
		xySeries4.dataFields.valueY = "column-5";
		xySeries4.dataFields.dateX = "category";
		xySeries4.strokeWidth = 3;
		xySeries4.tensionX = 0.8;
		xySeries4.bullets.push(new am4charts.CircleBullet());
		xySeries4.data = chart4data;
		xySeries4.name = "Reports Collected and Delivered";
		xySeries4.tooltipText = "{name}: [bold]{valueY}[/]";
		xySeries4.strokeWidth = 1;
	}
	generatefunnelChart() {
		let chart = am4core.create("funnelGraph", am4charts.SlicedChart);
		chart.data = [
			{
				name: "Leads generated",
				value: 600,
			},
			{
				name: "Vouchers for Chest X-Ray issued",
				value: 400,
			},
			{
				name: "Vouchers for Chest X-Ray redeemed",
				value: 300,
			},
			{
				name: "Patients with X-Ray abnormalities",
				value: 180,
			},
			{
				name: "Vouchers for clinic visit issued",
				value: 150,
			},
			{
				name: "All patients initiated on TB treatment",
				value: 100,
			},
			{
				name: "Patients with microbiologically +ve TB",
				value: 60,
			},
			{
				name: "Vouchers for FDC drugs issued",
				value: 40,
			},
			{
				name: "FDC drugs redeemed",
				value: 20,
			},
			{
				name: "Treatment outcome",
				value: 10,
			},
		];
		let series = chart.series.push(new am4charts.FunnelSeries());
		series.dataFields.value = "value";
		series.dataFields.category = "name";

		let fillModifier = new am4core.LinearGradientModifier();
		fillModifier.brightnesses = [-0.5, 1, -0.5];
		fillModifier.offsets = [0, 0.5, 1];
		series.slices.template.fillModifier = fillModifier;
		series.alignLabels = true;
		series.labels.template.text = "{category}: [bold]{value}[/]";
	}
	exportTable() { }
}
