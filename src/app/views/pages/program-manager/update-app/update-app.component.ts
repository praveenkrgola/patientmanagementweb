import { AbstractControl } from '@angular/forms';
import { UserLoginService } from './../../../../../services/user-login/user-login.service';
import { ESmsType } from './../../../../../models/sms.dto';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DesignationList } from '../../../../../models/user.dto';
import { MatOption } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'kt-update-app',
  templateUrl: './update-app.component.html',
  styleUrls: ['./update-app.component.scss']
})
export class UpdateAppComponent implements OnInit {

	smsType = ESmsType;
	@ViewChild('allRegistrationType', { static: true }) private allRegistrationType: MatOption;
	designationList = DesignationList.sort((a, b) => (a.designation > b.designation) ? 1 : -1);
	filterForm: FormGroup;
	updateForm: FormGroup;

  constructor(private userService: UserLoginService, private fb: FormBuilder) { }

	ngOnInit() {
		this.createForm();
	}

	createForm() {
		this.updateForm = this.fb.group({
			designation: [[], Validators.required],
		});
	}

	selectSingle() {
		if (this.allRegistrationType.selected) {
			this.allRegistrationType.deselect();
			return false;
		}
		if (this.updateForm.controls.designation.value.length == this.designationList.length)
			this.allRegistrationType.select();
	}

	selectAll() {
		if (this.allRegistrationType.selected) {
			this.updateForm.controls.designation.patchValue([0, ...this.designationList.map(type => type.value)]);
		} else {
			this.updateForm.controls.designation.patchValue([]);
		}
	}

	getControl(control): AbstractControl {
		return this.updateForm.get(control);
	}

	async sendSMS() {

		let finalObj = {
			...this.updateForm.value,
			smsType: this.smsType.appUpdate,
		}

		this.userService.sendSmsToDesignations(finalObj).subscribe(x => {
			const Toast = Swal.mixin({
				toast: true,
				position: "top-end",
				showConfirmButton: false,
				timer: 4000,
				timerProgressBar: true,
			});
			Toast.fire({
				icon: "success",
				title: `SMS has been sent successfully !!`
			});

			this.updateForm.controls.designation.patchValue([]);
		}, err => {
			console.log(err);
		});
  	}

}
