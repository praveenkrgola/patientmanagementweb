import { Component, OnInit, Input } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4maps from "@amcharts/amcharts4/maps";
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { GISService } from '../../../../../services/GIS/gis.service';
import { HttpClient } from '@angular/common/http';
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'kt-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {


  @Input() mapChardDiv;

  constructor(private gisService: GISService, private http: HttpClient) { }


  ngOnInit() {
      const filter = {
        type: "subdistrict",
        states: ["Chhattisgarh"],
        districts: ["Durg"],
        subdistricts :  ["Durg", "Dhamdha", "Patan"]
      };
      this.gisService.getSubDistricts(filter).subscribe(res => {
        console.log("Data Len : ", res);
        this.generateMap(res, "Durg");
      });
  }

  data = [];
  subDistrictData = [];
  generateMap(res, stateName) {

    let chart = am4core.create(this.mapChardDiv, am4maps.MapChart);
    //set chart data initially [];
    chart.geodata = {};
    this.data = [];

    if (this.mapChardDiv === "arunachalPradesgDivId") {
      chart.colors.list = [am4core.color("#FF5722")];
    } else if (this.mapChardDiv === "sikkimDivId") {
      chart.colors.list = [am4core.color("#ff5050")];
    } else if (this.mapChardDiv === "manipurDivId") {
      chart.colors.list = [am4core.color("#4da6ff")];
    }else if (this.mapChardDiv === "durgMapDiv") {
      chart.colors.list = [am4core.color("#4da6dd")];
    }


    chart.projection = new am4maps.projections.Miller();
    //configureing mouse event
    chart.chartContainer.wheelable = false;


    chart.geodata = res[0];
    let districtSeries = chart.series.push(new am4maps.MapPolygonSeries());


    for (var i = 0; i < res[0].features.length; i++) {
      this.data.push({
        ...res[0].features[i].properties,
        id: res[0].features[i].id,
        value: Math.round((Math.random() * (20 - -10))+ 10) //Between -10 to 20
      })
    }
    districtSeries.data = this.data;
    districtSeries.useGeodata = true;

    let districtPolygon = districtSeries.mapPolygons.template;
    districtPolygon.tooltipText = `Block Name: {name} \n Indicator Value: {value}` //this.isShowSubDistrict ? "District Name : {districtName}\nSub District Name: {name} \n Indicator Value: {value}" : "District Name: {name} \n Indicator Value: {value}";
    districtPolygon.fill = chart.colors.getIndex(0);
    districtPolygon.nonScalingStroke = true;
    chart.homeZoomLevel = 1;
    // Hover state
    let hs = districtPolygon.states.create("hover");
    districtSeries.heatRules.push({
      property: "fill",
      target: districtSeries.mapPolygons.template,
      min: chart.colors.getIndex(1).brighten(2.5),
      max: chart.colors.getIndex(1).brighten(0)
    });
    // Set up click events
    districtPolygon.events.on("hit", (ev) => {
      let dataContext = ev.target.dataItem.dataContext;
      ev.target.series.chart.zoomToMapObject(ev.target);
    });


    // Set up heat legend
    let heatLegend = chart.createChild(am4maps.HeatLegend);
    heatLegend.series = districtSeries;
    heatLegend.align = "right";
    heatLegend.valign = "top";
    heatLegend.width = am4core.percent(20);
    heatLegend.marginRight = am4core.percent(4);
    heatLegend.minValue = 10;
    heatLegend.maxValue = 50;

    // console.log(this.mapChardDiv + " : " + heatLegend.minValue + " : " + heatLegend.maxValue);

    // Set up custom heat map legend labels using axis ranges
    let minRange = heatLegend.valueAxis.axisRanges.create();
    minRange.value = heatLegend.minValue;
    minRange.label.text = heatLegend.minValue + "";//"Little";
    let maxRange = heatLegend.valueAxis.axisRanges.create();
    maxRange.value = heatLegend.maxValue;
    maxRange.label.text = heatLegend.maxValue + "";//"A lot!";

    // Blank out internal heat legend value axis labels
    heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function (labelText) {
      return "";
    });

    // Zoom control
    chart.zoomControl = new am4maps.ZoomControl();


    let homeButton = new am4core.Button();
    homeButton.events.on("hit", () => {
      districtSeries.show();
      chart.geodata = res[0];
      chart.goHome();
    });



    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.insertBefore(chart.zoomControl.plusButton);


    //this.charts.push(chart)
  }


}
