// Angular
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
// Core Module
import { CoreModule } from "../../../core/core.module";
import { PartialsModule } from "../../partials/partials.module";
import { DashboardComponent } from "./dashboard.component";
import { FormsModule } from "@angular/forms";
import { MatDividerModule } from "@angular/material";
@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		FormsModule,
		MatDividerModule,
		//ReportsModule,
		RouterModule.forChild([
			{
				path: "",
				component: DashboardComponent,
			},
		]),
	],
	entryComponents: [],
	providers: [],
	declarations: [DashboardComponent],
})
export class DashboardModule {}
