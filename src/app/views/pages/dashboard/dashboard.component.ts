// Angular
import {
	Component,
	OnInit,
	OnDestroy,
	ViewChild,
	ChangeDetectorRef,
} from "@angular/core";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { DataList } from "../../../util/data-list";
import objectPath from "object-path";
import * as XLSX from "xlsx";
import * as moment from 'moment';

// Services
@Component({
	selector: "kt-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
	user = JSON.parse(sessionStorage.getItem("user"));
	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
	) { }
	ngOnInit(): void {

	}

}
