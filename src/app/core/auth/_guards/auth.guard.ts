// Angular
import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	RouterModule,
	UrlTree,
} from "@angular/router";
// RxJS
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { UserLoginService } from "../../../../services/user-login/user-login.service";

@Injectable()
export class AuthGuard implements CanActivate {
	user = JSON.parse(sessionStorage.getItem("user"));

	constructor(
		private router: Router,
		private userLoginService: UserLoginService
	) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		if (this.user) {
			return true;
		} else {
			this.router.navigate(["/auth/login"]);
			this.userLoginService.logout();
			return false;
		}

		// return this.userLoginService.userPs$.pipe(
		// 	map((user) => {
		// 		// console.log('user..authGuard...', user);

		// 		const isAuth = !!user;
		// 		console.log("Hello : ",isAuth);

		// 		if (isAuth) {
		// 			return true;
		// 		}
		// 		return this.router.createUrlTree(["/auth/login"]);
		// 	})
		// );
	}
}
