export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				// {
				// 	title: "WTC-Reports",
				// 	root: true,
				// 	alignment: "left",
				// 	toggle: "click",
				// 	submenu: [
				// 		{
				// 			title: "Referral List",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/verification",
				// 		},
				// 		{
				// 			title: "Alumni Transfer List",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/alumniTransferList",
				// 		},
				// 		{
				// 			title: "Bank Details",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/bankdetails",
				// 		},
				// 		{
				// 			title: "Assesment Status",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/assesmentstatus",
				// 		},
				// 		{
				// 			title: "Assesment Details",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/assesment",
				// 		},
				// 		{
				// 			title: "Health Checkup List",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/doctor-voucher-prescription",
				// 		}
				// 	],
				// },
				// {
				// 	title: "WTC-Master",
				// 	root: true,
				// 	alignment: "left",
				// 	toggle: "click",
				// 	submenu: [
				// 		{
				// 			title: "Add Alumni",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/user",
				// 		},
				// 		{
				// 			title: "Add Placement Agency",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/placement-agency",
				// 		},{
				// 			title: "Send Messages",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/sendmessage",
				// 		},
				// 		{
				// 			title: "Send Mail For Placement",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/placement-declaration",
				// 		},
				// 		{
				// 			title: "Student List",
				// 			bullet: "dot",
				// 			icon: "flaticon-web",
				// 			page: "/reports/student-list",
				// 		}
				// 	],
				// },
				// {
				// 	title: "Reports",
				// 	root: true,
				// 	alignment: "left",
				// 	toggle: "click",
				// 	submenu: [
				// 		{
				// 			title: "Report 1",
				// 			bullet: "dot",
				// 			icon: "flaticon-business",
				// 			permission: "accessToECommerceModule",
				// 			// submenu: [
				// 			// 	{
				// 			// 		title: 'Customers',
				// 			// 		page: '/ecommerce/customers'
				// 			// 	},
				// 			// 	{
				// 			// 		title: 'Products',
				// 			// 		page: '/ecommerce/products'
				// 			// 	},
				// 			// ]
				// 		},
				// 		{
				// 			title: "Report 2",
				// 			bullet: "dot",
				// 			icon: "flaticon-user",
				// 			// submenu: [
				// 			// 	{
				// 			// 		title: 'Users',
				// 			// 		page: '/user-management/users'
				// 			// 	},
				// 			// 	{
				// 			// 		title: 'Roles',
				// 			// 		page: '/user-management/roles'
				// 			// 	}
				// 			// ]
				// 		},
				// 	],
				// },
				// {
				// 	title: 'Custom',
				// 	root: true,
				// 	alignment: 'left',
				// 	toggle: 'click',
				// 	submenu: [
				// 		{
				// 			title: 'Error Pages',
				// 			bullet: 'dot',
				// 			icon: 'flaticon2-list-2',
				// 			submenu: [
				// 				{
				// 					title: 'Error 1',
				// 					page: '/error/error-v1'
				// 				},
				// 				{
				// 					title: 'Error 2',
				// 					page: '/error/error-v2'
				// 				},
				// 				{
				// 					title: 'Error 3',
				// 					page: '/error/error-v3'
				// 				},
				// 				{
				// 					title: 'Error 4',
				// 					page: '/error/error-v4'
				// 				},
				// 				{
				// 					title: 'Error 5',
				// 					page: '/error/error-v5'
				// 				},
				// 				{
				// 					title: 'Error 6',
				// 					page: '/error/error-v6'
				// 				},
				// 			]
				// 		},
				// 		{
				// 			title: 'Wizard',
				// 			bullet: 'dot',
				// 			icon: 'flaticon2-mail-1',
				// 			submenu: [
				// 				{
				// 					title: 'Wizard 1',
				// 					page: '/wizard/wizard-1'
				// 				},
				// 				{
				// 					title: 'Wizard 2',
				// 					page: '/wizard/wizard-2'
				// 				},
				// 				{
				// 					title: 'Wizard 3',
				// 					page: '/wizard/wizard-3'
				// 				},
				// 				{
				// 					title: 'Wizard 4',
				// 					page: '/wizard/wizard-4'
				// 				},
				// 			]
				// 		},
				// 	]
				// },
			],
		},
		aside: {
			self: {},
			items: [
			],
		},
	};

	public get configs(): any {
		const user = JSON.parse(sessionStorage.getItem("user"));		
		if (user.userAccess.length > 0) {
			this.defaults.header.items = user.userAccess[0].items;
		}
		return this.defaults;
	}
}
