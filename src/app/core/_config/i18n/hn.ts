// Hindi
export const locale = {
	lang: 'hn',
	data: {
		TRANSLATOR: {
			SELECT: 'अपनी भाषा का चयन करें',
		},
		MENU: {
			NEW: 'नया',
			ACTIONS: 'क्रिया',
			CREATE_POST: 'नई पोस्ट बनाएँ',
			PAGES: 'पेज',
			FEATURES: 'विशेषताएं',
			APPS: 'ऐप्स',
			DASHBOARD: 'डैशबोर्ड',
		},
		AUTH: {
			GENERAL: {
				OR: 'या',
				SUBMIT_BUTTON: 'प्रस्तुत',
				NO_ACCOUNT: 'खाता नहीं है?',
				SIGNUP_BUTTON: 'साइन अप करें',
				FORGOT_BUTTON: 'पासवर्ड भूल गए',
				BACK_BUTTON: 'वापस',
				PRIVACY: 'एकांत',
				LEGAL: 'कानूनी',
				CONTACT: 'संपर्क करें',
				ABOUT :'हमारे बारे में',
				TEAM : 'टीम',
				HELLO:'हैलो',
				SIGNOUT_BUTTON :'साइन आउट करें',
				NEED_ASSISTANCE : 'सहायता की जरूरत है',
				LABEL_LOGIN_BY_MANAGEMENT : 'प्रबंधन द्वारा लॉगिन करें',
				LABEL_LOGIN_BY_MOBILE : 'मोबाइल से लॉगइन करें',
				LABEL_GENERATE_OTP_BUTTON : 'ओटीपी जनरेट करें'
			},
			LOGIN: {
				TITLE: 'लॉगिन खाता',
				BUTTON: 'साइन इन करें',
				SIGNIN_LABEL : 'साइन इन'
			},
			FORGOT: {
				TITLE: 'पासवर्ड भूल गए?',
				DESC: 'अपना पासवर्ड रीसेट करने के लिए अपना ईमेल प्रविष्ट करें',
				SUCCESS: 'आपका खाता सफलतापूर्वक रीसेट कर दिया गया है।'
			},
			REGISTER: {
				TITLE: 'साइन अप करें',
				DESC: 'अपना खाता बनाने के लिए अपना विवरण दर्ज करें',
				SUCCESS: 'आपका खाता सफलतापूर्वक पंजीकृत हो गया है।'
			},
			INPUT: {
				EMAIL: 'ईमेल',
				FULLNAME: 'पूरा नाम',
				PASSWORD: 'कुंजिका',
				PASSWORD_PLACEHOLDER : 'पास वर्ड दर्ज करें',
				CONFIRM_PASSWORD: 'पासवर्ड की पुष्टि कीजिये',
				USERNAME: 'उपयोगकर्ता नाम',
				USERNAME_PLACEHOLDER : 'उपयोगकर्ता नाम दर्ज करें',
				MOBILE:'मोबाइल',
				MOBILE_PLACEHOLDER : 'मोबाइल नंबर दर्ज करें',
				OTP:'ओ.टी.पी.',
				OTP_PLACEHOLDER : 'ओ.टी.पी. दर्ज करें'
			},
			VALIDATION: {
				INVALID: '{{name}} मान्य नहीं है',
				REQUIRED: '{{name}} आवश्यक है',
				MIN_LENGTH: '{{name}} न्यूनतम लंबाई है {{min}}',
				AGREEMENT_REQUIRED: 'शर्तें स्वीकार कर रहा है',
				NOT_FOUND: 'अनुरोध किया {{name}} नहीं मिला',
				INVALID_LOGIN: 'लॉगिन विवरण गलत है',
				REQUIRED_FIELD: 'आवश्यक क्षेत्र',
				MIN_LENGTH_FIELD: 'न्यूनतम फ़ील्ड लंबाई:',
				MAX_LENGTH_FIELD: 'अधिकतम फ़ील्ड लंबाई:',
				INVALID_FIELD: 'फ़ील्ड मान्य नहीं है',
				MOBILE_NUMBER_FIELD : '10 डिजिट का मोबाइल नंबर डालें।',
				Digit_6_OTP : '6 अंको का ओ.टी.पी. दर्ज़ करे'
			}
		},
		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Selected records count: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'eCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Customers',
				CUSTOMERS_LIST: 'Customers list',
				NEW_CUSTOMER: 'New Customer',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Customer Delete',
					DESCRIPTION: 'Are you sure to permanently delete this customer?',
					WAIT_DESCRIPTION: 'Customer is deleting...',
					MESSAGE: 'Customer has been deleted'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Customers Delete',
					DESCRIPTION: 'Are you sure to permanently delete selected customers?',
					WAIT_DESCRIPTION: 'Customers are deleting...',
					MESSAGE: 'Selected customers have been deleted'
				},
				UPDATE_STATUS: {
					TITLE: 'Status has been updated for selected customers',
					MESSAGE: 'Selected customers status have successfully been updated'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Customer has been updated',
					ADD_MESSAGE: 'Customer has been created'
				}
			}
		}
	}
};
