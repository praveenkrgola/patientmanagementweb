// Marathi
export const locale = {
	lang: 'mr',
	data: {
		TRANSLATOR: {
			SELECT: 'आपली भाषा निवडा',
		},
		MENU: {
			NEW: 'नवीन',
			ACTIONS: 'क्रिया',
			CREATE_POST: 'नवीन पोस्ट तयार करा',
			PAGES: 'पृष्ठे',
			FEATURES: 'वैशिष्ट्ये',
			APPS: 'अ‍ॅप्स',
			DASHBOARD: 'डॅशबोर्ड',
		},
		AUTH: {
			GENERAL: {
				OR: 'किंवा',
				SUBMIT_BUTTON: 'प्रस्तुत करणे',
				NO_ACCOUNT: 'खाते नाही?',
				SIGNUP_BUTTON: 'साइन अप करा',
				FORGOT_BUTTON: 'संकेतशब्द विसरलात?',
				BACK_BUTTON: 'मागे',
				PRIVACY: 'गोपनीयता',
				LEGAL: 'कायदेशीर',
				CONTACT: 'संपर्क',
				ABOUT: 'आमच्याबद्दल',
				TEAM: 'संघ',
				HELLO: 'नमस्कार',
				SIGNOUT_BUTTON: 'साइन आउट करा',
				NEED_ASSISTANCE: 'सहाय्य पाहिजे',
				LABEL_LOGIN_BY_MANAGEMENT: 'व्यवस्थापनाद्वारे लॉगिन करा',
				LABEL_LOGIN_BY_MOBILE: 'मोबाइलद्वारे लॉगिन करा',
				LABEL_GENERATE_OTP_BUTTON : 'ओटीपी व्युत्पन्न करा'
			},
			LOGIN: {
				TITLE: 'लॉगिन खाते',
				BUTTON: 'साइन इन करा',
				SIGNIN_LABEL : 'साइन इन'
			},
			FORGOT: {
				TITLE: 'विसरलेला संकेतशब्द?',
				DESC: 'आपला संकेतशब्द रीसेट करण्यासाठी आपला ईमेल प्रविष्ट करा',
				SUCCESS: 'आपले खाते यशस्वीरित्या रीसेट केले गेले आहे.'
			},
			REGISTER: {
				TITLE: 'साइन अप करा',
				DESC: 'आपले खाते तयार करण्यासाठी आपले तपशील प्रविष्ट करा',
				SUCCESS: 'आपले खाते यशस्वीरित्या नोंदणीकृत झाले आहे.'
			},
			INPUT: {
				EMAIL: 'ईमेल',
				FULLNAME: 'पूर्ण नाव',
				PASSWORD: 'संकेतशब्द',
				PASSWORD_PLACEHOLDER: 'पासवर्ड टाका',
				CONFIRM_PASSWORD: 'पासवर्डची पुष्टी करा',
				USERNAME: 'वापरकर्तानाव',
				USERNAME_PLACEHOLDER: 'वापरकर्तानाव प्रविष्ट करा',
				MOBILE: 'मोबाईल',
				MOBILE_PLACEHOLDER: 'मोबाईल क्रमांक प्रविष्ट करा',
				OTP: 'ओ.टी.पी.',
				OTP_PLACEHOLDER: 'ओ.टी.पी. प्रविष्ट करा'
			},
			VALIDATION: {
				INVALID: '{{name}} वैध नाही',
				REQUIRED: '{{name}} आवश्यक आहे',
				MIN_LENGTH: '{{name}} किमान लांबी आहे {{min}}',
				AGREEMENT_REQUIRED: 'अटी स्वीकारत आहे',
				NOT_FOUND: 'विनंती केली {{name}} सापडत नाही',
				INVALID_LOGIN: 'लॉगिन तपशील चुकीचा आहे',
				REQUIRED_FIELD: 'आवश्यक फील्ड',
				MIN_LENGTH_FIELD: 'किमान फील्ड लांबी:',
				MAX_LENGTH_FIELD: 'कमाल फील्ड लांबी:',
				INVALID_FIELD: 'फील्ड वैध नाही',
				MOBILE_NUMBER_FIELD: '10 अंकी मोबाइल क्रमांक प्रविष्ट करा.',
				Digit_6_OTP : '6 अंकी ओ.टी.पी. प्रविष्ट करा'
			}
		},
		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Selected records count: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'eCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Customers',
				CUSTOMERS_LIST: 'Customers list',
				NEW_CUSTOMER: 'New Customer',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Customer Delete',
					DESCRIPTION: 'Are you sure to permanently delete this customer?',
					WAIT_DESCRIPTION: 'Customer is deleting...',
					MESSAGE: 'Customer has been deleted'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Customers Delete',
					DESCRIPTION: 'Are you sure to permanently delete selected customers?',
					WAIT_DESCRIPTION: 'Customers are deleting...',
					MESSAGE: 'Selected customers have been deleted'
				},
				UPDATE_STATUS: {
					TITLE: 'Status has been updated for selected customers',
					MESSAGE: 'Selected customers status have successfully been updated'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Customer has been updated',
					ADD_MESSAGE: 'Customer has been created'
				}
			}
		}
	}
};
