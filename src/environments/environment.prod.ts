export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done 
	authTokenKey: "authce9d77b308c149d5992a80073637e4d5",
	apiEndPoint: "https://chaitbapi.espldemo.com/api/", // production API
	//apiEndPoint: "https://stagingchaitbapi.espldemo.com/api/", // staging API
};
