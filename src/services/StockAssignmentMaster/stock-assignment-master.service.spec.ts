import { TestBed } from '@angular/core/testing';

import { StockAssignmentMasterService } from './stock-assignment-master.service';

describe('StockAssignmentMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockAssignmentMasterService = TestBed.get(StockAssignmentMasterService);
    expect(service).toBeTruthy();
  });
});
