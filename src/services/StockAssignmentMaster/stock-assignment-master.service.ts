import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class StockAssignmentMasterService {

  private user = JSON.parse(sessionStorage.getItem("user"));
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.token
  });
  get token(): string {
    if (this.user) {
      return this.user.accessToken.id;
    }
    return "";
  }

  addProduct(object: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}`;
    return this.http.post(url, object, { headers: this.headers });
  }

  updateProduct(where: object, data: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}/update?where=${JSON.stringify(where)}`;
    return this.http.post(url, data, { headers: this.headers });
  }

  getStockDetails(data: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}/getStockDetails`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getList(filter: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}?filter=${JSON.stringify(filter)}`;
    return this.http.get(url, { headers: this.headers });
  }
  getCount(where: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}/count?where=${JSON.stringify(where)}`;
    return this.http.get(url, { headers: this.headers });
  }
  SubmitOrUpdateStockUserWise(data: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}/SubmitOrUpdateStockUserWise`;
    return this.http.post(url, data, { headers: this.headers });
  }
  DeductOrUpdateStockUserWise(data: object): Observable<any> {
    const url = `${api.StockAssignmentMaster.common}/DeductOrUpdateStockUserWise`;
    return this.http.post(url, data, { headers: this.headers });
  }
}
