import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUserRegistration } from '../../models/sms.dto';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class SmsService {

	constructor(private http: HttpClient ) { }

	userRegistrationSms(data: string[]): Observable<string> {
		const url = api.SMS.userRegister;
		return this.http.post<string>(`${url}`, data);
	}

	getSMSData(where: any): Observable<any> {
		const url = `${api.SMS.common}?filter=${JSON.stringify(where)}`;
		return this.http.get<any>(url);
	}
}
