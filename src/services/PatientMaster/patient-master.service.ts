import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { api } from "../util/api";

@Injectable({
  providedIn: 'root'
})
export class PatientMasterService {

  private user = JSON.parse(sessionStorage.getItem("user"));
	constructor(private http: HttpClient) {}
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.token,
	});
	get token(): string {
		if (this.user) {
			return this.user.accessToken.id;
		}
		return "";
  }
  
  getEnrollmentDataForDashboard(parameter: object): Observable<any> {
		const url = `${api.PatientMaster.common}/getEnrollmentDataForDashboard`;
		return this.http.post(url, parameter, { headers: this.headers });
	}

	getEnrollmentByUserTypeForDashboard(parameter: object): Observable<any> {
		const url = `${api.PatientMaster.common}/getEnrollmentByUserTypeForDashboard`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getMonthOnMonthEnrollmentViewDashboard(parameter: object): Observable<any> {
		const url = `${api.PatientMaster.common}/getMonthOnMonthEnrollmentViewDashboard`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getDirectNotificationRegister(parameter: object): Observable<any> {
		const url = `${api.PatientMaster.common}/getDirectNotificationRegister`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getFDCRegister(parameter: object): Observable<any> {
		const url = `${api.PatientMaster.common}/getFDCRegister`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
}
