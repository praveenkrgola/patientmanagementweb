import { TestBed } from '@angular/core/testing';

import { PatientMasterService } from './patient-master.service';

describe('PatientMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PatientMasterService = TestBed.get(PatientMasterService);
    expect(service).toBeTruthy();
  });
});
