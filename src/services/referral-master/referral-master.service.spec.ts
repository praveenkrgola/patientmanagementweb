import { TestBed } from '@angular/core/testing';

import { ReferralMasterService } from './referral-master.service';

describe('ReferralMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReferralMasterService = TestBed.get(ReferralMasterService);
    expect(service).toBeTruthy();
  });
});
