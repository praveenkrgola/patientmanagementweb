import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { api } from "../util/api";

@Injectable({
	providedIn: "root",
})
export class ReferralMasterService {
	private user = JSON.parse(sessionStorage.getItem("user"));
	constructor(private http: HttpClient) { }
	headers = new HttpHeaders({
		"Content-Type": "application/json; charset=utf-8",
		Authorization: this.token,
	});
	get token(): string {
		if (this.user) {
			return this.user.accessToken.id;
		}
		return "";
	}

	getReferralData(filter: object): Observable<any> {
		const url = `${api.ReferralMasters.common}?filter=${JSON.stringify(filter)}`;
		return this.http.get(url, { headers: this.headers });
	}
	getCXRReport(data): Observable<any> {
		const url = `${api.ReferralMasters.cxrReport}`;
		return this.http.post(url, data);
	}
	getCBNAATSCTReport(data): Observable<any> {
		const url = `${api.ReferralMasters.CBNAATSCTReport}`;
		return this.http.post(url, data);
	}

	updateReferralData(where: object, data: object): Observable<any> {
		const url = `${api.ReferralMasters.common}/update?where=${JSON.stringify(
			where
		)}`;
		return this.http.post(url, data, { headers: this.headers });
	}

	getDoctorConsultationRegister(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.common}/getDoctorConsultationRegister`;
		return this.http.post(url, parameter, { headers: this.headers });
	}

	getConfirmTBRegister(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.common}/getConfirmTBRegister`;
		return this.http.post(url, parameter, { headers: this.headers });
	}

	getConfirmationTypeForDashboard(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.common}/getConfirmationTypeForDashboard`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getProviderViewDashboardData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.providerViewDashboard}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	issuedRedeemedRatioGraphsForProviderViewDashboard(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.issuedClaimedRatioGraphsForProviderViewDashboard}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	issuesEnrolledRatioGraphsForProviderViewDashboard(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.issuedEnrolledRatioGraphsForProviderViewDashboard}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getVouchersDataMonthWiseForProviderViewDashboard(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getVouchersDataMonthWiseForProviderViewDashboard}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetEnrollmentViewAndTile(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetEnrollmentViewAndTile}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getDataOfTilesForProviderViewDashboard(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getDataOfTilesForProviderViewDashboard}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}

	getEnrollmentViewDateWise(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetEnrollmentViewDateWise}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetEnrollmentByUserType(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetEnrollmentByUserType}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetEnrollmentByUserTypeDateWise(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetEnrollmentByUserTypeDateWise}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetConfirmationType(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetConfirmationType}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetConfirmationTypeDateWise(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetConfirmationTypeDateWise}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetTilesDataForSCTView(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetTilesDataForSCTView}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetTableDataForSCTView(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetTableDataForSCTView}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	GetMTB_Rif_StatusGraphForSCTView(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetMTB_Rif_StatusGraphForSCTView}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getChemistData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getChemistData}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getEmpanelledChemistData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getEmpanelledChemistData}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getXrayLabData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getXrayLabData}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getFormalProviderData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getFormalProviderData}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getDirectNotification(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetDirectNotification}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}
	getNotificationByUserType(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.GetNotificationByUserType}`;
		return this.http.post(url, parameter, { headers: this.headers });
	}

	getCompletePatientDetail(parameter: object):Promise<any> {
		const url = `${api.ReferralMasters.getCompletePatientDetail}`;
		return this.http.post(url, parameter, { headers: this.headers }).toPromise();
	}

	getAyushProviderData(parameter: object): Observable<any> {
		const url = `${api.ReferralMasters.getAyushProviderData}`;
		return this.http.post(url, parameter, {headers: this.headers})
	}
	
}

