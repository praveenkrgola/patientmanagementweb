import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';
@Injectable({
  providedIn: 'root'
})
export class BlockService {

  constructor(private http: HttpClient) { }

  getBlocks(filter: object): Observable<any> {
    const url = `${api.Blocks.common}?filter=${JSON.stringify(filter)}`;
    return this.http.get(url);
  }
}
