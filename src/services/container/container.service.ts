import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
	providedIn: 'root'
})
export class ContainerService {

	private user = JSON.parse(sessionStorage.getItem("user"));
	constructor(private http: HttpClient) { }
	headers = new HttpHeaders({
		'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundarymlBIESPbpE4pAaeD',
		'Authorization': this.token
	});
	get token(): string {
		if (this.user) {
			return this.user.accessToken.id;
		}
		return "";
	}

	uploadFile(
		containerName: string,
		apiCallingFor: string,
		fileUpload: File
	) {
		console.log(apiCallingFor, fileUpload);

		const formData: FormData = new FormData();
		formData.append("image", fileUpload, fileUpload.name);
		formData.append("apiCallingFor", apiCallingFor);

		const url = api.containers.common + "/" + containerName + "/upload";
		return this.http.post(url, formData, {
			reportProgress: true,
			observe: "events",
		});
	}

	downloadFile(containerName: string, imageName: string) {
		let url = api.containers.common + "/" + containerName + "/download/" + imageName;
		return this.http.get(url, { responseType: "blob" });
	}
}
