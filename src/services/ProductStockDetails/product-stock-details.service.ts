import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class ProductStockDetailsService {
  private user = JSON.parse(sessionStorage.getItem("user"));

  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.token
  });
  get token(): string {
    if (this.user) {
      return this.user.accessToken.id;
    }
    return "";
  }
  addStock(object: object): Observable<any> {
    const url = `${api.ProductStockDetails.common}`;
    return this.http.post(url, object, { headers: this.headers });
  }
}
