import { TestBed } from '@angular/core/testing';

import { ProductStockDetailsService } from './product-stock-details.service';

describe('ProductStockDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductStockDetailsService = TestBed.get(ProductStockDetailsService);
    expect(service).toBeTruthy();
  });
});
