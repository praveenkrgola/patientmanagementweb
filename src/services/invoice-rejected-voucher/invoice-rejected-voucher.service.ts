import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ErrorHandlerService } from '../../../src/app/util/error-handler/error-handler.service';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class InvoiceRejectedVoucherService {

  constructor(
    private _http: HttpClient,
    private errorHandler: ErrorHandlerService,
  ) { }

  postRejectedVoucher(data:any) {
    const url = `${api.InvoiceRejectedVouchers.common}`;
    return this._http.post(url, data);
  }
  
  getRejectedVoucherList(param: any) {
    const url = `${api.InvoiceRejectedVouchers.getRejectedVoucherList}`;
    return this._http.post(url, param);
  }
  
  getRejectedVoucherDetails(filter:any) {
    const url = `${api.InvoiceRejectedVouchers.common}?filter=${JSON.stringify(filter)}`
    return this._http.get(url);
  }

  updateData(where: any, data: any) {
		const url = `${api.InvoiceRejectedVouchers.update}?where=${JSON.stringify(where)}`
		return this._http.post<any>(url, data).pipe(
			tap(
				() => { },
				(error) => {
					this.errorHandler.error(error);
				}
			)
		);
	}
}
