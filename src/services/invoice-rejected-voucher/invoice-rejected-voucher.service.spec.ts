import { TestBed } from '@angular/core/testing';

import { InvoiceRejectedVoucherService } from './invoice-rejected-voucher.service';

describe('InvoiceRejectedVoucherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoiceRejectedVoucherService = TestBed.get(InvoiceRejectedVoucherService);
    expect(service).toBeTruthy();
  });
});
