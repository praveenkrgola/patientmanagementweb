import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  private user = JSON.parse(sessionStorage.getItem("user"));
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({
		'Content-Type': 'application/json; charset=utf-8',
		'Authorization': this.token
  });
  get token(): string {
    if (this.user) {
      return this.user.accessToken.id;
    }
    return "";
  }

  get(filter: object): Observable<any>{
    const url = `${api.Inventory.common}?filter=${JSON.stringify(filter)}`;
    return this.http.get(url, {headers:this.headers});
  }
  post(data: object): Observable<any>{
    const url = `${api.Inventory.common}`;
    return this.http.post(url,data, {headers:this.headers});
  }
}
