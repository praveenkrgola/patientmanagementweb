import { TestBed } from '@angular/core/testing';

import { FieldOfficerActivityService } from './field-officer-activity.service';

describe('FieldOfficerActivityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FieldOfficerActivityService = TestBed.get(FieldOfficerActivityService);
    expect(service).toBeTruthy();
  });
});
