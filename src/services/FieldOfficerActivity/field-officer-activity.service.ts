import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class FieldOfficerActivityService {

  
  private user = JSON.parse(sessionStorage.getItem("user"));
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': this.token
  });
  get token(): string {
    if (this.user) {
      return this.user.accessToken.id;
    }
    return "";
  }

  getFOActivity(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.common}/getFOActivity`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getDataByUserTypeForFOViewDashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.getDataByUserTypeForFOViewDashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getMonthWiseDataByUserTypeForFOViewDashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.getMonthWiseDataByUserTypeForFOViewDashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getMonthWiseConsultationByUserTypeForFODashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.getMonthWiseConsultationByUserTypeForFODashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getConsultationByUserTypeForFODashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.getConsultationByUserTypeForFODashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  getTilesDataForFODashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.getTilesDataForFODashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  visitDataTableForFODashboard(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.visitDataTableForFODashboard}`;
    return this.http.post(url, data, { headers: this.headers });
  }
  servicePerformanceDatatable(data: object): Observable<any> {
    const url = `${api.FieldOfficerActivity.servicePerformanceDatatable}`;
    return this.http.post(url, data, { headers: this.headers });
  }

}
