import { Injectable } from "@angular/core";
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpHeaders,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { UserLoginService } from "../../../services/user-login/user-login.service";
import { take, exhaustMap } from "rxjs/operators";

@Injectable({
	providedIn: "root",
})
export class AuthTokenInterceptor implements HttpInterceptor {
	user = JSON.parse(sessionStorage.getItem("user"));

	constructor(private userService: UserLoginService) {}
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		if (!this.user) {
			return next.handle(request);
		}
		// let Authorization = this.user.accessToken.id;
		// let headers = new HttpHeaders({
		// 	// "Content-Type": "application/json; charset=utf-8",
		// 	Authorization: Authorization,
		// });
		const modifiedRequest = request.clone();
		return next.handle(modifiedRequest);
		// return this.userService.userPs$.pipe(
		// 	take(1),
		// 	exhaustMap((user) => {
		// 		if (!user) {
		// 			return next.handle(request);
		// 		}
		// 		let access_token = user.id;
		// 		const modifiedRequest = request.clone({
		// 			setHeaders: { access_token },
		// 		});
		// 		return next.handle(modifiedRequest);
		// 	})
		// );
	}
}
