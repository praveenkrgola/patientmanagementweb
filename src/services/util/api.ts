import { environment } from "../../environments/environment";

export const apiEndPoint = environment.apiEndPoint;

class UserLogin {
	readonly common = `${apiEndPoint}UserLogins`;
	readonly loginByMobile = `${this.common}/loginByMobile`;
	readonly login = `${this.common}/login`;
	readonly byId = (id: string) => `${this.common}/${id}`;
	readonly update = `${this.common}/update`;
	readonly sendSmsToDesignations = `${this.common}/sendSMSToDesignations`;
}

class Sms {
	userLoginData = new UserLogin();
	readonly userRegister = `${this.userLoginData.common}/sendSMS`;
	readonly common = `${apiEndPoint}SMs`;
}


class LabelMasters {
	readonly common = `${apiEndPoint}LabelMasters`;
}

class Containers {
	readonly common = `${apiEndPoint}containers`;
}

class Message {
	readonly common = `${apiEndPoint}Messages`;
}

// class AccountMasters {
// 	readonly common = `${apiEndPoint}AccountMasters`;
// 	readonly byId = (alumniId: string) => `${this.common}/${alumniId}`;
// }

class ListMasters {
	readonly common = `${apiEndPoint}ListMasters`;
	readonly getLists = `${this.common}/getLists`
}

class Assesment {
	readonly common = `${apiEndPoint}Assesments`;
	readonly getAssesmnetAndAnswerStatus = `${this.common}/getAssesmnetAndAnswerStatus`;
	readonly getAssesmnetAndAnswerStatusForYes_Dashboard = `${this.common}/getAssesmnetAndAnswerStatusForYes_Dashboard`;
	readonly getAssesmnetAndAnswerStatusForNo_Dashboard = `${this.common}/getAssesmnetAndAnswerStatusForNo_Dashboard`;

}
class Answer {
	readonly common = `${apiEndPoint}Answers`;
	readonly getAnswerSummary = `${this.common}/getAnswerSummary`;


}

class State {
	readonly common = `${apiEndPoint}States`;
}
class City {
	readonly common = `${apiEndPoint}Cities`;
}
class PlacementAgency {
	readonly common = `${apiEndPoint}PlacementAgencies`;
}
class MailSent {
	readonly common = `${apiEndPoint}SentMails`;
}
class Gis {
	readonly common = `${apiEndPoint}Gis`;
	readonly getStates = `${this.common}/getStates`;
	readonly getDistricts = `${this.common}/getDistricts`;
	readonly getSubDistricts = `${this.common}/getSubDistricts`
}

class ProductMaster {
	readonly common = `${apiEndPoint}ProductMasters`;
}

class Inventory {
	readonly common = `${apiEndPoint}Inventories`;
}
class InvoiceHistory {
	readonly common = `${apiEndPoint}InvoiceHistories`;
	readonly dashboardTiles = `${this.common}/dashboardTiles`;
	readonly amountDueByPaymentType = `${this.common}/amountDueByPaymentType`;
	readonly amountDueByUserType = `${this.common}/amountDueByUserType`;
	readonly invoiceNotRaisedByUserType = `${this.common}/invoiceNotRaisedByUserType`;
	readonly amountRejectedGroupByReason = `${this.common}/amountRejectedGroupByReason`;
}
class ReferralMasters {
	readonly common = `${apiEndPoint}ReferralMasters`;
	readonly cxrReport = `${this.common}/cxrReport`;
	readonly CBNAATSCTReport = `${this.common}/CBNAATSCTReport`;
	readonly providerViewDashboard = `${this.common}/providerViewDashboard`;
	readonly issuedClaimedRatioGraphsForProviderViewDashboard = `${this.common}/issuedClaimedRatioGraphsForProviderViewDashboard`;
	readonly issuedEnrolledRatioGraphsForProviderViewDashboard = `${this.common}/issuedEnrolledRatioGraphsForProviderViewDashboard`;
	readonly GetEnrollmentViewAndTile = `${this.common}/GetEnrollmentViewAndTile`;
	readonly getVouchersDataMonthWiseForProviderViewDashboard = `${this.common}/getVouchersDataMonthWiseForProviderViewDashboard`;
	readonly getDataOfTilesForProviderViewDashboard = `${this.common}/getDataOfTilesForProviderViewDashboard`;
	readonly GetEnrollmentViewDateWise = `${this.common}/GetEnrollmentViewDateWise`;
	readonly GetEnrollmentByUserType = `${this.common}/GetEnrollmentByUserType`;
	readonly GetEnrollmentByUserTypeDateWise = `${this.common}/GetEnrollmentByUserTypeDateWise`;
	readonly GetConfirmationType = `${this.common}/GetConfirmationType`;
	readonly GetConfirmationTypeDateWise = `${this.common}/GetConfirmationTypeDateWise`;
	readonly GetTilesDataForSCTView = `${this.common}/GetTilesDataForSCTView`;
	readonly GetTableDataForSCTView = `${this.common}/GetTableDataForSCTView`;
	readonly GetMTB_Rif_StatusGraphForSCTView = `${this.common}/GetMTB_Rif_StatusGraphForSCTView`;
	readonly getChemistData = `${this.common}/getChemistData`;
	readonly getEmpanelledChemistData = `${this.common}/getEmpanelledChemistData`;
	readonly getXrayLabData = `${this.common}/getXrayLabData`;
	readonly getFormalProviderData = `${this.common}/getFormalProviderData`;
	readonly GetDirectNotification = `${this.common}/GetDirectNotification`;
	readonly GetNotificationByUserType = `${this.common}/GetNotificationByUserType`
	readonly getCompletePatientDetail = `${this.common}/getCompletePatientDetail`
	readonly getAyushProviderData = `${this.common}/getAyushProviderData`

}
class InvoiceRejectedVouchers {
	readonly common = `${apiEndPoint}InvoiceRejectedVouchers`;
	readonly getRejectedVoucherList = `${this.common}/getRejectedVoucherList`;
	readonly update = `${this.common}/update`;
}
class ProductStockDetails {
	readonly common = `${apiEndPoint}ProductStockDetails`;
}
class StockAssignmentMaster {
	readonly common = `${apiEndPoint}StockAssignmentMasters`;
}
class StockAssignmentDetails {
	readonly common = `${apiEndPoint}StockAssignmentDetails`;
}

class FieldOfficerActivity {
	readonly common = `${apiEndPoint}FieldOfficerActivities`;
	readonly getDataByUserTypeForFOViewDashboard = `${this.common}/getDataByUserTypeForFOViewDashboard`;
	readonly getMonthWiseDataByUserTypeForFOViewDashboard = `${this.common}/getMonthWiseDataByUserTypeForFOViewDashboard`;
	readonly getMonthWiseConsultationByUserTypeForFODashboard = `${this.common}/getMonthWiseConsultationByUserTypeForFODashboard`;
	readonly getConsultationByUserTypeForFODashboard = `${this.common}/getConsultationByUserTypeForFODashboard`;
	readonly getTilesDataForFODashboard = `${this.common}/getTilesDataForFODashboard`;
	readonly visitDataTableForFODashboard = `${this.common}/visitDataTableForFODashboard`;
	readonly servicePerformanceDatatable = `${this.common}/servicePerformanceDatatable`;
}
class PatientMaster {
	readonly common = `${apiEndPoint}PatientMasters`;
}
class Blocks {
	readonly common = `${apiEndPoint}Blocks`;
}
class Api {
	readonly userLogin = new UserLogin();
	// readonly formInputDetails = new FormInputDetails();
	readonly labelMasters = new LabelMasters();
	readonly containers = new Containers();
	readonly message = new Message();
	// readonly accountMasters = new AccountMasters();
	readonly listMasters = new ListMasters();
	readonly assesment = new Assesment();
	readonly answer = new Answer();
	readonly state = new State();
	readonly city = new City();
	readonly PlacementAgency = new PlacementAgency();
	readonly MailSent = new MailSent();
	readonly gis = new Gis();
	readonly ProductMaster = new ProductMaster();
	readonly Inventory = new Inventory();
	readonly InvoiceHistory = new InvoiceHistory();
	readonly ReferralMasters = new ReferralMasters();
	readonly InvoiceRejectedVouchers = new InvoiceRejectedVouchers();
	readonly ProductStockDetails = new ProductStockDetails();
	readonly StockAssignmentMaster = new StockAssignmentMaster();
	readonly StockAssignmentDetails = new StockAssignmentDetails();
	readonly FieldOfficerActivity = new FieldOfficerActivity();
	readonly PatientMaster = new PatientMaster();
	readonly Blocks = new Blocks();
	readonly SMS = new Sms();

}

export const api = new Api();
