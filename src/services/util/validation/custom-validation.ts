
import { ValidationErrors, AbstractControl, AsyncValidatorFn, FormArray } from "@angular/forms";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import Swal from 'sweetalert2';

export function passwordValidator(c: AbstractControl): any {
    if (c.value !== null && c.value !== undefined) {
        const confirmPassword = c.value;
        const passwordControl = c.root.get('password');
        if (passwordControl) {
            const passwordValue = passwordControl.value;
            if (passwordValue !== confirmPassword) {
                return { 'isMatch': true, 'message': 'Password and Confirm password should match!!' };
            }
        }
    }
    return null;
}
export function bankAccountValidator(c: AbstractControl): any {
    if (c.value !== null && c.value !== undefined) {
        const confirmBankAccount = c.value;
        const bankAccountControl = c.root.get('providerBankAccountNumber');
        if (bankAccountControl) {
            const bankAccountValue = bankAccountControl.value;
            if (bankAccountValue !== confirmBankAccount) {
                return { 'isMatch': true, 'message': 'Account Number and Confirm Account Number should match!!' };
            }
        }
    }
    return null;
}
export function uniqueUserNameValidator(_userLoginService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "username": c.value
        }
        return _userLoginService.checkUniqueness(obj).pipe(
            map(users => {
                if (users[0] !== undefined && users[0].hasOwnProperty('username') === true) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: "top-end",
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                    });
                    Toast.fire({
                        icon: "error",
                        title: `${c.value} Already Exists!`,
                    });
                    return { 'unique': true, 'message': 'Username Already Exists' };
                } else {
                    return null
                }

            })
        )
    }
}

export function uniqueEmailValidator(_userLoginService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "email": c.value
        }
        return _userLoginService.checkUniqueness(obj).pipe(
            map(users => {
                if (users[0] !== undefined && users[0].hasOwnProperty('email') === true) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: "top-end",
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                    });
                    Toast.fire({
                        icon: "error",
                        title: `${c.value} Already Exists!`,
                    });
                    return { 'unique': true, 'message': 'Email Already Exists' };
                } else {
                    return null
                }

            })
        )
    }
}
export function uniqueMobileValidator(_userLoginService): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
        var obj = {
            "mobile": c.value
        }
        return _userLoginService.checkUniqueness(obj).pipe(
            map(users => {
                if (users[0] !== undefined && users[0].hasOwnProperty('mobile') === true) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: "top-end",
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                    });
                    Toast.fire({
                        icon: "error",
                        title: `${c.value} Already Exists!`,
                    });
                    return { 'unique': true, 'message': 'Mobile Already Exists' };
                } else {
                    return null
                }

            })
        )
    }
}
export class CustomValidation {
}