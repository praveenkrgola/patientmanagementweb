import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from '../util/api';

@Injectable({
  providedIn: 'root'
})
export class InvoiceHistoryService {
  private user = JSON.parse(sessionStorage.getItem("user"));
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({
		'Content-Type': 'application/json; charset=utf-8',
		'Authorization': this.token
  });
  get token(): string {
    if (this.user) {
      return this.user.accessToken.id;
    }
    return "";
  }

  getInvoiceHistory(filter: object): Observable<any>{
    const url = `${api.InvoiceHistory.common}?filter=${JSON.stringify(filter)}`;
    return this.http.get(url, {headers:this.headers});
  }
  updateInvoiceHistory(where: object, data: object): Observable<any> {
		const url = `${api.InvoiceHistory.common}/update?where=${JSON.stringify(
			where
		)}`;
		return this.http.post(url, data, { headers: this.headers });
  }
  
  getTilesDataForFinanceDashboard(filter: object): Observable<any> {
    const url = `${api.InvoiceHistory.dashboardTiles}`;
    return this.http.post(url, filter);
  }
  
  amountDueByPaymentType(filter: object): Observable<any> {
    const url = `${api.InvoiceHistory.amountDueByPaymentType}`;
    return this.http.post(url, filter);
   }
   amountDueByUserType(filter: object): Observable<any> {
    const url = `${api.InvoiceHistory.amountDueByUserType}`;
    return this.http.post(url, filter);
   }
  
   invoiceNotRaisedByUserType(filter: object): Observable<any> {
    const url = `${api.InvoiceHistory.invoiceNotRaisedByUserType}`;
    return this.http.post(url, filter);
   }
  
   amountRejectedGroupByReason(filter: object): Observable<any> {
    const url = `${api.InvoiceHistory.amountRejectedGroupByReason}`;
    return this.http.post(url, filter);
   }
  

}
