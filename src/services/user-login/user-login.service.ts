import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { ErrorHandlerService } from "../../app/util/error-handler/error-handler.service";
import { api } from "../util/api";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { error } from "protractor";
import { Router } from "@angular/router";

@Injectable({
	providedIn: "root",
})
export class UserLoginService {
	_user: any;
	isLoggedIn$ = new Subject<boolean>();
	userPs$ = new BehaviorSubject<any>(null);

	constructor(
		private http: HttpClient,
		private errorHandler: ErrorHandlerService,
		private router: Router
	) {
		const user = JSON.parse(sessionStorage.getItem("user"));
		if (user) {
			this._user = user;
			//this.userPs$.next(user);
			this.isLoggedIn$.next(true);
		}
	}

	headers = new HttpHeaders({
		'Content-Type': 'application/json; charset=utf-8',
		'Authorization': this.token
	});

	get user() {
		return this._user;
	}

	get token(): string {
		if (this.user) {

			return this.user.accessToken.id;
		} else {
			return '';
		}
	}

	getList(filter: any) {
		const url = api.userLogin.common;
		let params = { filter: JSON.stringify(filter) };
		return this.http.get(url, { params });
	}

	// Authentication/Authorization
	login(username: string, password: string): Observable<any> {
		const url = `${api.userLogin.login}?include=user`;
		return this.http
			.post<any>(url, { username, password })
			.pipe(
				tap(
					(user) => {
						this.userPs$.next(user);
					},
					({ error: { error } }) => {
						//this.errorHandler.error(error);
					}
				)
			);
	}
	//update user
	update(user: any) {
		const url = api.userLogin.byId(user.id);
		delete user.id;
		return this.http.patch<any>(url, user).pipe(
			tap(
				() => { },
				(error) => {
					this.errorHandler.error(error);
				}
			)
		);
	}

	loginByMobile(mobileNumber: number, loginFrom: string) {
		const url = `${api.userLogin.loginByMobile}`;
		return this.http
			.post<any>(url, { mobileNum: mobileNumber, loginFrom })
			.pipe(
				tap(
					(user) => {
						this.userPs$.next(user);
					},
					({ error: { error } }) => {
						//this.errorHandler.error(error);
					}
				)
			);
	}

	autoLogin() {
		const loadedUser = JSON.parse(sessionStorage.getItem("user"));
		if (!loadedUser) {
			return;
		}
		if (loadedUser.id) {
			this.userPs$.next(loadedUser);
		}
	}

	add(user: any) {
		const url = api.userLogin.common;
		return this.http.post<any>(url, user, { headers:this.headers}).pipe(
			tap(
				() => { },
				({ error: { error } }) => {
					this.errorHandler.error(error);
				}
			)
		);
	}

	logout() {
		localStorage.removeItem("layoutConfig");
		sessionStorage.clear();
		this.userPs$.next(null);
		this.router.navigate(["/auth/login"]);
		setTimeout(() => {
			location.reload();
		}, 200);
		//this.store.dispatch(new Logout());
	}



	updateUser(where: any, data: any) {

		// const headers = new HttpHeaders({
		// 	'Content-Type': 'application/json;',
		// 	'Authorization': this.token()
		// });

		const url = `${api.userLogin.update}?where=${JSON.stringify(where)}`
		//return this.http.post<any>(url, data, { headers: this.headers }).pipe(

		return this.http.post<any>(url, data).pipe(
			tap(
				() => { },
				(error) => {
					this.errorHandler.error(error);
				}
			)
		);
	}

	checkUniqueness(params: Object): Observable<any> {
		let obj = {
			where: {},
		};
		if (params.hasOwnProperty("username") === true) {
			obj.where["username"] = params["username"];
		} else if (params.hasOwnProperty("email") === true) {
			obj.where["email"] = params["email"];
		}  else if (params.hasOwnProperty("mobile") === true) {
			obj.where["mobile"] = params["mobile"];
		}
		let url = api.userLogin.common + "?filter=" + JSON.stringify(obj);
		return this.http.get<any>(url);
	}

	sendSmsToDesignations(data: { designations: string[], smsType: string }): Observable<{ message: string, data: any }> {
		let url = api.userLogin.sendSmsToDesignations;
		return this.http.post<{ message: string, data: any }>(url, data);
	}
}
