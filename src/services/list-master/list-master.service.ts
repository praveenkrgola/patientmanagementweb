import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api } from '../util/api';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListMasterService {
  private user = JSON.parse(sessionStorage.getItem("user"));
  headers = new HttpHeaders({
		'Content-Type': 'application/json; charset=utf-8',
		'Authorization': this.token
  });
  get token(): string {
		if (this.user) {
			return this.user.accessToken.id;
		}
		return '';
  }
  constructor(private http: HttpClient) {     
  }
  
  getList(obj: object): Observable<any> {
    const url = api.listMasters.getLists
    return this.http.post(url, obj,{ headers: this.headers });
  }

  getStakeholder(filter: object): Observable<any> {
    const url = `${api.listMasters.common}?filter=${JSON.stringify(filter)}`;
    return this.http.get(url, { headers: this.headers });
  }
}
