export enum ESmsType {
	userRegister = 'user_registration',
	userInvoicePaymentExceeded = 'user_invoice_payment_exceeded',
	userInvoicePaymentClaimApproval = 'user_invoice_payment_claim_approval',
	userInvoicePaymentClaimRejection = 'user_invoice_payment_claim_rejection',
	userPatientEnrollment = 'user_patient_enrollment',
	userRedemption = 'user_redemption',
	userInvoiceConfirmation = 'user_invoice_confirmation',
	user_invoice_payment_initialize = "user_invoice_payment_initialize",
	user_invoice_payment_claim_approval = "user_invoice_payment_claim_approval",
	user_invoice_payment_claim_rejection = "user_invoice_payment_claim_rejection",
	appUpdate = 'app_update',
}


export interface IUserRegistration {
	mobile: number;
	type: ESmsType;
	loginId: string;
	where?: object;
}
