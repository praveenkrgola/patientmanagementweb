export enum EUserDesignations {
	programmanager = "Program Manager",
	empanelledchemist = "Empanelled Chemist",
	generaldoctor = "General Doctor",
	chemist = "Chemist",
	sctagent = "SCT Agent",
	empanelleddoctor = "Empanelled Doctor",
	fieldofficer = "Field Officer",
	patientcoordinator = "Patient Coordinator",
	lab = "Lab",
	finance = "Finance",
	ayushprovider = "Ayush Provider",
}

export const DesignationList = [
	{designation: "Ayush Provider", value: "ayushprovider" },
	{designation: "Chemist", value: "chemist"},
	{designation: "Empanelled Chemist", value: "empanelledchemist" },
	{designation: "Empanelled Doctor", value: "empanelleddoctor" },
	{designation: "Field Officer", value: "fieldofficer"},
	{designation: "Finance", value: "finance"},
	{designation: "General Doctor", value: "generaldoctor"},
	{designation: "Lab", value: "lab"},
	{designation: "Patient Coordinator", value: "patientcoordinator"},
	{designation: "Program Manager", value: "programmanager"},
	{designation: "SCT Agent", value: "sctagent"},
]

