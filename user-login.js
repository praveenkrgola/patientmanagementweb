"use strict";
const PDFDocument = require("pdfkit");
const fs = require("fs");
const async = require("async");
const { SendMail } = require("../util/util.js");

let bcrypt;
try {
  // Try the native module first
  bcrypt = require("bcrypt");
  // Browserify returns an empty object
  if (bcrypt && typeof bcrypt.compare !== "function") {
    bcrypt = require("bcryptjs");
  }
} catch (err) {
  // Fall back to pure JS impl
  bcrypt = require("bcryptjs");
}

module.exports = function (Userlogin) {
  //-------------------------Praveen Kumar(24-07-2018)----------------------------
  Userlogin.login = function (credentials, include, cb) {
    var self = this;
    if (cb == null) {
      cb = include;
    }
    //Praveen
    console.log("credentials--", credentials);
    if (credentials.username === undefined || credentials.username == "") {
      var err = new Error("Username is undefined or empty");
      err.statusCode = 401;
      err.code = "LOGIN_FAILED";
      return cb(err);
    }

    if (credentials.password === undefined || credentials.password == "") {
      var err = new Error("Password is undefined or empty");
      err.statusCode = 401;
      err.code = "LOGIN_FAILED";
      return cb(err);
    }

    let loginCredential = {
      username: credentials.username,
    };

    self.findOne(
      {
        where: loginCredential,
        include: [
          {
            relation: "userAccess",
          },
          {
            relation: "icon",
          },
          {
            relation: "healthFacility",
          },
          //// , {
          //     relation: 'school'
          // },
          // {
          // relation: 'company',
          // scope: {
          //     include: ["logo"]
          // }
          // },
          // {
          //     relation: 'image'
          // }
        ],
      },
      function (err, user) {
        if (err) {
          console.log(err);
          return cb(err);
        }
        if (!user) {
          var err = new Error("Oops!!!, Invalid Username or Password.");
          err.statusCode = 401;
          err.code = "LOGIN_FAILED";
          return cb(err);
        }
        if (user.length == 0) {
          var err = new Error("Oops!!!, Invalid Username or Password.");
          err.statusCode = 401;
          err.code = "LOGIN_FAILED";
          return cb(err);
        }

        if (user.status == true) {
          bcrypt.compare(
            credentials.password,
            user.password,
            function (err, isMatch) {
              if (err) {
                console.log(err);
                return fn(err);
              }
              if (!isMatch) {
                var defaultError = new Error(
                  "Oops!!!, Invalid Username or Password."
                );
                defaultError.statusCode = 401;
                defaultError.code = "LOGIN_FAILED";
                return cb(defaultError);
              } else {
                let accesstokens = [];
                user.createAccessToken(2419200, function (err, token) {
                  if (err) return cb(err);

                  if (!accesstokens || accesstokens === undefined) {
                    accesstokens = [];
                  }
                  accesstokens.push(token);
                  user.accessToken = accesstokens[0];

                  self.app.models.AccessToken.replaceOrCreate(
                    accesstokens,
                    function (err, updatedAccessToken) {
                      if (err) {
                        console.log(err);
                      }

                      self.app.models.Form.find(
                        {
                          where: { designation: user.designation },
                          include: "question",
                        },
                        function (err, formResult) {
                          if (err) return cb(false, { user });

                          user.formDetail = formResult;
                          return cb(false, { user });
                        }
                      );
                    }
                  );
                });
              }
            }
          );
        } else {
          err.statusCode = 401;
          err.code = "LOGIN_FAILED";
          return cb(err);
        }
      }
    );
  };
  //---------------------------------END------------------------------------------------------

  Userlogin.generateCertificate = async function (parameters) {
    //parameters={"name" : "Praveen Kumar", "fromDate" : "2019-12-26", "toDate": "2020-06-18"}
    const self = this;
    const companyDetail = await Userlogin.app.models.CompanyMaster.find({
      where: { id: parameters.companyId },
    });

    const userDetail = await self.find({ where: { id: parameters.userId } });
    // console.log(userDetail);

    let name = "";
    const nameArray = userDetail[0].name.split(" ");
    nameArray.forEach((nm) => {
      name += nm.charAt(0).toUpperCase() + nm.slice(1).toLowerCase() + " ";
    });

    const logoDetail = await Userlogin.app.models.FileLibrary.find({
      where: { id: companyDetail[0].logoId },
    });
    const certificateObject = {
      name,
      logoname:
        logoDetail.length > 0 ? logoDetail[0].modifiedFileName : undefined,
    };
    const fileName = await generateDigitalCertificate(certificateObject);

    const passingObject = {
      publicUsername: companyDetail[0].publicEmail, //'support@etech-services.com',
      publicPassword: companyDetail[0].publicEmailPassword, // 'etech#$%@325',
      from: companyDetail[0].publicEmail,
      to: userDetail[0].email,
      //cc: [''],
      subject: companyDetail[0].subject,
      mailBody: companyDetail[0].mailBody.replace("Username", name),
      filePath: [
        {
          path: `./client/doc/DigitalCertificate/${fileName}`,
        },
      ],
    };

    const mailResult = await SendMail(passingObject);

    const CourseAssignmentResult = await Userlogin.app.models.CourseAssignment.update(
      { userId: parameters.userId, courseId: parameters.courseId },
      { certificateName: fileName, courseEndDate: new Date() }
    );

    return CourseAssignmentResult;
  };

  Userlogin.remoteMethod("generateCertificate", {
    description: "Generate Digital Certificate",
    accepts: [
      {
        arg: "parameters",
        type: "object",
        http: { source: "body" },
        required: true,
      },
    ],
    returns: {
      root: true,
      type: "string",
    },
    http: {
      verb: "post",
    },
  });

  function generateDigitalCertificate(data) {
    const doc = new PDFDocument({
      size: "A4",
      //size: '[3509, 2481]'
      layout: "landscape",
    });

    doc.image("./client/assets/background/bg1.jpg", 0, 0, {
      width: doc.page.width,
      height: doc.page.height,
    });

    doc.moveDown(14);
    doc.fontSize(28);
    doc.fillAndStroke("#8d3e41");
    doc.font("./client/assets/fonts/angelicy.regular.ttf"); //Custom Font
    doc.text(data.name, {
      align: "center",
    });

    if (data.logoname) {
      doc.image(`./client/doc/logo/${data.logoname}`, 730, 520, {
        fit: [80, 80],
      });
    }

    doc.end();

    const fileName = `${new Date().getTime()}_Digital Certificate ${
      data.name
    }.pdf`;
    doc.pipe(
      fs.createWriteStream(`./client/doc/DigitalCertificate/${fileName}`)
    );

    return fileName;
  }
};
